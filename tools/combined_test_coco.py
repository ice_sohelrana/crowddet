import os
import sys
import math
import argparse

import numpy as np
from tqdm import tqdm
import torch
from torch.multiprocessing import Queue, Process
from pytorch_eval.coco_utils import get_coco_api_from_dataset
from pytorch_eval.coco_eval import CocoEvaluator
sys.path.insert(0, '../lib')
sys.path.insert(0, '../model')
from data.CrowdHuman import CrowdHuman
from data.sports import Sports

from utils import misc_utils, nms_utils
from evaluate import compute_JI, compute_APMR
import pdb

def eval_all(args, config, network):
    # model_path
    saveDir = os.path.join('../model', args.model_dir, config.model_dir)
    evalDir = os.path.join('../model', args.model_dir, config.eval_dir)
    misc_utils.ensure_dir(evalDir)
    # model_file = os.path.join(saveDir, 
    #         'dump-{}.pth'.format(args.resume_weights))
    # model_file = os.path.join(saveDir,'rcnn_fpn_baseline_mge.pth')

    assert os.path.exists(args.weights_dir)
    # get devices
    str_devices = args.devices
    devices = misc_utils.device_parser(str_devices)
    # load data
    # crowdhuman = CrowdHuman(config, if_train=False)
    sportsdata = Sports(config, if_train=False,other_sports=False)
    data_iter = torch.utils.data.DataLoader(dataset=sportsdata, shuffle=False)
    coco = get_coco_api_from_dataset(sportsdata)
    iou_types = ["bbox"]
    coco_evaluator = CocoEvaluator(coco, iou_types)
    inference_single_process(config, network, args.weights_dir, data_iter,devices[0], coco_evaluator)
    coco_evaluator.synchronize_between_processes()
    coco_evaluator.accumulate()
    coco_evaluator.summarize()
def inference_single_process(config, network, model_file, data_iter,device, coco_evaluator):
    net = network()
    net.cuda(device)
    net = net.eval()
    check_point = torch.load(model_file)
    net.load_state_dict(check_point['state_dict'])    
    # inference
    # images, gt_boxes, im_info,boxmaps
    for (image, gt_boxes, im_info, boxmaps) in data_iter:
        # print(image.shape,gt_boxes.shape,)
        pdb.set_trace()
        pred_boxes,boxmap = net(image.cuda(device), im_info.cuda(device),gt_boxes['boxes'].cuda(device),boxmaps.cuda(device))

        scale = im_info[0, 2]
        if config.test_nms_method == 'set_nms':
            assert pred_boxes.shape[-1] > 6, "Not EMD Network! Using normal_nms instead."
            assert pred_boxes.shape[-1] % 6 == 0, "Prediction dim Error!"
            top_k = pred_boxes.shape[-1] // 6
            n = pred_boxes.shape[0]
            pred_boxes = pred_boxes.reshape(-1, 6)
            idents = np.tile(np.arange(n)[:,None], (1, top_k)).reshape(-1, 1)
            pred_boxes = np.hstack((pred_boxes, idents))
            keep = pred_boxes[:, 4] > config.pred_cls_threshold
            pred_boxes = pred_boxes[keep]
            keep = nms_utils.set_cpu_nms(pred_boxes, 0.5)
            pred_boxes = pred_boxes[keep]
        elif config.test_nms_method == 'normal_nms':
            assert pred_boxes.shape[-1] % 6 == 0, "Prediction dim Error!"
            pred_boxes = pred_boxes.reshape(-1, 6)
            keep = pred_boxes[:, 4] > config.pred_cls_threshold
            pred_boxes = pred_boxes[keep]
            keep = nms_utils.cpu_nms(pred_boxes, config.test_nms)
            pred_boxes = pred_boxes[keep]
        elif config.test_nms_method == 'none':
            assert pred_boxes.shape[-1] % 6 == 0, "Prediction dim Error!"
            pred_boxes = pred_boxes.reshape(-1, 6)
            keep = pred_boxes[:, 4] > config.pred_cls_threshold
            pred_boxes = pred_boxes[keep]
        else:
            raise ValueError('Unknown NMS method.')
        #if pred_boxes.shape[0] > config.detection_per_image and \
        #    config.test_nms_method != 'none':
        #    order = np.argsort(-pred_boxes[:, 4])
        #    order = order[:config.detection_per_image]
        #    pred_boxes = pred_boxes[order]
        # recovery the scale
        # pdb.set_trace()
        # im = transforms.ToPILImage()(orig_image)
        # draw = ImageDraw.Draw(im)
        # for a_box in pred_boxes[:,0:5]:
        #     draw.rectangle([int(a_box[0]),int(a_box[1]),int(a_box[2]),int(a_box[3])],fill=None,outline="red",width=2)
        predictions = coco_evaluator.convert_sports_to_coco_detection(pred_boxes[:,0:5])
        # print(predictions)
        res = {gt_boxes['image_id'].item():predictions[0]}
        # res = {target["image_id"].item(): output for target, output in zip(gt_boxes, predictions)}
        # for target,output in zip(gt_boxes,predictions):
        #     print(gt_boxes)
        # print(res)
        coco_evaluator.update(res)
def inference(config, network, model_file, device, dataset, start, end, coco_evaluator,result_queue):
    torch.set_default_tensor_type('torch.FloatTensor')
    torch.multiprocessing.set_sharing_strategy('file_system')
    # init model
    net = network()
    net.cuda(device)
    net = net.eval()
    check_point = torch.load(model_file)
    net.load_state_dict(check_point['state_dict'])
    # init data
    # dataset.records = dataset.records[start:end];
    dataset.records = dataset.records[start:end];

    data_iter = torch.utils.data.DataLoader(dataset=dataset, shuffle=False)
    # inference
    for (image, gt_boxes, im_info, ID) in data_iter:
        # print(image.shape,gt_boxes.shape,)
        pred_boxes = net(image.cuda(device), im_info.cuda(device))
        scale = im_info[0, 2]
        if config.test_nms_method == 'set_nms':
            assert pred_boxes.shape[-1] > 6, "Not EMD Network! Using normal_nms instead."
            assert pred_boxes.shape[-1] % 6 == 0, "Prediction dim Error!"
            top_k = pred_boxes.shape[-1] // 6
            n = pred_boxes.shape[0]
            pred_boxes = pred_boxes.reshape(-1, 6)
            idents = np.tile(np.arange(n)[:,None], (1, top_k)).reshape(-1, 1)
            pred_boxes = np.hstack((pred_boxes, idents))
            keep = pred_boxes[:, 4] > config.pred_cls_threshold
            pred_boxes = pred_boxes[keep]
            keep = nms_utils.set_cpu_nms(pred_boxes, 0.5)
            pred_boxes = pred_boxes[keep]
        elif config.test_nms_method == 'normal_nms':
            assert pred_boxes.shape[-1] % 6 == 0, "Prediction dim Error!"
            pred_boxes = pred_boxes.reshape(-1, 6)
            keep = pred_boxes[:, 4] > config.pred_cls_threshold
            pred_boxes = pred_boxes[keep]
            keep = nms_utils.cpu_nms(pred_boxes, config.test_nms)
            pred_boxes = pred_boxes[keep]
        elif config.test_nms_method == 'none':
            assert pred_boxes.shape[-1] % 6 == 0, "Prediction dim Error!"
            pred_boxes = pred_boxes.reshape(-1, 6)
            keep = pred_boxes[:, 4] > config.pred_cls_threshold
            pred_boxes = pred_boxes[keep]
        else:
            raise ValueError('Unknown NMS method.')
        #if pred_boxes.shape[0] > config.detection_per_image and \
        #    config.test_nms_method != 'none':
        #    order = np.argsort(-pred_boxes[:, 4])
        #    order = order[:config.detection_per_image]
        #    pred_boxes = pred_boxes[order]
        # recovery the scale


        predictions = coco_evaluator.convert_sports_to_coco_detection(pred_boxes[:,0:5])
        # print(predictions)
        res = {gt_boxes['image_id'].item():predictions[0]}
        # res = {target["image_id"].item(): output for target, output in zip(gt_boxes, predictions)}
        # for target,output in zip(gt_boxes,predictions):
        #     print(gt_boxes)
        # print(res)
        coco_evaluator.update(res)
        pred_boxes[:, :4] /= scale
        pred_boxes[:, 2:4] -= pred_boxes[:, :2]
        gt_boxes = gt_boxes['boxes'][0].numpy()

        gt_boxes[:, 2:4] -= gt_boxes[:, :2]
        # result_dict = dict(ID=ID[0].item(), height=int(im_info[0, -3]), width=int(im_info[0, -2]),
        #         dtboxes=boxes_dump(pred_boxes), gtboxes=boxes_dump(gt_boxes))
        # result_dict = dict(dtboxes=boxes_dump(pred_boxes), gtboxes=boxes_dump(gt_boxes))
        result_dict = torch.tensor(1)

        # print(gt_boxes)
        # predictions = coco_evaluator.convert_sports_to_coco_detection(pred_boxes)
        # res = {target["image_id"].item(): output for target, output in zip(targets, predictions)}
        # coco_evaluator.update(res)
        result_queue.put_nowait(result_dict)

def boxes_dump(boxes):
    if boxes.shape[-1] == 7:
        result = [{'box':[round(i, 1) for i in box[:4]],
                   'score':round(float(box[4]), 5),
                   'tag':int(box[5]),
                   'proposal_num':int(box[6])} for box in boxes]
    elif boxes.shape[-1] == 6:
        result = [{'box':[round(i, 1) for i in box[:4].tolist()],
                   'score':round(float(box[4]), 5),
                   'tag':int(box[5])} for box in boxes]
    elif boxes.shape[-1] == 5:
        result = [{'box':[round(i, 1) for i in box[:4]],
                   'tag':int(box[4])} for box in boxes]
    else:
        raise ValueError('Unknown box dim.')
    return result

def run_test():
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_dir', '-md', default=None, required=True, type=str)
    parser.add_argument('--weights_dir', '-r', default='/media/sohel/HDD2/detection_algorithms/CrowdDet/model/rcnn_fpn_baseline/rcnn_fpn_all_combined/model_dump/dump-4.pth', required=False, type=str)
    parser.add_argument('--devices', '-d', default='0', type=str)
    os.environ['NCCL_IB_DISABLE'] = '1'
    args = parser.parse_args()
    # import libs
    model_root_dir = os.path.join('../model/', args.model_dir)

    sys.path.insert(0, model_root_dir)
    from config import config
    from combined_network import Network
    eval_all(args, config, Network)

if __name__ == '__main__':
    run_test()

