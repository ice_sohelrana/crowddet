import os
import sys
import argparse
import torch.utils.data as data
from torchvision import datasets, transforms as T
import cv2
import torch
import csv
import glob
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
from PIL import Image
import gzip
sys.path.insert(0, '../boxmap_model/utils')
from gaussian import gaussian_radius,draw_gaussian
import pdb
class Sports(data.Dataset):
	def __init__(self, img_source):
		self.training = True
		source = img_source
		self.short_size = 1080 
		self.max_size = 1920 
		self.width = 1920
		self.height = 1080
		videos = os.listdir(source)
		self.images = []
		self.targets = []
		self.boxmap = []
		self.source = source
		self.image_ids = []
		image_id = 1
		self.videonames=[]
		self.frm_names = []
		for a_video in videos:
			frm_id = 1
			image_dirs = sorted(glob.glob(os.path.join(source, a_video, "img1","*.png")))
			if not os.path.exists(os.path.join(source,a_video,'cornermaps','tl')):
				os.makedirs(os.path.join(source,a_video,'cornermaps','tl'))
			if not os.path.exists(os.path.join(source,a_video,'cornermaps','br')):
				os.makedirs(os.path.join(source,a_video,'cornermaps','br'))
			ann_path = os.path.join(source,a_video,"gt.csv")
			annotations = self.load_annotation(ann_path)
			for ind,a_dir in enumerate(image_dirs):
				self.images.append(a_dir)
				self.image_ids.append(image_id)
				frame_boxes = self.get_single_same_frame_objects(frm_id)
				self.targets.append(frame_boxes)
				self.videonames.append(a_video)
				frm_id = frm_id+1
				self.frm_names.append(a_dir.split('/')[-1])
				image_id = image_id + 1
		self.images = self.images
		self.targets = self.targets
		self.image_ids = self.image_ids
		self.records = self.targets
		self.size = len(self.images)
		self.normalize = T.Normalize(mean=[0.406, 0.456, 0.485],std=[0.225, 0.224, 0.229])
	def __getitem__(self, index):
		image_path = self.images[index]
		image = cv2.imread(image_path, cv2.IMREAD_COLOR)
		image_h = image.shape[0]
		image_w = image.shape[1]
		gtboxes = self.targets[index] 
		image = image.transpose(2, 0, 1)
		image = torch.tensor(image).float()
		gtboxes = np.array(gtboxes)
		gtboxes = gtboxes.astype('float64')
		target = {}
		target["boxes"] = torch.from_numpy(gtboxes[:,0:4])
		target["labels"] = torch.ones((len(gtboxes),), dtype=torch.int64)
		target["area"] = (gtboxes[:, 3] - gtboxes[:, 1]) * (gtboxes[:, 2] - gtboxes[:, 0])
		target["image_id"] = torch.tensor([self.image_ids[index]])
		target["iscrowd"] = torch.zeros((len(gtboxes),), dtype=torch.int64)
		nr_gtboxes = gtboxes.shape[0]
		im_info = np.array([0, 0, 1, image_h, image_w, nr_gtboxes])
		return image,target, self.videonames[index],self.frm_names[index]
	def load_annotation(self,ann_path):
		annotations = []
		with open(ann_path, 'r') as csvfile:
			csv_read = csv.reader(csvfile, delimiter = ',')
			next(csv_read)
			for row in csv_read:
				if row[2]!= 'Others' and row[2]!= 'other' and row[2] != 'others' and row[2]!= 'Other':
					x = int(float(row[10]))
					y = int(float(row[9]))
					width = int(float(row[11]))
					height = int(float(row[12]))
					frame_number = int(row[4].replace(".png",''))
					annotations.append([frame_number,x,y,x+width,y+height])
		annotations = self.same_frame_objects(annotations)
		return annotations
	def same_frame_objects(self,annotations):
		framewise_order = sorted(annotations,key=lambda x: int(x[0]))
		prev_frame_number = 1
		single_frame_objs = []
		self.frame_objs = []
		for i in range(len(framewise_order)):
			if int(framewise_order[i][0])==prev_frame_number:
				single_frame_objs.append([framewise_order[i][1],framewise_order[i][2],framewise_order[i][3],framewise_order[i][4],1])
			else:
				if len(single_frame_objs)==0:
					self.frame_objs.append("empty")
				else:
					self.frame_objs.append(single_frame_objs)
				single_frame_objs=[]
				single_frame_objs.append([framewise_order[i][1],framewise_order[i][2],framewise_order[i][3],framewise_order[i][4],1])
			prev_frame_number=int(framewise_order[i][0])
		if len(single_frame_objs)==0:
			self.frame_objs.append("empty")
		else:
			self.frame_objs.append(single_frame_objs)
		return
	def get_single_same_frame_objects(self,frame_number):
		return self.frame_objs[int(frame_number)-1] 
def run(args):
	datasets=Sports(args.video_dirs)
	for img,target,videoname,img_id in datasets:
		boxes = target['boxes'].numpy()
		img_height = img.shape[1]
		img_width = img.shape[2]
		tl_heatmap = np.zeros((img_height,img_width))
		br_heatmap = np.zeros((img_height,img_width))
		for box in boxes:
			width=box[2]-box[0]
			height=box[3]-box[1]
			# radious=gaussian_radius((height,width),.3)
			radious=14.0
			draw_gaussian(tl_heatmap,[int(box[0]),int(box[1])],int(round(radious)))
			draw_gaussian(br_heatmap,[int(box[2]),int(box[3])],int(round(radious)))
		Path(os.path.join(args.video_dirs,videoname,'fixed_cornermaps','tl')).mkdir(parents=True, exist_ok=True)
		Path(os.path.join(args.video_dirs,videoname,'fixed_cornermaps','br')).mkdir(parents=True, exist_ok=True)
		tl_path=os.path.join(args.video_dirs,videoname,'fixed_cornermaps','tl',img_id.split('.')[0])
		br_path=os.path.join(args.video_dirs,videoname,'fixed_cornermaps','br',img_id.split('.')[0])
		# pdb.set_trace()
		np.savez_compressed(tl_path,heatmap=tl_heatmap)
		np.savez_compressed(br_path,heatmap=br_heatmap)
		# import pdb
		# pdb.set_trace()
def run_cornermap():
	parser = argparse.ArgumentParser()
	parser.add_argument('--video_dirs',  default="/media/sohel/HDD2/googledrive/detection_sports/netball_bbl/other_sports/val",  type=str)
	args = parser.parse_args()
	# import libs
	run(args)

if __name__ == '__main__':
	run_cornermap()
