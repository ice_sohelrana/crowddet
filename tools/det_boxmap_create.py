import os
import sys
import argparse

import cv2
import torch
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
sys.path.insert(0, '../lib')
from utils import misc_utils, visual_utils, nms_utils
import pdb
import os
def inference(args, config, network):
    # model_path
    misc_utils.ensure_dir('outputs')
    # saveDir = os.path.join('../model', args.model_dir, config.model_dir)
    # model_file = os.path.join(saveDir,
    #         'dump-{}.pth'.format(args.resume_weights))
    model_file = '/media/sohel/HDD2/detection_algorithms/CrowdDet/model/rcnn_emd_refine/outputs/model_dump_all/dump-8.pth'
    assert os.path.exists(model_file)
    # build network
    net = network()
    net.eval()
    check_point = torch.load(model_file)
    net.load_state_dict(check_point['state_dict'])
    net = net.cuda()
    videos = os.listdir(args.image_dir)
    # videos = ['val']
    for a_video in videos:
        # get data
        if not os.path.exists(os.path.join(args.image_dir,a_video,'emd_detection_boxes')):
            os.makedirs(os.path.join(args.image_dir,a_video,'emd_detection_boxes'))
        if not os.path.exists(os.path.join(args.image_dir,a_video,'emd_detection_boxmaps')):
            os.makedirs(os.path.join(args.image_dir,a_video,'emd_detection_boxmaps'))
        image_lists = sorted(os.listdir(os.path.join(args.image_dir,a_video,"img1")))
        for ind,image_list in enumerate(image_lists):
            image_id = image_list.split('.png')[0]
            img_path  =os.path.join(args.image_dir,a_video,"img1",image_list)
            image, resized_img, im_info = get_data(
                    img_path, config.eval_image_short_size, config.eval_image_max_size)
            # This part is for crowdhuman data
            # image = cv2.imread(img_path, cv2.IMREAD_COLOR)
            # image_h = image.shape[0]
            # image_w = image.shape[1]
            # t_height, t_width, scale = target_size(
            #         image_h, image_w, config.eval_image_short_size, config.eval_image_max_size)
            # resized_image = cv2.resize(image, (t_width, t_height), interpolation=cv2.INTER_LINEAR)
            # resized_image = resized_image.transpose(2, 0, 1)
            # resized_img = torch.tensor(resized_image).float()
            # im_info = torch.tensor([t_height, t_width, scale, image_h, image_w,1000])
            # im_info = im_info.unsqueeze(dim=0)

            pred_boxes = net(resized_img.cuda(), im_info.cuda()).numpy()
            pred_boxes = post_process(pred_boxes, config, im_info[0, 2])
            boxmap = boxmapCreate(pred_boxes,image.shape[:2])
            boxmap = Image.fromarray(np.uint8(boxmap))
            boxmap.save(os.path.join(args.image_dir,a_video,"emd_detection_boxmaps",image_id+'.png'))
            np.save(os.path.join(args.image_dir,a_video,"emd_detection_boxes",image_id+'.npy'), pred_boxes)

def boxmapCreate(boxes, imageShape, image=None):
    """Render a boxmap image.

    Parameters
    ----------
    boxes : np.ndarray
        [N,4] numpy array boxes
    imageShape : tuple
        (height, width) represent image height and width
    image : :obj:`np.ndarray`, optional
        Initial image to render the boxes on. If not specified, the boxes will be rendered over a blank image.
    Returns
    -------
    np.ndarray : 2-dimensional boxmap image which shape equal to imageShape
    """
    if image is None:
        image = np.zeros((imageShape))
    for a_box in np.asarray(boxes, dtype=np.int32):
        image[a_box[1]:a_box[3], a_box[0]:a_box[2]] += 1
    return image
def post_process(pred_boxes, config, scale):
    if config.test_nms_method == 'set_nms':
        assert pred_boxes.shape[-1] > 6, "Not EMD Network! Using normal_nms instead."
        assert pred_boxes.shape[-1] % 6 == 0, "Prediction dim Error!"
        top_k = pred_boxes.shape[-1] // 6
        n = pred_boxes.shape[0]
        pred_boxes = pred_boxes.reshape(-1, 6)
        idents = np.tile(np.arange(n)[:,None], (1, top_k)).reshape(-1, 1)
        pred_boxes = np.hstack((pred_boxes, idents))
        keep = pred_boxes[:, 4] > config.pred_cls_threshold
        pred_boxes = pred_boxes[keep]
        # keep = nms_utils.set_cpu_nms(pred_boxes, 0.5)
        # pred_boxes = pred_boxes[keep]
    elif config.test_nms_method == 'normal_nms':
        assert pred_boxes.shape[-1] % 6 == 0, "Prediction dim Error!"
        pred_boxes = pred_boxes.reshape(-1, 6)
        keep = pred_boxes[:, 4] > config.pred_cls_threshold
        pred_boxes = pred_boxes[keep]
    #     keep = nms_utils.cpu_nms(pred_boxes, config.test_nms)
    #     pred_boxes = pred_boxes[keep]
    # elif config.test_nms_method == 'none':
    #     assert pred_boxes.shape[-1] % 6 == 0, "Prediction dim Error!"
    #     pred_boxes = pred_boxes.reshape(-1, 6)
    #     keep = pred_boxes[:, 4] > config.pred_cls_threshold
    #     pred_boxes = pred_boxes[keep]
    #if pred_boxes.shape[0] > config.detection_per_image and \
    #    config.test_nms_method != 'none':
    #    order = np.argsort(-pred_boxes[:, 4])
    #    order = order[:config.detection_per_image]
    #    pred_boxes = pred_boxes[order]
    # recovery the scale
    pred_boxes[:, :4] /= scale
    # keep = pred_boxes[:, 4] > config.visulize_threshold
    # pred_boxes = pred_boxes[keep]
    return pred_boxes

def get_data(img_path, short_size, max_size):
    image = cv2.imread(img_path, cv2.IMREAD_COLOR)
    resized_img = image
    # resized_img, scale = resize_img(
    #         image, short_size, max_size)

    original_height, original_width = image.shape[0:2]
    height, width = resized_img.shape[0:2]
    resized_img = resized_img.transpose(2, 0, 1)
    resized_img = torch.tensor(resized_img).float()
    # im_info = np.array([height, width, scale, original_height, original_width, 0])
    im_info = np.array([0, 0, 1, height, width, 0])

    return image, resized_img, torch.tensor([im_info])

def resize_img(image, short_size, max_size):
    image = cv2.imread(image, cv2.IMREAD_COLOR)
    height = image.shape[0]
    width = image.shape[1]
    im_size_min = np.min([height, width])
    im_size_max = np.max([height, width])
    scale = (short_size + 0.0) / im_size_min
    if scale * im_size_max > max_size:
        scale = (max_size + 0.0) / im_size_max
    t_height, t_width = int(round(height * scale)), int(
        round(width * scale))
    resized_image = cv2.resize(
            image, (t_width, t_height), interpolation=cv2.INTER_LINEAR)
    return resized_image, scale
def target_size(height, width, short_size, max_size):
    im_size_min = np.min([height, width])
    im_size_max = np.max([height, width])
    scale = (short_size + 0.0) / im_size_min
    if scale * im_size_max > max_size:
        scale = (max_size + 0.0) / im_size_max
    t_height, t_width = int(round(height * scale)), int(
        round(width * scale))
    return t_height, t_width, scale
def run_inference():
    parser = argparse.ArgumentParser()
    parser.add_argument('--image_dir',  default="/media/sohel/HDD2/googledrive/detection_sports/netball_bbl/other_sports/train",  type=str)
    parser.add_argument('--model_dir', '-md', default=None, required=False, type=str)
    parser.add_argument('--resume_weights', '-r', default=None, required=False, type=str)
    parser.add_argument('--img_path', '-i', default=None, required=False, type=str)
    args = parser.parse_args()
    # import libs
    model_root_dir = os.path.join('../model/', args.model_dir)
    sys.path.insert(0, model_root_dir)
    from config import config
    from network import Network
    inference(args, config, Network)

if __name__ == '__main__':
    run_inference()
