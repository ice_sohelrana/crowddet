import os
import cv2
import torch
import numpy as np
import torch.utils.data as data

from utils import misc_utils
from data.resize import fit
import os
import pdb
import glob, csv
class Sports(data.Dataset):
    def __init__(self, config, if_train,other_sports=False):
        # pdb.set_trace()
        if if_train:
            self.training = True
            source = config.train_source
            other_source = config.train_other_source
            self.short_size = 1080 #config.train_image_short_size
            self.max_size = 1920 #config.train_image_max_size
            self.width = config.width
            self.height = config.height
            # image_set = 'train'
        else:
            self.training = False
            source = config.eval_source
            other_source = config.eval_other_source
            self.short_size = 1080 #config.eval_image_short_size
            self.max_size = 1920 #config.eval_image_max_size
            self.width = config.width
            self.height = config.height
            # image_set='val'
        # self.records = misc_utils.load_json_lines(source)
        videos = os.listdir(source)
        self.images = []
        self.targets = []
        self.boxmap = []
        self.source = source
        self.image_ids = []
        image_id = 1
        for a_video in videos:
            frm_id = 1
            image_dirs = sorted(glob.glob(os.path.join(source, a_video, "img1","*.png")))
            ann_path = os.path.join(source,a_video,"gt.csv")
            annotations = self.load_annotation(ann_path)
            for ind,a_dir in enumerate(image_dirs):

                self.images.append(a_dir)
                boxmap_path=os.path.join(source,a_video,'masks',str(int(a_dir.split('/')[-1].split('.png')[0]))+'.png')
                if os.path.isfile(boxmap_path)==True:
                    self.boxmap.append(boxmap_path)
                else:
                    print('Error')
                self.image_ids.append(image_id)
                frame_boxes = self.get_single_same_frame_objects(frm_id)
                self.targets.append(frame_boxes)
                frm_id = frm_id+1
                image_id = image_id + 1
        self.config = config
        if other_sports==True:
            videos = os.listdir(other_source)
            for a_video in videos:
                frm_id = 1
                image_dirs = sorted(glob.glob(os.path.join(other_source, a_video, "img1","*.png")))
                ann_path = os.path.join(other_source,a_video,"gt.csv")
                annotations = self.load_annotation(ann_path)

                for ind,a_dir in enumerate(image_dirs):
                    self.images.append(a_dir)
                    self.image_ids.append(image_id)
                    boxmap_path=os.path.join(other_source,a_video,'masks',str(int(a_dir.split('/')[-1].split('.png')[0]))+'.png')
                    if os.path.isfile(boxmap_path)==True:
                        self.boxmap.append(boxmap_path)
                    else:
                        print("Error")

                    frame_boxes = self.get_single_same_frame_objects(frm_id)
                    self.targets.append(frame_boxes)
                    frm_id = frm_id+1
                    image_id = image_id + 1
        # print(len(self.images))
        # print(len(self.images))
        total_img = 10 #int(len(self.images)/4)*4
        self.images = self.images[0:total_img]
        self.targets = self.targets[0:total_img]
        self.boxmap = self.boxmap[0:total_img]
        # self.det_maps = self.det_maps[0:total_img]
        self.image_ids = self.image_ids[0:total_img]
        self.records = self.targets
        self.size = len(self.images)
        # pdb.set_trace()
    def __getitem__(self, index):
        if self.training:
            if_flap = np.random.randint(2) == 1
        else:
            if_flap = False
        # image

        image_path = self.images[index] #os.path.join(self.source, record['ID']+'.jpg') # Chaged from PNG
        image = misc_utils.load_img(image_path)
        boxmap = cv2.imread(self.boxmap[index],cv2.IMREAD_UNCHANGED)
        # print(image_path,self.boxmap[index])
        image_h = image.shape[0]
        image_w = image.shape[1]
        if if_flap:
            image = cv2.flip(image, 1)
            boxmap = cv2.flip(boxmap,1)
        if self.training:
            # ground_truth
            gtboxes = self.targets[index] #misc_utils.load_gt(record, 'gtboxes', 'fbox', self.config.class_names)
            # keep = (gtboxes[:, 2]>=0) * (gtboxes[:, 3]>=0)
            # gtboxes=gtboxes[keep, :]
            # gtboxes[:, 2:4] += gtboxes[:, :2]
            gtboxes = np.array(gtboxes)
            if if_flap:
                gtboxes = flip_boxes(gtboxes, image_w)
            # im_info
            gtboxes = gtboxes.astype('float64') 
            nr_gtboxes = gtboxes.shape[0]
            im_info = np.array([0, 0, 1, image_h, image_w, nr_gtboxes])
            return image, gtboxes, im_info,boxmap
        else:
            gtboxes = self.targets[index] #misc_utils.load_gt(record, 'gtboxes', 'fbox', self.config.class_names)
            image = image.transpose(2, 0, 1)
            image = torch.tensor(image).float()
            gtboxes = np.array(gtboxes)
            gtboxes = gtboxes.astype('float64')
            target = {}
            target["boxes"] = torch.from_numpy(gtboxes[:,0:4])
            target["labels"] = torch.ones((len(gtboxes),), dtype=torch.int64)
            target["area"] = (gtboxes[:, 3] - gtboxes[:, 1]) * (gtboxes[:, 2] - gtboxes[:, 0])
            target["image_id"] = torch.tensor([self.image_ids[index]])
            target["iscrowd"] = torch.zeros((len(gtboxes),), dtype=torch.int64)
            # gtboxes = torch.tensor(gtboxes)
            
            # keep = (gtboxes[:, 2]>=0) * (gtboxes[:, 3]>=0)
            # gtboxes=gtboxes[keep, :]
            # gtboxes[:, 2:4] += gtboxes[:, :2]
            # im_info
            nr_gtboxes = gtboxes.shape[0]
            im_info = np.array([0, 0, 1, image_h, image_w, nr_gtboxes])
            return image, target, im_info,boxmap#,self.image_ids[index]
    def validation_annotation(self):
        for a_id in self.image_ids:
            for ind,a_box in enumerate(self.targets[a_id]):
                single_box ={"fbox":a_box,"tag":"person","h_box":a_box,"extra":None,"vbox":a_box,"head_attr":None}
    def __len__(self):
        return self.size



    def merge_batch(self, data):
        # image
        images = [it[0] for it in data]

        gt_boxes = [it[1] for it in data]

        im_info = np.array([it[2] for it in data])
        boxmaps=[it[3] for it in data]
        batch_height = np.max(im_info[:, 3])
        batch_width = np.max(im_info[:, 4])
        padded_images = [pad_image(
                im, batch_height, batch_width, self.config.image_mean) for im in images]
        t_height, t_width, scale = target_size(
                batch_height, batch_width, self.short_size, self.max_size)
        # INTER_CUBIC, INTER_LINEAR, INTER_NEAREST, INTER_AREA, INTER_LANCZOS4
        resized_images = np.array([cv2.resize(
                im, (t_width, t_height), interpolation=cv2.INTER_LINEAR) for im in padded_images])
        resized_images = resized_images.transpose(0, 3, 1, 2)
        images = torch.tensor(resized_images).float()

        # Boxmap code
        
        padded_boxmaps = [pad_image(
                boxmap, batch_height, batch_width, 0) for boxmap in boxmaps]        
        resized_boxmaps = np.array([cv2.resize(
                boxmap, (t_width, t_height), interpolation=cv2.INTER_LINEAR) for boxmap in padded_boxmaps])
        # resized_boxmaps = resized_boxmaps.transpose(0, 3, 1, 2)
        boxmaps = torch.tensor(resized_boxmaps).float()
        # boxmaps=boxmaps/10
        # ground_truth
        ground_truth = []
        for it in gt_boxes:
            gt_padded = np.zeros((self.config.max_boxes_of_image, self.config.nr_box_dim))
            it[:, 0:4] *= scale
            max_box = min(self.config.max_boxes_of_image, len(it))
            gt_padded[:max_box] = it[:max_box]
            ground_truth.append(gt_padded)
        ground_truth = torch.tensor(ground_truth).float()
        # im_info
        im_info[:, 0] = t_height
        im_info[:, 1] = t_width
        im_info[:, 2] = scale
        im_info = torch.tensor(im_info)
        if max(im_info[:, -1] < 2):
            return None, None, None,None
        else:
            return images, ground_truth, im_info,boxmaps

    def load_annotation(self,ann_path):
        annotations = []
        with open(ann_path, 'r') as csvfile:
            csv_read = csv.reader(csvfile, delimiter = ',')
            next(csv_read)
            for row in csv_read:
                if row[2]!= 'Others' and row[2]!= 'other' and row[2] != 'others' and row[2]!= 'Other':
                    x = int(float(row[10]))
                    y = int(float(row[9]))
                    width = int(float(row[11]))
                    height = int(float(row[12]))
                    frame_number = int(row[4].replace(".png",''))
                    annotations.append([frame_number,x,y,x+width,y+height])
        annotations = self.same_frame_objects(annotations)
        return annotations
    def same_frame_objects(self,annotations):
        framewise_order = sorted(annotations,key=lambda x: int(x[0]))
        prev_frame_number = 1
        single_frame_objs = []
        self.frame_objs = []
        for i in range(len(framewise_order)):
            if int(framewise_order[i][0])==prev_frame_number:
                single_frame_objs.append([framewise_order[i][1],framewise_order[i][2],framewise_order[i][3],framewise_order[i][4],1])
            else:
                if len(single_frame_objs)==0:
                    self.frame_objs.append("empty")
                else:
                    self.frame_objs.append(single_frame_objs)
                single_frame_objs=[]
                single_frame_objs.append([framewise_order[i][1],framewise_order[i][2],framewise_order[i][3],framewise_order[i][4],1])
            prev_frame_number=int(framewise_order[i][0])
        if len(single_frame_objs)==0:
            self.frame_objs.append("empty")
        else:
            self.frame_objs.append(single_frame_objs)
        return
    def get_single_same_frame_objects(self,frame_number):
        return self.frame_objs[int(frame_number)-1]  
def target_size(height, width, short_size, max_size):
    im_size_min = np.min([height, width])
    im_size_max = np.max([height, width])
    scale = (short_size + 0.0) / im_size_min
    if scale * im_size_max > max_size:
        scale = (max_size + 0.0) / im_size_max
    t_height, t_width = int(round(height * scale)), int(
        round(width * scale))
    return t_height, t_width, scale

def flip_boxes(boxes, im_w):
    flip_boxes = boxes.copy()
    for i in range(flip_boxes.shape[0]):
        flip_boxes[i, 0] = im_w - boxes[i, 2] - 1
        flip_boxes[i, 2] = im_w - boxes[i, 0] - 1
    return flip_boxes

def pad_image(img, height, width, mean_value):
    if len(img.shape)==3:
        o_h, o_w, _ = img.shape
    if len(img.shape)==2:
        o_h, o_w = img.shape
    margins = np.zeros(2, np.int32)
    assert o_h <= height
    margins[0] = height - o_h
    img = cv2.copyMakeBorder(
        img, 0, margins[0], 0, 0, cv2.BORDER_CONSTANT, value=0)
    if len(img.shape)==3:
        img[o_h:, :, :] = mean_value
    if len(img.shape)==2:
        img[o_h:, :] = mean_value
    assert o_w <= width
    margins[1] = width - o_w
    img = cv2.copyMakeBorder(
        img, 0, 0, 0, margins[1], cv2.BORDER_CONSTANT, value=0)
    if len(img.shape)==3:
        img[:, o_w:, :] = mean_value
    if len(img.shape)==2:
        img[:, o_w:] = mean_value
    return img
