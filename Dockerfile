FROM nvidia/cuda:9.2-base-ubuntu18.04

# Install some basic utilities
RUN apt-get update && apt-get install -y \
    curl \
    ca-certificates \
    sudo \
    git \
    bzip2 \
    libx11-6 \
 && rm -rf /var/lib/apt/lists/*

# Create a working directory
RUN mkdir /app
WORKDIR /app

# Create a non-root user and switch to it
RUN adduser --disabled-password --gecos '' --shell /bin/bash user \
 && chown -R user:user /app
RUN echo "user ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/90-user
USER user

# All users can use /home/user as their home directory
ENV HOME=/home/user
RUN chmod 777 /home/user

# Install Miniconda and Python 3.8
ENV CONDA_AUTO_UPDATE_CONDA=false
ENV PATH=/home/user/miniconda/bin:$PATH
RUN curl -sLo ~/miniconda.sh https://repo.continuum.io/miniconda/Miniconda3-py38_4.8.2-Linux-x86_64.sh \
 && chmod +x ~/miniconda.sh \
 && ~/miniconda.sh -b -p ~/miniconda \
 && rm ~/miniconda.sh \
 && conda install -y python==3.8.1 \
 && conda clean -ya

# CUDA 9.2-specific steps
RUN conda install -y -c pytorch \
    cudatoolkit=9.2 \
    "pytorch=1.5.0=py3.8_cuda9.2.148_cudnn7.6.3_0" \
    "torchvision=0.6.0=py38_cu92" \
 && conda clean -ya

RUN pip install --no-cache-dir numpy scipy pandas matplotlib opencv-contrib-python tensorboard natsort git+https://github.com/qubvel/segmentation_models.pytorch

RUN sudo apt-get update
RUN sudo apt-get install 'ffmpeg'\
    'libsm6'\ 
    'libxext6'  -y

# Install other dependencies from pip
#COPY requirements.txt .
#RUN pip install -r requirements.txt

# Create /data directory so that a container can be run without volumes mounted
# RUN sudo mkdir /dsort_rand_det_F && sudo chown user:user /dsort_rand_det_F
# RUN sudo mkdir /ann_dsort_F && sudo chown user:user /ann_dsort_F
# RUN sudo mkdir /dsort_val_det_S && sudo chown user:user /dsort_val_det_S
# RUN sudo mkdir /first_half_hockey && sudo chown user:user /first_half_hockey
# RUN sudo mkdir /new_random && sudo chown user:user /new_random
RUN sudo mkdir /data && sudo chown user:user /data
# Copy source code into the image
COPY --chown=user:user . /app

# Set the default command to python3
CMD ["python3"]