import torch
from torch import nn
import torch.nn.functional as F
import numpy as np

from config import config
from backbone.resnet50 import ResNet50
from backbone.fpn import FPN
from module.rpn import RPN
from layers.pooler import roi_pooler
from det_oprs.bbox_opr import bbox_transform_inv_opr
from det_oprs.fpn_roi_target import fpn_roi_target
from det_oprs.loss_opr import emd_loss_softmax,emd_loss_softmax_k
from det_oprs.utils import get_padded_tensor
import pdb
class Network(nn.Module):
    def __init__(self):
        super().__init__()
        self.resnet50 = ResNet50(config.backbone_freeze_at, False)
        self.FPN = FPN(self.resnet50, 2, 6)
        self.RPN = RPN(config.rpn_channel)
        self.RCNN = RCNN()
        assert config.num_classes == 2, 'Only support two class(1fg/1bg).'

    def forward(self, image, im_info, gt_boxes=None):
        image = (image - torch.tensor(config.image_mean[None, :, None, None]).type_as(image)) / (
                torch.tensor(config.image_std[None, :, None, None]).type_as(image))
        image = get_padded_tensor(image, 64)
        if self.training:
            return self._forward_train(image, im_info, gt_boxes)
        else:
            return self._forward_test(image, im_info)

    def _forward_train(self, image, im_info, gt_boxes):
        loss_dict = {}
        fpn_fms = self.FPN(image)
        # fpn_fms stride: 64,32,16,8,4, p6->p2
        rpn_rois, loss_dict_rpn = self.RPN(fpn_fms, im_info, gt_boxes)
        rcnn_rois, rcnn_labels, rcnn_bbox_targets = fpn_roi_target(
                rpn_rois, im_info, gt_boxes, top_k=config.k_size)
        loss_dict_rcnn = self.RCNN(fpn_fms, rcnn_rois,
                rcnn_labels, rcnn_bbox_targets)
        loss_dict.update(loss_dict_rpn)
        loss_dict.update(loss_dict_rcnn)
        return loss_dict

    def _forward_test(self, image, im_info):
        fpn_fms = self.FPN(image)
        rpn_rois = self.RPN(fpn_fms, im_info)
        pred_bbox = self.RCNN(fpn_fms, rpn_rois)
        return pred_bbox.cpu().detach()

class RCNN(nn.Module):
    def __init__(self):
        super().__init__()
        # roi head
        self.fc1 = nn.Linear(256*7*7, 1024)
        self.fc2 = nn.Linear(1024, 1024)
        self.fc3 = nn.Linear(1044, 1024)
        for l in [self.fc1, self.fc2, self.fc3]:
            nn.init.kaiming_uniform_(l.weight, a=1)
            nn.init.constant_(l.bias, 0)
        # box predictor
        self.emd_pred_cls = []
        self.emd_pred_delta = []
        self.ref_pred_cls = []
        self.ref_pred_delta = []
        self.cls_layer = nn.Linear(1024, config.num_classes)
        self.delta_layer = nn.Linear(1024, config.num_classes * 4)
        for i in range(config.k_size):
            self.emd_pred_cls.append(self.cls_layer)
            self.emd_pred_delta.append(self.delta_layer)
            self.ref_pred_cls.append(self.cls_layer)
            self.ref_pred_delta.append(self.delta_layer)
        emd_cls_branches = [self.emd_pred_cls[i] for i in range(config.k_size)]
        emd_delta_branches =  [self.emd_pred_delta[i] for i in range(config.k_size)]
        ref_cls_branches = [self.ref_pred_cls[i] for i in range(config.k_size)]
        ref_delta_branches =  [self.ref_pred_delta[i] for i in range(config.k_size)]
        cls_branches = emd_cls_branches + ref_cls_branches
        delta_branches = emd_delta_branches + ref_delta_branches
        for l in cls_branches:
            nn.init.normal_(l.weight, std=0.001)
            nn.init.constant_(l.bias, 0)
        for l in delta_branches:
            nn.init.normal_(l.weight, std=0.001)
            nn.init.constant_(l.bias, 0)

    def forward(self, fpn_fms, rcnn_rois, labels=None, bbox_targets=None):
        # stride: 64,32,16,8,4 -> 4, 8, 16, 32
        fpn_fms = fpn_fms[1:][::-1]
        stride = [4, 8, 16, 32]
        pool_features = roi_pooler(fpn_fms, rcnn_rois, stride, (7, 7), "ROIAlignV2")
        flatten_feature = torch.flatten(pool_features, start_dim=1)
        flatten_feature = F.relu_(self.fc1(flatten_feature))
        flatten_feature = F.relu_(self.fc2(flatten_feature))
        pred_emd_cls = []
        pred_emd_delta = []
        pred_emd_scores = []
        for i in range(config.k_size):
            pred_emd_cls.append(self.emd_pred_cls[i](flatten_feature))
            pred_emd_delta.append(self.emd_pred_delta[i](flatten_feature))
        for i in range(config.k_size):
            pred_emd_scores.append(F.softmax(pred_emd_cls[i],dim=-1))

        # cons refine feature
        boxes_feature = []
        for i in range(config.k_size):
            boxes_feature.append(torch.cat((pred_emd_delta[i][:,4:],pred_emd_scores[i][:,1][:,None]),dim=1).repeat(1,4))

        for i in range(config.k_size):
            boxes_feature[i] = torch.cat((flatten_feature,boxes_feature[i]),dim=1)
        refine_feature = []
        for i in range(config.k_size):
            refine_feature.append(F.relu_(self.fc3(boxes_feature[i])))
        pred_ref_cls = []
        pred_ref_delta = []
        for i in range(config.k_size):
            pred_ref_cls.append(self.ref_pred_cls[i](refine_feature[i]))
            pred_ref_delta.append(self.ref_pred_delta[i](refine_feature[i]))
        if self.training:
            loss_rcnn = emd_loss_softmax_k(pred_emd_delta,pred_emd_cls,bbox_targets,labels)
            loss_ref = emd_loss_softmax_k(pred_ref_delta,pred_ref_cls,bbox_targets,labels)
            _, min_indices_rcnn = loss_rcnn.min(axis=1)
            _, min_indices_ref = loss_ref.min(axis=1)
            loss_rcnn = loss_rcnn[torch.arange(loss_rcnn.shape[0]), min_indices_rcnn]
            loss_rcnn = loss_rcnn.mean()
            loss_ref = loss_ref[torch.arange(loss_ref.shape[0]), min_indices_ref]
            loss_ref = loss_ref.mean()
            loss_dict = {}
            loss_dict['loss_rcnn_emd'] = loss_rcnn
            loss_dict['loss_ref_emd'] = loss_ref
            return loss_dict
        else:
            class_num = pred_ref_cls[0].shape[-1] - 1
            tag = torch.arange(class_num).type_as(pred_ref_cls[0])+1
            tag = tag.repeat(pred_ref_cls[0].shape[0], 1).reshape(-1,1)
            pred_scores = []
            pred_deltas = []
            for i in range(config.k_size):
                pred_scores.append(F.softmax(pred_ref_cls[0], dim=-1)[:, 1:].reshape(-1, 1))
                pred_deltas.append(pred_ref_delta[i][:, 4:].reshape(-1, 4))
            base_rois = rcnn_rois[:, 1:5].repeat(1, class_num).reshape(-1, 4)
            pred_bboxes = []
            for i in range(config.k_size):
                pred_bboxes.append(restore_bbox(base_rois, pred_deltas[i], True))
            for i in range(config.k_size):
                if i==0:
                    pred_bbox = torch.cat([pred_bboxes[i], pred_scores[i], tag], axis=1)
                else:
                    pbox = torch.cat([pred_bboxes[i], pred_scores[i], tag], axis=1)
                    pred_bbox = torch.cat((pred_bbox,pbox),axis=1)
            return pred_bbox

def restore_bbox(rois, deltas, unnormalize=True):
    if unnormalize:
        std_opr = torch.tensor(config.bbox_normalize_stds[None, :]).type_as(deltas)
        mean_opr = torch.tensor(config.bbox_normalize_means[None, :]).type_as(deltas)
        deltas = deltas * std_opr
        deltas = deltas + mean_opr
    pred_bbox = bbox_transform_inv_opr(rois, deltas)
    return pred_bbox
