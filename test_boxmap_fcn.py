import argparse
import os

import torch
import torch.backends.cudnn
from torch.nn import DataParallel
from torch.optim.rmsprop import RMSprop
import torch.optim 
from torch.utils.data import DataLoader
from tqdm import trange, tqdm
# from stacked_hourglass.model import hg1, hg2, hg8
from torch.utils.data import WeightedRandomSampler

# from stacked_hourglass.datasets.mpii import Mpii
# from stacked_hourglass.datasets.CrowdHuman import CrowdHuman
from boxmap_model.datasets.sports_boxmap_train import sports
from boxmap_model.datasets.CrowdHuman_boxmap_train import CrowdHuman
from boxmap_model.datasets.motdet_boxmap_train import motdet20

# from boxmap_model.datasets.CrowdHuman import CrowdHuman

# from boxmap_model.datasets.motdet import motdet20
# from stacked_hourglass.train import do_training_epoch, do_validation_epoch
from boxmap_model.train import do_training_epoch, do_validation_epoch
# from stacked_hourglass.pinball_loss import PinballLoss

from boxmap_model.utils.logger import Logger
from boxmap_model.utils.misc import save_checkpoint, adjust_learning_rate
from boxmap_model.resnet_fcn import ResNetFcn
from boxmap_model.seg_models import DeepLabV3Plus_sub,PAN_sub
import torchvision.models as models
import pdb
import random
from torch.utils.tensorboard import SummaryWriter
import torch.optim.lr_scheduler as lr_scheduler
from boxmap_model.utils.misc import multi_size_collate
from pathlib import Path
import sys
import numpy as np
# sys.path.insert(0, './experiment')
# import tests

def main(args):
	# Select the hardware device to use for inference.
	os.environ["CUDA_VISIBLE_DEVICES"]="0"
	device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

	torch.backends.cudnn.benchmark = True
	torch.manual_seed(1234)
	torch.cuda.manual_seed(1234)
	np.random.seed(1234)
	# Disable gradient calculations by default.
	torch.set_grad_enabled(False)

	# create checkpoint dir
	os.makedirs(args.checkpoint, exist_ok=True)
	train_output_stride = 16
	eval_output_stride = 8
	in_channels=3
	if args.use_detmap==True:
		in_channels=4
	if args.use_scoremap==True:
		in_channels=9
	if args.arch == 'hg1':
		model = hg1(pretrained=False)
	elif args.arch == 'hg2':
		model = hg2(pretrained=False)
	elif args.arch == 'hg8':
		model = hg8(pretrained=True)
	elif args.arch == 'resnet_fcn':
		model = ResNetFcn(models.resnet34(pretrained=True), train_output_stride, eval_output_stride,4)
	elif args.arch == 'deeplabv3plus':
		model = DeepLabV3Plus_sub(encoder_name='resnet34',encoder_weights='imagenet',encoder_output_stride=eval_output_stride,in_channels=in_channels)
		# preprocessing_fn = DeepLabV3Plus_sub.encoders.get_preprocessing_fn("resnet18", "imagenet")
	elif args.arch == 'pan':
		model = PAN_sub(encoder_name='resnet34',decoder_channels=64)
	else:
		raise Exception('unrecognised model architecture: ' + args.arch)
	if args.dataset == "CrowdHuman":
		dataset = CrowdHuman
		val_dataset = dataset(args.image_path, dset="train",is_train=True)
	elif args.dataset == "sports":
		dataset = sports
		val_dataset = dataset(args.image_path, dset="val",is_train=False,use_detmap=args.use_detmap,use_scoremap=args.use_scoremap,other_sports=False,others_path=args.other_sports_path)
	elif args.dataset == "motdet20":
		dataset = motdet20
		val_dataset = dataset(args.image_path, dset="train",is_train=True)
	elif args.dataset == 'crowd_mot20':
		mot_dataset = motdet20(args.mot_path, dset="train",is_train=True,use_detmap=T)
		crowd_dataset = CrowdHuman(args.crowd_path, dset="train",is_train=True,use_detmap=False)
		val_dataset = torch.utils.data.ConcatDataset([crowd_dataset, mot_dataset])
	elif args.dataset == 'crowd_mot20_sports':
		sports_dataset = sports(args.image_path, dset="train",is_train=True,use_detmap=True,other_sports=True,others_path=args.other_sports_path)
		mot_dataset = motdet20(args.mot_path, dset="train",is_train=True,use_detmap=True)
		crowd_dataset = CrowdHuman(args.crowd_path, dset="train",is_train=True,use_detmap=True)
		crowd_dataset2 = CrowdHuman(args.crowd_path, dset="val",is_train=True,use_detmap=True)		
		val_dataset = torch.utils.data.ConcatDataset([crowd_dataset, crowd_dataset2, mot_dataset, sports_dataset])
		#total_data = len(train_dataset)
		#keep = int(len(train_dataset)/args.train_batch)*args.train_batch
		#train_dataset = train_dataset[0:keep]
	else:
		raise Exception('unrecognised dataset')
	# if torch.cuda.device_count() > 1:
	#   print("Let's use", torch.cuda.device_count(), "GPUs!")
	model = DataParallel(model)
	model.load_state_dict(torch.load("./results/deeplabv3plus_all_sports_5_scoremap_emd_r_resnet34_s_16_no_t_both.pth")['state_dict'])
	model = model.to(device)
	# if len(args.gpus)>1:
	# 	model = DataParallel(model).to(device)
	# else:
	#model = model.to(device)
	# state_dict = torch.load('./results/motdet_resnet34_s_8_t_equal_image.pth',map_location='cpu')
	# model.load_state_dict(state_dict['state_dict'])

	criterion = torch.nn.MSELoss()
	classes = torch.tensor([0,1,2,3,4,5,6,7]).to(device)
	weights = torch.tensor([0.125, 0.125, 0.125, 0.125, 0.125, 0.125, 0.125, 0.125]).to(device)

	optimizer = torch.optim.Adam(model.parameters(), lr=args.lr, betas=(0.9, 0.999))


	# val_dataset = sports(args.image_path, dset="val",is_train=False,use_detmap=False,other_sports=True,others_path=args.other_sports_path)
	val_loader = DataLoader(
		val_dataset,
		batch_size=args.test_batch, shuffle=False,
		num_workers=args.workers,pin_memory=True,drop_last=True
	)
	for epoch in trange(args.start_epoch, args.epochs, desc='Overall', ascii=True):
		# train_loss = do_training_epoch(train_loader, model, device,optimizer,train_output_stride,eval_output_stride,criterion,classes,weights)
		# if epoch%10==0:
		val_loss,class_losses = do_validation_epoch(val_loader, model, device,train_output_stride,eval_output_stride,criterion,classes,weights,False)
		print(val_loss)
		# train_writer.add_scalar('mse loss', train_loss, epoch)
		# val_writer.add_scalar('mse loss', val_loss, epoch)
		# for inds,_ in enumerate(class_losses):
		# 	if class_losses[inds]!=None:
		# 		val_writer.add_scalar(str(inds),class_losses[inds],epoch)

		# if epoch!=0 and epoch%15==0:
		# 	for param_group in optimizer.param_groups:
		# 		param_group['lr']=lr/10

		# train_writer.add_scalar('pinball_augment_loss', train_loss, epoch)
		# val_writer.add_scalar('pinball_augment_loss', val_loss, epoch)
		# evaluate on validation set
		# valid_loss = do_validation_epoch(val_loader, model, device,train_output_stride,eval_output_stride,False)

		# print metrics
		# tqdm.write(f'[{epoch + 1:3d}/{args.epochs:3d}] lr={lr:0.2e} '
		#            f'train_loss={train_loss:0.4f} train_acc={100 * train_acc:0.2f} '
		#            f'valid_loss={valid_loss:0.4f} valid_acc={100 * valid_acc:0.2f}')
		# tqdm.write(f'[{epoch + 1:3d}/{args.epochs:3d}] lr={lr:0.2e} '
		#            f'train_loss={train_loss:0.4f}'
		#            f'valid_loss={valid_loss:0.4f}')
		# tqdm.write(f'[{epoch + 1:3d}/{args.epochs:3d}] lr={lr:0.6} '
		# 		   f'train_loss={train_loss:0.4f} '
		# 		   f'val_loss={val_loss:0.4f} ')
				   # f'weighted_validation_loss={weighted_loss:0.4f}')
			# append logger file
			# logger.append([epoch + 1, lr, train_loss, valid_loss, train_acc, valid_acc])
			# logger.plot_to_file(os.path.join(args.checkpoint, 'log.svg'), ['Train Acc', 'Val Acc'])
			# is_best = valid_loss < best_loss
			# best_loss = min(valid_loss,valid_loss)
			# remember best acc and save checkpoint
			# is_best = valid_acc > best_acc
			# best_acc = max(valid_acc, best_acc)
			# filename='checkpoint_'+str(int(float(epoch)))+'.pth'




if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Train a stacked hourglass model.')
	# Dataset setting
	parser.add_argument('--image-path', default='/media/sohel/HDD2/googledrive/detection_sports/netball_bbl/netball', type=str,
						help='path to images')
	parser.add_argument('--gpus', nargs='*', type=str, default=['0','1'],
						help='path to images')
	# parser.add_argument('--image-path', default='/data/netball', type=str,
	# 					help='path to images')
	# parser.add_argument('--image-path', default='/data/crowdHuman', type=str,
	# 					help='path to images')
	parser.add_argument('--spec', default='deeplabv3plus_pretrain_all_sports_emd_r_resnet34_s_16_no_t_both', type=str,
						help='Specify for tensorboard')
	parser.add_argument('--crowd_path', default='/data/crowdHuman', type=str,
						help='path to images')
	parser.add_argument('--mot_path', default='/data/MOT20', type=str,
						help='path to images')
	parser.add_argument('--other_sports_path', default='/media/sohel/HDD2/googledrive/detection_sports/netball_bbl/other_sports', type=str,
						help='path to images')
	# Model structure
	parser.add_argument('--dataset', default="sports", type=str, help='Name of the dataset used for')
	parser.add_argument('--use_scoremap', default=True, type=bool, help='Name of the dataset used for')
	parser.add_argument('--use_detmap', default=True, type=bool, help='Name of the dataset used for')
	parser.add_argument('--arch', '-a', metavar='ARCH', default='deeplabv3plus',
						choices=['hg1', 'hg2', 'hg8','resnet_fcn','deeplabv3plus','pan'],
						help='model architecture')
	# Training strategy
	parser.add_argument('-j', '--workers', default=8, type=int, metavar='N',
						help='number of data loading workers (default: 4)')
	parser.add_argument('--epochs', default=1, type=int, metavar='N',
						help='number of total epochs to run')
	parser.add_argument('--start-epoch', default=0, type=int, metavar='N',
						help='manual epoch number (useful on restarts)')
	parser.add_argument('--train-batch', default= 1, type=int, metavar='N',
						help='train batchsize')
	parser.add_argument('--test-batch', default= 1, type=int, metavar='N',
						help='test batchsize')
	parser.add_argument('--lr', '--learning-rate', default=2.5e-3, type=float, #0.00001 2.5e-4,.001
						metavar='LR', help='initial learning rate')
	parser.add_argument('--momentum', default=0, type=float, metavar='M',
						help='momentum')
	parser.add_argument('--weight-decay', '--wd', default=0, type=float,
						metavar='W', help='weight decay (default: 0)')
	parser.add_argument('--schedule', type=int, nargs='+', default=[60, 90],
						help='Decrease learning rate at these epochs.')
	parser.add_argument('--gamma', type=float, default=0.1,
						help='LR is multiplied by gamma on schedule.')
	# Miscs
	parser.add_argument('-c', '--checkpoint', default='checkpoint', type=str, metavar='PATH',
						help='path to save checkpoint (default: checkpoint)')
	parser.add_argument('--snapshot', default=0, type=int,
						help='save models for every #snapshot epochs (default: 0)')
	parser.add_argument('--resume', default='', type=str, metavar='PATH',
						help='path to latest checkpoint (default: none)')

	main(parser.parse_args())
