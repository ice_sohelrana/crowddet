import torch
import torch.backends.cudnn
import torch.nn.parallel
import torch.nn as nn
from tqdm import tqdm
import matplotlib
# from stacked_hourglass.loss import joints_mse_loss,boxmap_mse_loss,boxmap_mse_loss_fcn
# from stacked_hourglass.pinball_loss import PinballLoss
from boxmap_model.utils.evaluation import accuracy, AverageMeter, final_preds
from boxmap_model.utils.transforms import fliplr, flip_back
import os
import pdb
from PIL import Image, ImageDraw
import matplotlib.pyplot as plt
import numpy as np
from torchvision import transforms
import os
# from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
# from sklearn.preprocessing import normalize
os.environ["CUDA_VISIBLE_DEVICES"]="0,1"

def weighted_mse_loss(inputs, target, classes,weights):
	# weight = [0.1135, 0.1148, 0.1159, 0.1175, 0.1201, 0.1251, 0.1335, 0.1595]
	se=(inputs-target)**2
	
	for inds,a_class in enumerate(classes):
		se[target==a_class]=se[target==a_class]*weights[inds]
	loss = se.mean() #torch.sum(se) #se.mean()
	# del classes
	# del se
	# del weight
	return loss
def evaluate_class_loss(inputs,target,classes):
	class_losses = {}
	se=(inputs-target)**2
	for i,a_class in enumerate(classes):
		class_loss = se[target==a_class]
		class_losses[i] = class_loss.mean() #torch.sum(class_loss)
	return class_losses

def do_training_step(model, optimiser, input, target,tls,brs, train_output_stride,eval_output_stride,criterion,classes,weights,target_weight=None):
	assert model.training, 'model must be in training mode.'
	assert len(input) == len(target), 'input and target must contain the same number of examples.'

	with torch.enable_grad():
		# Forward pass and loss calculation.
		output = model(input)
		# 
		brs=brs.float()
		tls=tls.float()
		target=target.float()
		target=torch.cat((target,tls,brs),dim=1)

		loss = criterion(output, target)

		# Backward pass and parameter update.
		optimiser.zero_grad()
		loss.backward()
		optimiser.step()
	# pdb.set_trace()
	return output, loss.item()


def do_training_epoch(train_loader, model, device, optimiser,train_output_stride,eval_output_stride,criterion,classes,weights,quiet=False):
	losses = AverageMeter()
	accuracies = AverageMeter()
	losses.update(0, 1)
	# Put the model in training mode.
	model.train()

	iterable = enumerate(train_loader)
	progress = None
	if not quiet:
		progress = tqdm(iterable, desc='Train', total=len(train_loader), ascii=True, leave=False)
		iterable = progress

	for i, (inputs, target,im_info,tls,brs) in iterable:
		input, target = inputs.to(device), target.to(device)
		tls, brs = tls.to(device), brs.to(device)
		output, loss = do_training_step(model, optimiser, input, target,tls,brs,train_output_stride,eval_output_stride,criterion,classes,weights) #, target_weight)
		# measure accuracy and record loss
		losses.update(loss, input.size(0))

		# Show accuracy and loss as part of the progress bar.
		if progress is not None:
			progress.set_postfix_str('Loss: {loss:0.4f}'.format(
				loss=losses.avg,
			))
	print(losses.avg)
	return losses.avg


def do_validation_step(model, input, target, train_output_stride,eval_output_stride, criterion,target_weight=None, flip=False):
	assert not model.training, 'model must be in evaluation mode.'
	assert len(input) == len(target), 'input and target must contain the same number of examples.'

	# Forward pass and loss calculation.
	output = model(input)

	# Get the heatmaps.
	flip=False
	if flip:
		# If `flip` is true, perform horizontally flipped inference as well. This should
		# result in more robust predictions at the expense of additional compute.
		flip_input = fliplr(input)
		flip_output = model(flip_input)
		flip_output = flip_output[-1].cpu()
		flip_output = flip_back(flip_output.detach(), data_info.hflip_indices)
		heatmaps = (output[-1].cpu() + flip_output) / 2
	else:
		heatmaps = output[-1].cpu()

	return output#, loss.item()

@torch.no_grad()
def do_validation_epoch(val_loader, model, device,train_output_stride,eval_output_stride, criterion,classes,weights,flip=False, quiet=False):
	losses = AverageMeter()
	accuracies = AverageMeter()
	predictions = [None] * len(val_loader.dataset)
	losses.update(0, 1)
	weighted_losses = AverageMeter()
	weighted_losses.update(0, 1)
	# Put the model in evaluation mode.
	model.eval()

	iterable = enumerate(val_loader)
	progress = None
	if not quiet:
		progress = tqdm(iterable, desc='Valid', total=len(val_loader), ascii=True, leave=False)
		iterable = progress
	count = 500
	out_list = []
	targ_list = []
	losses.avg = 0
	weighted_losses.avg = 0
	class_losses = {}
	for a_class in [0,1,2,3,4,5,6,7]:
		class_losses[a_class] = []
	all_img_confusion_matrix = np.zeros((8,8))
	for i, (inputs, target,im_info,tls,brs) in iterable:
		# if inputs.shape[2]*inputs.shape[3]<17000000:
		# Copy data to the training device (eg GPU).
		brs=brs.float()
		tls=tls.float()
		target=target.float()
		input = inputs.to(device, non_blocking=True)
		target = target.to(device, non_blocking=True)
		# pdb.set_trace()
		tls, brs = tls.to(device, non_blocking=True), brs.to(device, non_blocking=True)
		
		# target_weight = meta['target_weight'].to(device, non_blocking=True)

		output = do_validation_step(model, input, target,train_output_stride,eval_output_stride,criterion)#, target_weight, flip)
    

		# pdb.set_trace()
		# plt.imshow(output[0][1].cpu())
		# plt.savefig('./output/tls/pred/'+str(i)+'.png')
		# plt.imshow(output[0][2].cpu())
		# plt.savefig('./output/trs/pred/'+str(i)+'.png')
		# plt.imshow(tls[0][0].cpu())
		# plt.savefig('./output/tls/gt/'+str(i)+'.png')
		# plt.imshow(brs[0][0].cpu())
		# plt.savefig('./output/trs/gt/'+str(i)+'.png')
		concatenated_target=torch.cat((target,tls,brs),dim=1)	
		loss = criterion(output,concatenated_target)
		# pdb.set_trace()
		class_loss = evaluate_class_loss(output[:,0:1,:,:],target,classes)
		for inds,_ in enumerate(class_loss):
			if torch.isnan(class_loss[inds])==False:
				class_losses[inds].append(class_loss[inds].item())
		
		losses.update(loss, input.size(0))
		# weighted_losses.update(weighted_loss,input.size(0))
		if progress is not None:
			progress.set_postfix_str('Loss: {loss:0.4f}'.format(
				loss=losses.avg#,weighted_loss=weighted_losses.avg
			))

	
	avg_class_losses = {}
	for inds,_ in enumerate(class_losses):
		if len(class_losses[inds])!=0:
			avg_class_losses[inds] = sum(class_losses[inds])/len(class_losses[inds])
		else:
			avg_class_losses[inds] = None
	
	print('validation class loss: {0}'.format(avg_class_losses))
	return losses.avg,avg_class_losses#,weighted_losses.avg#, accuracies.avg, predictions

def confusion_mat(preds,gts):
	conf_matrix = np.zeros((8,8))
	preds = torch.round(preds.cpu())
	preds[preds<=0]=0
	gts = gts.squeeze(dim=0).squeeze(dim=0)
	preds = preds.squeeze(dim=0).squeeze(dim=0)
	gts = gts[0:1080,]
	preds = preds[0:1080,]
	gts = gts.view(-1).cpu()
	preds = preds.view(-1)
	gts = gts.type(torch.int)
	preds = preds.type(torch.int)
	result_confusion_matrix = confusion_matrix(gts, preds)
	conf_matrix[:result_confusion_matrix.shape[0],:result_confusion_matrix.shape[1]]=result_confusion_matrix
	return conf_matrix
 #preds[gts==i]
