import os
import shutil

import scipy.io
import torch
import torch.nn.functional as F


def to_numpy(tensor):
    if torch.is_tensor(tensor):
        return tensor.detach().cpu().numpy()
    elif type(tensor).__module__ != 'numpy':
        raise ValueError("Cannot convert {} to numpy array"
                         .format(type(tensor)))
    return tensor


def to_torch(ndarray):
    if type(ndarray).__module__ == 'numpy':
        return torch.from_numpy(ndarray)
    elif not torch.is_tensor(ndarray):
        raise ValueError("Cannot convert {} to torch tensor"
                         .format(type(ndarray)))
    return ndarray


def save_checkpoint(state, preds, is_best, checkpoint='checkpoint', filename='checkpoint.pth.tar', snapshot=None):
    preds = to_numpy(preds)
    filepath = os.path.join(checkpoint, filename)
    torch.save(state, filepath)
    scipy.io.savemat(os.path.join(checkpoint, 'preds.mat'), mdict={'preds' : preds})

    if snapshot and state['epoch'] % snapshot == 0:
        shutil.copyfile(filepath, os.path.join(checkpoint, 'checkpoint_{}.pth.tar'.format(state['epoch'])))

    if is_best:
        shutil.copyfile(filepath, os.path.join(checkpoint, 'model_best.pth.tar'))
        scipy.io.savemat(os.path.join(checkpoint, 'preds_best.mat'), mdict={'preds' : preds})


def save_pred(preds, checkpoint='checkpoint', filename='preds_valid.mat'):
    preds = to_numpy(preds)
    filepath = os.path.join(checkpoint, filename)
    scipy.io.savemat(filepath, mdict={'preds' : preds})


def adjust_learning_rate(optimizer, epoch, lr, schedule, gamma):
    """Sets the learning rate to the initial LR decayed by schedule"""
    if epoch in schedule:
        lr *= gamma
        for param_group in optimizer.param_groups:
            param_group['lr'] = lr
    return lr
def multi_size_collate(batch,use_shared_memory=False):
    # import pdb
    
    images = [a_batch[0] for a_batch in batch]
    targets = [a_batch[1] for a_batch in batch]
    width = [a_batch[2] for a_batch in batch]
    height = [a_batch[3] for a_batch in batch]
    max_width = max(width)
    max_height = max(height)
    for indx,a_batch in enumerate(batch):
        width_pad = 0
        height_pad = 0
        if a_batch[2]<max_width:
            width_pad = max_width-a_batch[2]    
        if a_batch[3]<max_height:
            height_pad = max_height-a_batch[3]
        if width_pad>0 or height_pad>0:
            image = a_batch[0]
            target = a_batch[1]
            if width_pad>0:
                image = F.pad(input=image, pad=(0,width_pad), mode='constant', value=0)
                target = F.pad(input=target, pad=(0,width_pad), mode='constant', value=0)
            if height_pad>0:
                image = F.pad(input=image, pad=(0,0,0,height_pad), mode='constant', value=0)
                target = F.pad(input=target, pad=(0,0,0,height_pad), mode='constant', value=0)
            images[indx] = image
            targets[indx] = target
    images = torch.stack(images)
    targets = torch.stack(targets)
    return [images,targets, width,height]