import torch
import torch.backends.cudnn
import torch.nn.parallel
import torch.nn as nn
from tqdm import tqdm
import matplotlib
# from stacked_hourglass.loss import joints_mse_loss,boxmap_mse_loss,boxmap_mse_loss_fcn
# from stacked_hourglass.pinball_loss import PinballLoss
from boxmap_model.utils.evaluation import accuracy, AverageMeter, final_preds
from boxmap_model.utils.transforms import fliplr, flip_back
import os
import pdb
from PIL import Image, ImageDraw
import matplotlib.pyplot as plt
import numpy as np
from torchvision import transforms
import os
# from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
# from sklearn.preprocessing import normalize
os.environ["CUDA_VISIBLE_DEVICES"]="0,1"

def weighted_mse_loss(inputs, target, classes,weights):
	# weight = [0.1135, 0.1148, 0.1159, 0.1175, 0.1201, 0.1251, 0.1335, 0.1595]
	se=(inputs-target)**2
	
	for inds,a_class in enumerate(classes):
		se[target==a_class]=se[target==a_class]*weights[inds]
	loss = se.mean() #torch.sum(se) #se.mean()
	# del classes
	# del se
	# del weight
	return loss
def evaluate_class_loss(inputs,target,classes):
	class_losses = {}
	se=(inputs-target)**2
	for i,a_class in enumerate(classes):
		class_loss = se[target==a_class]
		class_losses[i] = class_loss.mean() #torch.sum(class_loss)
	return class_losses
# def evaluate_cross_entropy_class_loss(inputs,target):
# 	for i in range(8):
# 		targ = target[target==]
# 		inputs[:,i,:,:]-target[]

def do_training_step(model, optimiser, input, target, train_output_stride,eval_output_stride,criterion,classes,weights,target_weight=None):
	assert model.training, 'model must be in training mode.'
	assert len(input) == len(target), 'input and target must contain the same number of examples.'

	with torch.enable_grad():
		# Forward pass and loss calculation.
		output = model(input)
		# upsample = nn.Upsample(size=(output.shape[2]*train_output_stride,output.shape[3]*train_output_stride), mode='bilinear',align_corners=True)
		# output = upsample(output)
		# output = output.view(output.shape[0],output.shape[2],output.shape[3])
		# if output.shape[1]>target.shape[1]:
		# 	shape_diff = output.shape[1]-target.shape[1]
		# 	output = output[:,0:-shape_diff,:]
		# if output.shape[2]>target.shape[2]:
		# 	shape_diff = output.shape[2] - target.shape[2]
		# 	output = output[:,:,0:-shape_diff]
		loss = criterion(output, target)
		# loss = weighted_mse_loss(output,target,classes,weights)
		# loss = cross_entropy(output,target,classes,criterion)
		# loss = loss.mean()
		# loss = boxmap_mse_loss_fcn(output,target)
		# pdb.set_trace()
		# Backward pass and parameter update.
		optimiser.zero_grad()
		loss.backward()
		optimiser.step()
	# pdb.set_trace()
	return output, loss.item()


def do_training_epoch(train_loader, model, device, optimiser,train_output_stride,eval_output_stride,criterion,classes,weights,quiet=False):
	losses = AverageMeter()
	accuracies = AverageMeter()
	losses.update(0, 1)
	# Put the model in training mode.
	model.train()

	iterable = enumerate(train_loader)
	progress = None
	if not quiet:
		progress = tqdm(iterable, desc='Train', total=len(train_loader), ascii=True, leave=False)
		iterable = progress

	# for i, (inputs, target,img_path) in iterable:
	for i, (inputs, target,im_info) in iterable:
		input, target = inputs.to(device), target.to(device)
		output, loss = do_training_step(model, optimiser, input, target,train_output_stride,eval_output_stride,criterion,classes,weights) #, target_weight)


		# measure accuracy and record loss
		losses.update(loss, input.size(0))
		# accuracies.update(acc[0], input.size(0))

		# Show accuracy and loss as part of the progress bar.
		if progress is not None:
			progress.set_postfix_str('Loss: {loss:0.4f}'.format(
				loss=losses.avg,
				# acc=100 * accuracies.avg
			))
		#, Acc: {acc:6.2f}
	print(losses.avg)
	return losses.avg#, accuracies.avg


def do_validation_step(model, input, target, train_output_stride,eval_output_stride, criterion,target_weight=None, flip=False):
	assert not model.training, 'model must be in evaluation mode.'
	assert len(input) == len(target), 'input and target must contain the same number of examples.'

	# Forward pass and loss calculation.
	output = model(input)
	# pdb.set_trace()
	# loss = sum(joints_mse_loss(o, target, target_weight) for o in output)

	# Get the heatmaps.
	flip=False
	if flip:
		# If `flip` is true, perform horizontally flipped inference as well. This should
		# result in more robust predictions at the expense of additional compute.
		flip_input = fliplr(input)
		flip_output = model(flip_input)
		flip_output = flip_output[-1].cpu()
		flip_output = flip_back(flip_output.detach(), data_info.hflip_indices)
		heatmaps = (output[-1].cpu() + flip_output) / 2
	else:
		heatmaps = output[-1].cpu()

	return output#, loss.item()

@torch.no_grad()
def do_validation_epoch(val_loader, model, device,train_output_stride,eval_output_stride, criterion,classes,weights,flip=False, quiet=False):
	losses = AverageMeter()
	accuracies = AverageMeter()
	predictions = [None] * len(val_loader.dataset)
	losses.update(0, 1)
	weighted_losses = AverageMeter()
	weighted_losses.update(0, 1)
	# Put the model in evaluation mode.
	model.eval()

	iterable = enumerate(val_loader)
	progress = None
	if not quiet:
		progress = tqdm(iterable, desc='Valid', total=len(val_loader), ascii=True, leave=False)
		iterable = progress
	count = 500
	out_list = []
	targ_list = []
	losses.avg = 0
	weighted_losses.avg = 0
	class_losses = {}
	for a_class in [0,1,2,3,4,5,6,7]:
		class_losses[a_class] = []
	all_img_confusion_matrix = np.zeros((8,8))
	for i, (inputs, target,im_info) in iterable:
		# if inputs.shape[2]*inputs.shape[3]<17000000:
		# Copy data to the training device (eg GPU).
		input = inputs.to(device, non_blocking=True)
		target = target.to(device, non_blocking=True)
		# target_weight = meta['target_weight'].to(device, non_blocking=True)

		output = do_validation_step(model, input, target,train_output_stride,eval_output_stride,criterion)#, target_weight, flip)
		# upsample = nn.Upsample(size=(output.shape[2]*eval_output_stride,output.shape[3]*eval_output_stride), mode='bilinear',align_corners=True)
		# output = upsample(output)
		
		# upsample = nn.Upsample(size=(heatmaps.shape[2],heatmaps.shape[3]), mode='bilinear',align_corners=True)
		# target=target.view(target.shape[0],-1,target.shape[1],target.shape[2])
		# target=upsample(target)
		# target = target.view(target.shape[0],target.shape[2],target.shape[3])        


		# for j in range(len(output)):        
		#     out = output[j]#*400#[0]
		#     out = out[0].cpu().numpy()
		#     plt.imshow(out, cmap='magma')
		#     plt.colorbar()
		#     plt.savefig("./results/output/prediction/"+str(count)+".png")
		#     plt.clf()
		#     targ = target[j].cpu().numpy()
		#     plt.imshow(targ, cmap='magma')
		#     plt.colorbar()
		#     plt.savefig("./results/output/gt/"+str(count)+".png")
		#     plt.clf()
		#     count = count + 1
		# output = output.view(output.shape[0],output.shape[2],output.shape[3])
		# if output.shape[1]>target.shape[1]:
		# 	shape_diff = output.shape[1]-target.shape[1]
		# 	output = output[:,0:-shape_diff,:]
		# if output.shape[2]>target.shape[2]:
		# 	shape_diff = output.shape[2] - target.shape[2]
		# 	output = output[:,:,0:-shape_diff]
		# confusion_matrix = confusion_mat(output,target)
		# all_img_confusion_matrix = all_img_confusion_matrix + confusion_matrix
		# if i>1000:
		# 	normed_matrix = normalize(all_img_confusion_matrix, axis=0, norm='l1')
		# 	pdb.set_trace()
		# 	f = open('image.npy','wb')
		# 	np.save(f, all_img_confusion_matrix)
		# 	plt.imshow(normed_matrix)
		# 	plt.colorbar()
		# 	plt.xlabel('Predictions')
		# 	plt.ylabel('Ground truths')
		# 	plt.show()
			
		loss = criterion(output,target)
		class_loss = evaluate_class_loss(output,target,classes)
		for inds,_ in enumerate(class_loss):
			if torch.isnan(class_loss[inds])==False:
				class_losses[inds].append(class_loss[inds].item())
		
		losses.update(loss, input.size(0))
		# weighted_losses.update(weighted_loss,input.size(0))
		if progress is not None:
			progress.set_postfix_str('Loss: {loss:0.4f}'.format(
				loss=losses.avg#,weighted_loss=weighted_losses.avg
			))
			

		## Single Image r-square graph generatation
		# out = output[0].cpu().view(-1)
		# targ = target[0].cpu().view(-1)
		# max_val = max(targ.numpy())
		# minval = min(out.numpy())
		# for i in range(0,int(max_val)+1):
		#     bin_range = np.arange(minval,i+.5+.10,0.10)
		#     plt.hist(targ.numpy(),bins=bin_range, alpha=0.5, label='target')
		#     plt.hist(out.numpy(),bins=bin_range, alpha=0.5,label='output')
		#     plt.legend(loc='upper right')
		#     plt.savefig(str(i)+".png")
		#     plt.show()
		#     minval = i +.5
		# print("MAE:{}".format(abs(out-targ).sum()/(out.shape[0])))    
		# pdb.set_trace()
		# plt.plot(targ,out,'ro',markersize=1)
		# plt.xlabel("Target")
		# plt.ylabel("Prediction")
		# plt.savefig("r-square.png")
		# plt.show()
		# pdb.set_trace()
		## Single Image r-square graph generatation
		# out_list.append(out)
		# targ_list.append(targ)

		# pdb.set_trace()
		#
		# diff = target[0].cpu().numpy() - heatmaps[0].cpu().numpy()
		# plt.imshow(diff, cmap='magma')
		# plt.colorbar()
		# plt.savefig("difference.png")
		# plt.clf()
		# pdb.set_trace()
		# # heatmaps=heatmaps*50
		# zeros = np.zeros((1,heatmaps.shape[1],heatmaps.shape[2]))

		# pdb.set_trace()



		# zeros = zeros.astype(np.uint8)
		# zeros = zeros.transpose(1,2,0)
		# matplotlib.image.imsave('name.png', zeros)
		# pdb.set_trace()
		# heatmaps = heatmaps.type(torch.IntTensor)
		# trans = transforms.ToPILImage()
		# img=trans(heatmaps)
		# path_split = path[0].split('/')
		# videoname = path_split[7]
		# image_name = path_split[9]
		# root = '/media/sohel/HDD/googledrive/detection_sports/netball_bbl'
		# path = os.path.join(root, videoname,"res",image_name)
		# if not os.path.isdir(os.path.join(root, videoname,"res")):
		#     os.mkdir(os.path.join(root, videoname,"res"))
		# matplotlib.image.imsave(path, zeros)
		# plt.imshow(img)
		# plt.savefig(path)
		# pdb.set_trace()
		# if epoch==2:
		#     pdb.set_trace()
		# Calculate PCK from the predicted heatmaps.
		# acc = accuracy(heatmaps, target.cpu(), acc_joints)

		# Calculate locations in original image space from the predicted heatmaps.
		# preds = final_preds(heatmaps, meta['center'], meta['scale'], [64, 64])
		# for example_index, pose in zip(meta['index'], preds):
		#     predictions[example_index] = pose

		# Record accuracy and loss for this batch.
		# losses.update(loss, input.size(0))
		# # accuracies.update(acc[0].item(), input.size(0))

		# # Show accuracy and loss as part of the progress bar.
		# if progress is not None:
		#     progress.set_postfix_str('Loss: {loss:0.4f}'.format(
		#         loss=losses.avg
		#         # acc=100 * accuracies.avg
		#     ))
		#Acc: {acc:6.2f}
	# predictions = torch.stack(predictions, dim=0)
	# All image r-square
	# out = torch.stack(out_list)
	# targ = torch.stack(targ_list)
	# plt.plot(targ,out,'ro',markersize=1)
	# plt.xlabel("Target")
	# plt.ylabel("Prediction")
	# plt.savefig("r-square.png")
	# plt.show()
	
	avg_class_losses = {}
	for inds,_ in enumerate(class_losses):
		if len(class_losses[inds])!=0:
			avg_class_losses[inds] = sum(class_losses[inds])/len(class_losses[inds])
		else:
			avg_class_losses[inds] = None
	
	print('validation class loss: {0}'.format(avg_class_losses))
	return losses.avg,avg_class_losses#,weighted_losses.avg#, accuracies.avg, predictions

def confusion_mat(preds,gts):
	conf_matrix = np.zeros((8,8))
	preds = torch.round(preds.cpu())
	preds[preds<=0]=0
	gts = gts.squeeze(dim=0).squeeze(dim=0)
	preds = preds.squeeze(dim=0).squeeze(dim=0)
	gts = gts[0:1080,]
	preds = preds[0:1080,]
	gts = gts.view(-1).cpu()
	preds = preds.view(-1)
	gts = gts.type(torch.int)
	preds = preds.type(torch.int)
	result_confusion_matrix = confusion_matrix(gts, preds)
	conf_matrix[:result_confusion_matrix.shape[0],:result_confusion_matrix.shape[1]]=result_confusion_matrix
	return conf_matrix
 #preds[gts==i]
