import segmentation_models_pytorch as smp
class DeepLabV3Plus_sub(smp.DeepLabV3Plus):
	"""docstring for DeepLabV3_sub"""
	def __init__(self,
			encoder_name = "resnet34",
			encoder_depth = 5,
			encoder_weights = "imagenet",
			encoder_output_stride = 8,
			decoder_channels = 256,
			decoder_atrous_rates = (12, 24, 36),
			# decoder_atrous_rates = (12, 36, 72),

			in_channels = 4,
			classes = 3,
			activation = None,
			upsampling = 4,
			aux_params = None):
		super().__init__(
			encoder_name = encoder_name,
			encoder_depth = encoder_depth,
			encoder_weights = encoder_weights,
			encoder_output_stride = encoder_output_stride,
			decoder_channels = decoder_channels,
			decoder_atrous_rates = decoder_atrous_rates,
			in_channels = in_channels,
			classes = classes,
			activation = activation,
			upsampling = upsampling,
			aux_params = aux_params)
	# def feature_extractor(self,):
		
class PAN_sub(smp.PAN):
	"""docstring for DeepLabV3_sub"""
	def __init__(self,
			encoder_name = "resnet34",
			encoder_weights = None,
			encoder_dilation = True,
			decoder_channels = 32,
			in_channels = 4,
			classes = 1,
			activation = None,
			upsampling = 4,
			aux_params = None):
		super().__init__(
			encoder_name = encoder_name,
			encoder_weights = encoder_weights,
			encoder_dilation = encoder_dilation,
			decoder_channels = decoder_channels,
			in_channels = in_channels,
			classes = classes,
			activation = activation,
			upsampling = upsampling,
			aux_params = aux_params)
# model = DeepLabV3Plus_sub()
		