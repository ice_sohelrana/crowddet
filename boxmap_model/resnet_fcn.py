import math

from torch import nn
import torchvision.models
import pdb

class ResNetFcn(nn.Module):
    DEFAULT_OUTPUT_STRIDE = 32  # Default output stride of an unmodified ResNet.

    def __init__(self, resnet, train_output_stride=DEFAULT_OUTPUT_STRIDE,
                 eval_output_stride=DEFAULT_OUTPUT_STRIDE,input_dim=3, centred_striding=True):
        """Create a fully-convolutional feature extractor from an existing ResNet model.

        Args:
            resnet (torchvision.models.ResNet): A ResNet model from the `torchvision` package.
            train_output_stride (int): Output stride of the module when in training mode.
            eval_output_stride (int): Output stride of the model when in evaluation mode.
            centred_striding (bool): If `True`, centred striding will be used (Sárándi, 2020).
        """
        super().__init__()

        self.output_stride = self.DEFAULT_OUTPUT_STRIDE
        self._train_output_stride = train_output_stride
        self._eval_output_stride = eval_output_stride
        self.centred_striding = centred_striding
        self.out_features = resnet.fc.in_features

        # Use neural network modules from the original ResNet.
        resnet.conv1=nn.Conv2d(input_dim,64,kernel_size=(7,7),stride=(2,2),padding=(3,3),bias=False)
        self.conv1 = resnet.conv1
        self.bn1 = resnet.bn1
        self.relu = resnet.relu
        self.maxpool = resnet.maxpool
        self.layer1 = resnet.layer1
        self.layer2 = resnet.layer2
        self.layer3 = resnet.layer3
        self.layer4 = resnet.layer4

        self.output_layer = nn.Conv2d(self.out_features,1,1)

        # Module references for using different output strides.
        self._dilatable_layers = [self.layer2, self.layer3, self.layer4]
        self._out_stride_refs = []
        for layer in self._dilatable_layers:
            refs = {'stride': [], 'dilate': []}
            for module in layer.modules():
                if isinstance(module, nn.Conv2d):
                    if module.stride == (2, 2):
                        refs['stride'].append(module)
                    elif module.kernel_size == (3, 3):
                        refs['dilate'].append(module)
            self._out_stride_refs.append(refs)

        # Set initial output stride of the model.
        self._update_output_stride()

    @property
    def train_output_stride(self):
        return self._train_output_stride

    @train_output_stride.setter
    def train_output_stride(self, value):
        self._train_output_stride = value
        self._update_output_stride()

    @property
    def eval_output_stride(self):
        return self._eval_output_stride

    @eval_output_stride.setter
    def eval_output_stride(self, value):
        self._eval_output_stride = value
        self._update_output_stride()

    def _update_output_stride(self):
        """Update the model's output stride based on the current training mode."""
        output_stride = self.train_output_stride if self.training else self.eval_output_stride
        assert output_stride in {4, 8, 16, 32}, 'output stride must be 4, 8, 16, or 32'
        if output_stride == self.output_stride:
            return
        self.output_stride = output_stride

        dilation_start_index = len(self._out_stride_refs) + int(math.log2(self.output_stride / self.DEFAULT_OUTPUT_STRIDE))
        for i, refs in enumerate(self._out_stride_refs):
            # Calculate stride and dilation values for this "tier" of modules.
            if i < dilation_start_index:
                stride = (2, 2)
                dilation = (1, 1)
            else:
                dilx = dily = 2 ** (i - dilation_start_index + 1)
                stride = (1, 1)
                dilation = (dilx, dily)
            # Update convolutional layer settings.
            for module in refs['stride']:
                module.stride = stride
            for module in refs['dilate']:
                module.dilation = dilation
                module.padding = tuple(
                    (d * (k - 1) + 1) // 2 for d, k in zip(module.dilation, module.kernel_size)
                )

    def train(self, mode=True):
        super().train(mode)
        self._update_output_stride()

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)
        x = self.layer1(x)
        shift_activations_before = len(self._out_stride_refs) + int(math.log2(self.output_stride / self.DEFAULT_OUTPUT_STRIDE)) - 1
        for i, layer in enumerate(self._dilatable_layers):
            if self.centred_striding and i == shift_activations_before:
                # Shift activations up and to the left by 1 pixel.
                x = nn.functional.pad(x[..., 1:, 1:], [0, 1, 0, 1])
            x = layer(x)
        # pdb.set_trace()
        x = self.output_layer(x)
        return x
