import os
import cv2
import torch
import numpy as np
from numpy import array
import torch.utils.data as data
from torchvision import datasets, transforms as T
import matplotlib.pyplot as plt
import natsort
from PIL import Image

from torchvision.datasets.coco import CocoDetection
class cocoDetection_sub(CocoDetection):
	def __init__(self,root_dir,annFile):
    	super().__init__(root=root_dir, annFile=annFile)
    def __getitem__(self, index: int) -> Tuple[Any, Any]:
        """
        Args:
            index (int): Index
        Returns:
            tuple: Tuple (image, target). target is the object returned by ``coco.loadAnns``.
        """
        coco = self.coco
        img_id = self.ids[index]
        ann_ids = coco.getAnnIds(imgIds=img_id)
        # target = coco.loadAnns(ann_ids)

        path = coco.loadImgs(img_id)[0]['file_name']

        img = Image.open(os.path.join(self.root, path)).convert('RGB')
        # if self.transforms is not None:
        #     img, target = self.transforms(img, target)

        return img, target
    