import os
import cv2
import torch
import numpy as np
from numpy import array
import torch.utils.data as data
from torchvision import datasets, transforms as T
import matplotlib.pyplot as plt
import natsort
from PIL import Image
from lib.utils import misc_utils
from boxmap_model.utils.resize import fit
import os
import pdb
import glob, csv

class sports(data.Dataset):
	def __init__(self, image_path,dset, is_train,use_detmap=True,use_scoremap=True,use_cornermap=False,other_sports=False,others_path=None):
		# pdb.set_trace()
		self.images = []
		self.targets = []
		self.det_maps = []
		self.smap_1 = []
		self.smap_2 = []
		self.smap_3 = []
		self.smap_4 = []
		self.smap_5 = []
		self.height = 1088
		self.width = 1920
		if is_train==True:
			self.training=True
		else:
			self.training=False
		self.training=True
		# self.videos = os.listdir(image_path)
		self.videos = os.listdir(os.path.join(image_path,dset))
		self.tls=[]
		self.brs=[]
		self.dset = dset
		# if dset=="train":
		#     self.videos= ["14_netball_1"] #self.videos[0:20] + self.videos[21:len(self.videos)]# ["21_netball_8"]
		# else:
		#     self.videos = ["15_netball_2"]
		for a_video in self.videos:
			target_files = os.listdir(os.path.join(image_path,dset,a_video, "masks"))
			target_files = natsort.natsorted(target_files)
			image_dirs = sorted(glob.glob(os.path.join(image_path, dset,a_video, "img1","*.png")))
			if use_cornermap:
				tls=sorted(glob.glob(os.path.join(image_path, dset,a_video, "fixed_cornermaps","tl","*.npz")))
				brs=sorted(glob.glob(os.path.join(image_path, dset,a_video, "fixed_cornermaps","br","*.npz")))
			targets = []
			det_maps = []
			cornermaps=[]
			smap_1,smap_2,smap_3,smap_4,smap_5=[],[],[],[],[]
			for a_file in target_files:
				targets.append(os.path.join(image_path, dset,a_video, "masks",a_file))
				if use_detmap:
					det_maps.append(os.path.join(image_path, dset,a_video, "emd_detection_boxmaps",a_file.rjust(12, '0')))
				if use_scoremap:
					smap_1.append(os.path.join(image_path, dset,a_video, "emd_score_boxmaps",'0.2',a_file.rjust(12, '0')))
					smap_2.append(os.path.join(image_path, dset,a_video, "emd_score_boxmaps",'0.4',a_file.rjust(12, '0')))
					smap_3.append(os.path.join(image_path, dset,a_video, "emd_score_boxmaps",'0.6',a_file.rjust(12, '0')))
					smap_4.append(os.path.join(image_path, dset,a_video, "emd_score_boxmaps",'0.8',a_file.rjust(12, '0')))
					smap_5.append(os.path.join(image_path, dset,a_video, "emd_score_boxmaps",'1.0',a_file.rjust(12, '0')))

				# image_dirs = sorted(glob.glob(os.path.join(image_path, a_video, "coco/img1","*.png")))
				# targets_dir = sorted(glob.glob(os.path.join(image_path, a_video, "coco/masks","*.png")))
				# targets_dir_2 = sorted(glob.glob(os.path.join(image_path, dset,a_video, "masks","*.png")))
			# if dset=='train':                
			self.images = self.images + image_dirs #+image_dirs + 
			self.targets = self.targets + targets #targets_dir
			if use_detmap:
				self.det_maps = self.det_maps + det_maps
			if use_scoremap:
				self.smap_1 = self.smap_1+smap_1
				self.smap_2 = self.smap_2+smap_2
				self.smap_3 = self.smap_3+smap_3
				self.smap_4 = self.smap_4+smap_4
				self.smap_5 = self.smap_5+smap_5
			if use_cornermap:
				self.tls = self.tls + tls
				self.brs = self.brs + brs
			# else:                
			# 	self.images = self.images + image_dirs #+image_dirs + 
			# 	self.targets = self.targets + targets #targets_dir
			# 	if use_detmap:
			# 		self.det_maps = self.det_maps + det_maps
			# 	if use_scoremap:
			# 		self.smap_1 = self.smap_1+smap_1
			# 		self.smap_2 = self.smap_2+smap_2
			# 		self.smap_3 = self.smap_3+smap_3
			# 		self.smap_4 = self.smap_4+smap_4
			# 		self.smap_5 = self.smap_5+smap_5
		# total_img = int(len(self.images)/4)*4
		# self.images = self.images[0:total_img]
		# self.targets = self.targets[0:total_img]
		# self.det_maps = self.det_maps[0:total_img]
		# Add augmented images
		# if is_train == True:
		# 	aug_imgs = sorted(glob.glob(os.path.join(image_path, dset,a_video, "aug_img1","*.png")))
		# 	targets = []
		# 	det_maps = []
		# 	for a_img in aug_imgs:
		# 		target = a_img.split(os.path.join(image_path, dset,a_video,'aug_img1')+'/')[1]
		# 		targets.append(os.path.join(image_path, dset,a_video,'aug_boxmaps',target))
		# 		if use_detmap:
		# 			det_maps.append(os.path.join(image_path, dset,a_video,'aug_detection_boxmaps',target))
		# 	self.images = self.images + aug_imgs #+image_dirs + 
		# 	self.targets = self.targets + targets
		# 	if use_detmap:
		# 		self.det_maps = self.det_maps + det_maps
		if other_sports==True:
			self.videos = os.listdir(os.path.join(others_path,dset))
			for a_video in self.videos:
				target_files = os.listdir(os.path.join(others_path,dset,a_video, "masks"))
				target_files = natsort.natsorted(target_files)
				image_dirs = sorted(glob.glob(os.path.join(others_path, dset,a_video, "img1","*.png")))
				if use_cornermap:
					tls=sorted(glob.glob(os.path.join(others_path, dset,a_video, "fixed_cornermaps","tl","*.npz")))
					brs=sorted(glob.glob(os.path.join(others_path, dset,a_video, "fixed_cornermaps","br","*.npz")))
				targets = []
				det_maps = []
				smap_1,smap_2,smap_3,smap_4,smap_5=[],[],[],[],[]
				for a_file in target_files:
					targets.append(os.path.join(others_path, dset,a_video, "masks",a_file))
					if use_detmap:
						det_maps.append(os.path.join(others_path, dset,a_video, "emd_detection_boxmaps",a_file.rjust(12, '0')))
					if use_scoremap:
						smap_1.append(os.path.join(others_path, dset,a_video, "emd_score_boxmaps",'0.2',a_file.rjust(12, '0')))
						smap_2.append(os.path.join(others_path, dset,a_video, "emd_score_boxmaps",'0.4',a_file.rjust(12, '0')))
						smap_3.append(os.path.join(others_path, dset,a_video, "emd_score_boxmaps",'0.6',a_file.rjust(12, '0')))
						smap_4.append(os.path.join(others_path, dset,a_video, "emd_score_boxmaps",'0.8',a_file.rjust(12, '0')))
						smap_5.append(os.path.join(others_path, dset,a_video, "emd_score_boxmaps",'1.0',a_file.rjust(12, '0')))

					# image_dirs = sorted(glob.glob(os.path.join(image_path, a_video, "coco/img1","*.png")))
					# targets_dir = sorted(glob.glob(os.path.join(image_path, a_video, "coco/masks","*.png")))
					# targets_dir_2 = sorted(glob.glob(os.path.join(image_path, dset,a_video, "masks","*.png")))
				# if dset=='train':                
				self.images = self.images + image_dirs #+image_dirs + 
				self.targets = self.targets + targets #targets_dir
				if use_detmap:
					self.det_maps = self.det_maps + det_maps
				if use_scoremap:
					self.smap_1 = self.smap_1+smap_1
					self.smap_2 = self.smap_2+smap_2
					self.smap_3 = self.smap_3+smap_3
					self.smap_4 = self.smap_4+smap_4
					self.smap_5 = self.smap_5+smap_5
				if use_cornermap:
					self.tls = self.tls + tls
					self.brs = self.brs + brs
				# else:                
					# self.images = self.images + image_dirs #+image_dirs + 
					# self.targets = self.targets + targets #targets_dir
					# if use_detmap:
					# 	self.det_maps = self.det_maps + det_maps
					# if use_scoremap:
					# 	self.smap_1 = self.smap_1+smap_1
					# 	self.smap_2 = self.smap_2+smap_2
					# 	self.smap_3 = self.smap_3+smap_3
					# 	self.smap_4 = self.smap_4+smap_4
					# 	self.smap_5 = self.smap_5+smap_5
		total_img = int(len(self.images)/2)*2
		self.images = self.images[0:total_img]
		self.targets = self.targets[0:total_img]
		if use_detmap:
			self.det_maps = self.det_maps[0:total_img]
		if use_scoremap:
			self.smap_1 = self.smap_1[0:total_img]
			self.smap_2 = self.smap_2[0:total_img]
			self.smap_3 = self.smap_3[0:total_img]
			self.smap_4 = self.smap_4[0:total_img]
			self.smap_5 = self.smap_5[0:total_img]
		if use_cornermap:
			self.tls = self.tls[0:total_img]
			self.brs = self.brs[0:total_img]
		self.normalize = T.Normalize(mean=[0.406, 0.456, 0.485],std=[0.225, 0.224, 0.229])
		self.use_detmap=use_detmap
		self.use_scoremap=use_scoremap
		self.use_cornermap=use_cornermap
		self.size = len(self.images)
	def __getitem__(self, index):
		# if self.training:
		# 	if_flap = np.random.randint(2) == 1
		# else:
		# 	if_flap = False
		image_path = self.images[index]
		image = cv2.imread(image_path, cv2.IMREAD_COLOR)

		image_h = image.shape[0]
		image_w = image.shape[1]
		# if if_flap:
		# 	image = cv2.flip(image, 1)
		# if self.training:
			# ground_truth
		image = self.image_preprocess(image)

		gt_boxmap = self.boxmap_preprocess(self.targets[index])
		if self.use_scoremap:
			det_maps = self.boxmap_preprocess(self.det_maps[index],det_map=True)
			det_maps = det_maps.unsqueeze(dim=0)
			smap_1 = self.boxmap_preprocess(self.smap_1[index],det_map=True)
			smap_1 = smap_1.unsqueeze(dim=0)
			smap_2 = self.boxmap_preprocess(self.smap_2[index],det_map=True)
			smap_2 = smap_2.unsqueeze(dim=0)
			smap_3 = self.boxmap_preprocess(self.smap_3[index],det_map=True)
			smap_3 = smap_3.unsqueeze(dim=0)
			smap_4 = self.boxmap_preprocess(self.smap_4[index],det_map=True)
			smap_4 = smap_4.unsqueeze(dim=0)
			smap_5 = self.boxmap_preprocess(self.smap_5[index],det_map=True)
			smap_5 = smap_5.unsqueeze(dim=0)
			inp = torch.cat((image,det_maps,smap_1,smap_2,smap_3,smap_4,smap_5),dim=0)			
		elif self.use_detmap:
			det_maps = self.boxmap_preprocess(self.det_maps[index],det_map=True)
			det_maps = det_maps.unsqueeze(dim=0)
			inp = torch.cat((image,det_maps),dim=0)
		else:
			inp = image
		if self.use_cornermap:
			tl = torch.from_numpy(np.load(self.tls[index])['heatmap'])
			br = torch.from_numpy(np.load(self.brs[index])['heatmap'])
			tl = fit(tl.unsqueeze(dim=0), (self.height,self.width), fit_mode='contain')
			br = fit(br.unsqueeze(dim=0), (self.height,self.width), fit_mode='contain')
		inp = fit(inp, (self.height,self.width), fit_mode='contain')
		gt_boxmap = fit(gt_boxmap.unsqueeze(dim=0), (self.height,self.width), fit_mode='contain')
		#pdb.set_trace()
		# gtboxes = self.targets[index] #misc_utils.load_gt(record, 'gtboxes', 'fbox', self.config.class_names)
		# keep = (gtboxes[:, 2]>=0) * (gtboxes[:, 3]>=0)
		# gtboxes=gtboxes[keep, :]
		# gtboxes[:, 2:4] += gtboxes[:, :2]
		# gtboxes = np.array(gtboxes)
		# if if_flap:
		#     gtboxes = flip_boxes(gtboxes, image_w)
		# im_info
		# gtboxes = gtboxes.astype('float64') 
		# nr_gtboxes = gtboxes.shape[0]
		im_info = np.array([0, 0, 1, image_h, image_w, 0])
		if self.use_cornermap:
			return inp, gt_boxmap, im_info,tl,br
		else:
			return inp, gt_boxmap, im_info
		# else:
		#     gtboxes = self.targets[index] #misc_utils.load_gt(record, 'gtboxes', 'fbox', self.config.class_names)
		#     image = image.transpose(2, 0, 1)
		#     image = torch.tensor(image).float()
		#     gtboxes = np.array(gtboxes)
		#     gtboxes = gtboxes.astype('float64')
		#     target = {}
		#     target["boxes"] = torch.from_numpy(gtboxes[:,0:4])
		#     target["labels"] = torch.ones((len(gtboxes),), dtype=torch.int64)
		#     target["area"] = (gtboxes[:, 3] - gtboxes[:, 1]) * (gtboxes[:, 2] - gtboxes[:, 0])
		#     target["image_id"] = torch.tensor([self.image_ids[index]])
		#     target["iscrowd"] = torch.zeros((len(gtboxes),), dtype=torch.int64)
		#     # gtboxes = torch.tensor(gtboxes)
			
		#     # keep = (gtboxes[:, 2]>=0) * (gtboxes[:, 3]>=0)
		#     # gtboxes=gtboxes[keep, :]
		#     # gtboxes[:, 2:4] += gtboxes[:, :2]
		#     # im_info
		#     nr_gtboxes = gtboxes.shape[0]
		#     im_info = np.array([0, 0, 1, image_h, image_w, nr_gtboxes])
	def image_preprocess(self,image):
		image = torch.tensor(image).float()
		image=image.permute(2,0,1)
		image = image/255
		norm_im = self.normalize(image)
		# norm_im = norm_im.permute(1,2,0)
		return norm_im
	def boxmap_preprocess(self,boxmap_url,det_map=False):
		boxmap=Image.open(boxmap_url)
		boxmap=array(boxmap)
		boxmap=torch.from_numpy(boxmap)
		boxmap=boxmap.float()
		# if det_map:
		# 	if boxmap.max()>1:
		# 		boxmap=boxmap/100
		return boxmap
	def validation_annotation(self):
		for a_id in self.image_ids:
			for ind,a_box in enumerate(self.targets[a_id]):
				single_box ={"fbox":a_box,"tag":"person","h_box":a_box,"extra":None,"vbox":a_box,"head_attr":None}
	def __len__(self):
		return self.size



	def merge_batch(self, data):
		# image
		images = [it[0] for it in data]
		gt_boxes = [it[1] for it in data]
		im_info = np.array([it[2] for it in data])
		batch_height = np.max(im_info[:, 3])
		batch_width = np.max(im_info[:, 4])
		padded_images = [pad_image(
				im, batch_height, batch_width, self.config.image_mean) for im in images]
		t_height, t_width, scale = target_size(
				batch_height, batch_width, self.short_size, self.max_size)
		# INTER_CUBIC, INTER_LINEAR, INTER_NEAREST, INTER_AREA, INTER_LANCZOS4
		resized_images = np.array([cv2.resize(
				im, (t_width, t_height), interpolation=cv2.INTER_LINEAR) for im in padded_images])
		resized_images = resized_images.transpose(0, 3, 1, 2)
		images = torch.tensor(resized_images).float()
		# ground_truth
		ground_truth = []
		for it in gt_boxes:
			gt_padded = np.zeros((self.config.max_boxes_of_image, self.config.nr_box_dim))
			it[:, 0:4] *= scale
			max_box = min(self.config.max_boxes_of_image, len(it))
			gt_padded[:max_box] = it[:max_box]
			ground_truth.append(gt_padded)
		ground_truth = torch.tensor(ground_truth).float()
		# im_info
		im_info[:, 0] = t_height
		im_info[:, 1] = t_width
		im_info[:, 2] = scale
		im_info = torch.tensor(im_info)
		if max(im_info[:, -1] < 2):
			return None, None, None
		else:
			return images, ground_truth, im_info

	def load_annotation(self,ann_path):
		annotations = []
		with open(ann_path, 'r') as csvfile:
			csv_read = csv.reader(csvfile, delimiter = ',')
			next(csv_read)
			for row in csv_read:
				if row[2]!= 'Others' and row[2]!= 'other' and row[2] != 'others' and row[2]!= 'Other':
					x = int(float(row[10]))
					y = int(float(row[9]))
					width = int(float(row[11]))
					height = int(float(row[12]))
					frame_number = int(row[4].replace(".png",''))
					annotations.append([frame_number,x,y,x+width,y+height])
		annotations = self.same_frame_objects(annotations)
		return annotations
	def same_frame_objects(self,annotations):
		framewise_order = sorted(annotations,key=lambda x: int(x[0]))
		prev_frame_number = 1
		single_frame_objs = []
		self.frame_objs = []
		for i in range(len(framewise_order)):
			if int(framewise_order[i][0])==prev_frame_number:
				single_frame_objs.append([framewise_order[i][1],framewise_order[i][2],framewise_order[i][3],framewise_order[i][4],1])
			else:
				if len(single_frame_objs)==0:
					self.frame_objs.append("empty")
				else:
					self.frame_objs.append(single_frame_objs)
				single_frame_objs=[]
				single_frame_objs.append([framewise_order[i][1],framewise_order[i][2],framewise_order[i][3],framewise_order[i][4],1])
			prev_frame_number=int(framewise_order[i][0])
		if len(single_frame_objs)==0:
			self.frame_objs.append("empty")
		else:
			self.frame_objs.append(single_frame_objs)
		return
	def get_single_same_frame_objects(self,frame_number):
		return self.frame_objs[int(frame_number)-1]  
def target_size(height, width, short_size, max_size):
	im_size_min = np.min([height, width])
	im_size_max = np.max([height, width])
	scale = (short_size + 0.0) / im_size_min
	if scale * im_size_max > max_size:
		scale = (max_size + 0.0) / im_size_max
	t_height, t_width = int(round(height * scale)), int(
		round(width * scale))
	return t_height, t_width, scale

def flip_boxes(boxes, im_w):
	flip_boxes = boxes.copy()
	for i in range(flip_boxes.shape[0]):
		flip_boxes[i, 0] = im_w - boxes[i, 2] - 1
		flip_boxes[i, 2] = im_w - boxes[i, 0] - 1
	return flip_boxes

def pad_image(img, height, width, mean_value):
	o_h, o_w, _ = img.shape
	margins = np.zeros(2, np.int32)
	assert o_h <= height
	margins[0] = height - o_h
	img = cv2.copyMakeBorder(
		img, 0, margins[0], 0, 0, cv2.BORDER_CONSTANT, value=0)
	img[o_h:, :, :] = mean_value
	assert o_w <= width
	margins[1] = width - o_w
	img = cv2.copyMakeBorder(
		img, 0, 0, 0, margins[1], cv2.BORDER_CONSTANT, value=0)
	img[:, o_w:, :] = mean_value
	return img
