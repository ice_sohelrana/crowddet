import os
import cv2
import torch
import numpy as np
from numpy import array
import os
import torch.utils.data as data
from PIL import Image
from torchvision import datasets, transforms as T
from boxmap_model.utils.resize import fit


import torch.nn.functional as nnf
import pdb
import natsort
import glob
class motdet20(data.Dataset):
	def __init__(self, image_path, dset,is_train=True,use_detmap=True):
		self.height = 1088
		self.width = 1920
		self.images = []
		self.targets = []
		self.det_maps = []
		# self.videos = os.listdir(image_path)
		self.videos = os.listdir(os.path.join(image_path,dset))
		self.dset = dset
		# if dset=="train":
		#     self.videos= ["14_netball_1"] #self.videos[0:20] + self.videos[21:len(self.videos)]# ["21_netball_8"]
		# else:
		#     self.videos = ["15_netball_2"]
		for a_video in self.videos:
			target_files = os.listdir(os.path.join(image_path,dset,a_video, "masks"))
			target_files = natsort.natsorted(target_files)
			image_dirs = sorted(glob.glob(os.path.join(image_path, dset,a_video, "img1","*.jpg")))
			targets = []
			det_maps = []
			for a_file in target_files:
				targets.append(os.path.join(image_path, dset,a_video, "masks",a_file))
				if use_detmap:
					det_maps.append(os.path.join(image_path, dset,a_video, "emd_detection_boxmaps",a_file.split('.png')[0].rjust(6,'0')+'.png'))
				# image_dirs = sorted(glob.glob(os.path.join(image_path, a_video, "coco/img1","*.png")))
				# targets_dir = sorted(glob.glob(os.path.join(image_path, a_video, "coco/masks","*.png")))
				# targets_dir_2 = sorted(glob.glob(os.path.join(image_path, dset,a_video, "masks","*.png")))
			train_portion = int(len(image_dirs)*1.0)
			val_portion = int(len(image_dirs)*0.0)
		   
			if is_train==True:                
				self.images = self.images + image_dirs[0:train_portion] #+image_dirs + 
				self.targets = self.targets + targets[0:train_portion] #targets_dir
				if use_detmap:
					self.det_maps = self.det_maps + det_maps
			else:                
				self.images = self.images + image_dirs[train_portion:train_portion+val_portion] #+image_dirs + 
				self.targets = self.targets + targets[train_portion:train_portion+val_portion] #targets_dir
				if use_detmap:
					self.det_maps = self.det_maps + det_maps
		# pdb.set_trace()
		self.normalize = T.Normalize(mean=[0.406, 0.456, 0.485],std=[0.225, 0.224, 0.229])
		# self.flip = T.RandomHorizontalFlip(p=1)
		self.use_detmap = use_detmap
		total_img = int(len(self.images)/4)*4
		self.images = self.images[0:total_img]
		self.targets = self.targets[0:total_img]
		if use_detmap:
			self.det_maps = self.det_maps[0:total_img]
		self.size = len(self.images)
		# self.trans = T.ToTensor()
		# self.config = config
		# pdb.set_trace()
	def __getitem__(self, index):

		image_path = self.images[index]
		image = cv2.imread(image_path, cv2.IMREAD_COLOR)

		image_h = image.shape[0]
		image_w = image.shape[1]
		# t_height, t_width, scale = self.target_size(
		#             height, width, self.short_size, self.max_size)
		image = self.image_preprocess(image)
		
		gt_boxmap = self.boxmap_preprocess(self.targets[index])
		if self.use_detmap:
			det_maps = self.boxmap_preprocess(self.det_maps[index],det_map=True)
			det_maps = det_maps.unsqueeze(dim=0)
			inp = torch.cat((image,det_maps),dim=0)
		else:
			inp = image

		inp = fit(inp, (self.height,self.width), fit_mode='contain')
		gt_boxmap = fit(gt_boxmap.unsqueeze(dim=0), (self.height,self.width), fit_mode='contain')
		# gtboxes = self.targets[index] #misc_utils.load_gt(record, 'gtboxes', 'fbox', self.config.class_names)
		# keep = (gtboxes[:, 2]>=0) * (gtboxes[:, 3]>=0)
		# gtboxes=gtboxes[keep, :]
		# gtboxes[:, 2:4] += gtboxes[:, :2]
		# gtboxes = np.array(gtboxes)
		# if if_flap:
		#     gtboxes = flip_boxes(gtboxes, image_w)
		# im_info
		# gtboxes = gtboxes.astype('float64') 
		# nr_gtboxes = gtboxes.shape[0]
		im_info = np.array([0, 0, 1, image_h, image_w, 0])
		return inp, gt_boxmap, im_info
	def image_preprocess(self,image):
		image = torch.tensor(image).float()
		image=image.permute(2,0,1)
		image = image/255
		norm_im = self.normalize(image)
		# norm_im = norm_im.permute(1,2,0)
		return norm_im
	def boxmap_preprocess(self,boxmap_url,det_map=False):
		boxmap=Image.open(boxmap_url)
		boxmap=array(boxmap)
		boxmap=torch.from_numpy(boxmap)
		boxmap=boxmap.float()
		# if det_map:
		#   if boxmap.max()>1:
		#       boxmap=boxmap/100
		return boxmap
	def __len__(self):
		return len(self.images)
