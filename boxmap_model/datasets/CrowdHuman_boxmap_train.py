import os
import torch
import numpy as np
from numpy import array
import cv2
import os
import torch.utils.data as data
from PIL import Image
from torchvision import datasets, transforms as T
from boxmap_model.utils.resize import fit

import torch.nn.functional as nnf
import pdb 
class CrowdHuman(data.Dataset):
	def __init__(self, image_path, dset,is_train=True,use_detmap=True):
		self.images = []
		self.targets = []
		self.det_maps = []
		self.image_path = os.path.join(image_path,dset,"Images")
		self.boxmap_path = os.path.join(image_path,dset,"masks")
		if use_detmap:
			self.det_map_path = os.path.join(image_path,dset,"emd_detection_boxmaps")
		self.short_size = 800
		self.max_size = 1400
		self.width=1920
		self.height=1088
		# self.videos = os.listdir(image_path)
		# if is_train:
		#     self.training = True
		#     source = os.path.join(data_path,dset,'annotation_train.odgt')
		#     # self.short_size = config.train_image_short_size
		#     # self.max_size = config.train_image_max_size
		# else:
		#     self.training = False
		#     source = os.path.join(data_path,dset,'annotation_val.odgt')
			# self.short_size = config.eval_image_short_size
			# self.max_size = config.eval_image_max_size
		self.images = os.listdir(os.path.join(image_path, dset, "Images"))
		#pdb.set_trace()
		self.normalize = T.Normalize(mean=[0.406, 0.456, 0.485],std=[0.225, 0.224, 0.229])
		total_img = int(len(self.images)/4)*4
		self.images = self.images[0:total_img]
		self.size = len(self.images)
		# self.trans = T.ToTensor()
		# self.config = config
		# pdb.set_trace()
		self.use_detmap = use_detmap
	def __getitem__(self, index):
		image_path = self.images[index]
		image = cv2.imread(os.path.join(self.image_path,self.images[index]), cv2.IMREAD_COLOR)
		#pdb.set_trace()
		image_h = image.shape[0]
		image_w = image.shape[1]
		image = self.image_preprocess(image)
		gt_boxmap = self.boxmap_preprocess(os.path.join(self.boxmap_path,self.images[index].split('.jpg')[0]+'.png'))
		if self.use_detmap:
			det_maps = self.boxmap_preprocess(os.path.join(self.det_map_path,self.images[index].split('.jpg')[0]+'.png'),det_map=True)
			det_maps = det_maps.unsqueeze(dim=0)
			inp = torch.cat((image,det_maps),dim=0)
		else:
			inp = image
		inp = fit(inp, (self.height,self.width), fit_mode='contain')
		gt_boxmap = fit(gt_boxmap.unsqueeze(dim=0), (self.height,self.width), fit_mode='contain')
		im_info = np.array([0, 0, 1, image_h, image_w, 0])
		return inp, gt_boxmap, im_info
	def image_preprocess(self,image):
		image = torch.tensor(image).float()
		image=image.permute(2,0,1)
		image = image/255
		norm_im = self.normalize(image)
		# norm_im = norm_im.permute(1,2,0)
		return norm_im
	def boxmap_preprocess(self,boxmap_url,det_map=False):
		boxmap=Image.open(boxmap_url)
		boxmap=array(boxmap)
		boxmap=torch.from_numpy(boxmap)
		boxmap=boxmap.float()
		return boxmap
	# def __getitem__(self, index):
	#     image = load_image(os.path.join(self.image_path,self.images[index]))
	#     width = image.shape[2]
	#     height = image.shape[1]
	#     t_height, t_width, scale = self.target_size(
	#                 height, width, self.short_size, self.max_size)
	#     inp = self.normalize(image)
	#     target = im_to_boxmap_fcn(os.path.join(self.bomxap_path,self.images[index].split('.jpg')[0]+'.png'),"real",None)
	#     det_maps = im_to_boxmap_fcn(os.path.join(self.det_map_path,self.images[index].split('.jpg')[0]+'.png'),"real",None)
	#     det_maps = torch.unsqueeze(det_maps,dim=0)
	#     inp = torch.cat((inp,det_maps),dim=0)
	#     inp = nnf.interpolate(inp.unsqueeze(dim=0), size=(t_height, t_width), mode='bilinear', align_corners=False)
	#     target = nnf.interpolate(target.unsqueeze(dim=0).unsqueeze(dim=0), size=(t_height, t_width), mode='bilinear', align_corners=False)
	#     inp = inp.squeeze(dim=0)
	#     target = target.squeeze(dim=0).squeeze(dim=0)
	#     im_info = torch.tensor([t_height, t_width, scale, height, width])
	#     return inp,target,im_info
	# def target_size(self,height, width, short_size, max_size):
	#     im_size_min = np.min([height, width])
	#     im_size_max = np.max([height, width])
	#     scale = (short_size + 0.0) / im_size_min
	#     if scale * im_size_max > max_size:
	#         scale = (max_size + 0.0) / im_size_max
	#     t_height, t_width = int(round(height * scale)), int(
	#         round(width * scale))
	#     return t_height, t_width, scale
	def __len__(self):
		return self.size


		# else:
		#     # image
		#     t_height, t_width, scale = target_size(
		#             image_h, image_w, self.short_size, self.max_size)
		#     # INTER_CUBIC, INTER_LINEAR, INTER_NEAREST, INTER_AREA, INTER_LANCZOS4
		#     resized_image = cv2.resize(image, (t_width, t_height), interpolation=cv2.INTER_LINEAR)
		#     resized_image = resized_image.transpose(2, 0, 1)
		#     image = torch.tensor(resized_image).float()
		#     gtboxes = misc_utils.load_gt(record, 'gtboxes', 'fbox', self.config.class_names)
		#     gtboxes[:, 2:4] += gtboxes[:, :2]
		#     gtboxes = torch.tensor(gtboxes)
		#     # im_info
		#     nr_gtboxes = gtboxes.shape[0]
		#     im_info = torch.tensor([t_height, t_width, scale, image_h, image_w, nr_gtboxes])
		#     return image, gtboxes, im_info, record['ID']

