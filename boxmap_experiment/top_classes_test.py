import argparse
import os

import torch
import torch.backends.cudnn
from torch.nn import DataParallel
from torch.optim.rmsprop import RMSprop
from torch.utils.data import DataLoader
from tqdm import trange, tqdm
import os
import math
import matplotlib.pyplot as plt
import pdb
from torchvision import transforms
import torchvision.models as models
import sys
import sacred
import random
import pdb
import numpy as np
import time
import glob
import os
from boxmap_detect_test import detect
from datasets.sports_boxmap_experiment import Sports
from pytorch_eval.coco_utils import get_coco_api_from_dataset
from pytorch_eval.coco_eval import CocoEvaluator
import pytorch_eval.utils
# from boxmap_detect_multithread import detect

# sys.path.insert(0, '..')
from datasets.sports_boxmap_experiment import Sports
from get_model import prediction_model_load,boxmap_model_load
sys.path.insert(0, '../lib')
sys.path.insert(0, '../model/rcnn_emd_refine')
from config import config
# sys.path.insert(0, '../model')
# from misc import collate_fn
# from frcnn_transforms import image_transforms
# 

from sacred import Experiment
ex = Experiment()
ex.add_config('./cfgs/config.yaml')
# detect = ex.capture(detect)
@ex.automain
def my_main(_config,_log,_run):
	sacred.commands.print_config(_run)
	torch.manual_seed(_config['seed'])
	torch.cuda.manual_seed(_config['seed'])
	np.random.seed(_config['seed'])
	torch.backends.cudnn.deterministic = True
	if torch.cuda.is_available():
		device = torch.device('cuda', torch.cuda.current_device())
		torch.backends.cudnn.benchmark = True
	else:
		device = torch.device('cpu')
	_log.info("Initializing object detector.")
	print('Loading models')
	prediction_model = prediction_model_load(_config['predictor'],device)
	boxmap_model= boxmap_model_load(_config['boxmap'],device)
	#_config['data_dir']
	seqs = os.listdir(os.path.join(_config['dataset']['data_dir'],_config['dataset']['image_set']))
	output_save_dir = os.path.join(_config["output_dir"],_config['selector']['algorithm'])
	overall_mae=[]
	fname=0	
	gt_boxes = []

	dataset_test=Sports(config,if_train=False,other_sports=True)
	total_images = len(dataset_test)
	# pdb.set_trace()
	# for seq in seqs:
	barWidth = 0.05
	pr_model = []
	pr_nms = []
	recall_model = []
	recall_nms = []
	fig = plt.figure()
	for ids in range(total_images):

		start = time.time()
		# ids=2000
		dataset = dataset_test[ids]
		# dataset_test=Sport(_config['dataset']['data_dir'],_config['dataset']['image_set'],videoname=[seq],transforms=image_transforms)
		# coco = get_coco_api_from_dataset([dataset])
		# iou_types = ["bbox"]
		# coco_evaluator = CocoEvaluator(coco, iou_types)
		mae,fname,pr_recall_model,pr_recall_nms=detect(boxmap_model, prediction_model, dataset, ids,fig,_config,device,fname,output_save_dir)#,coco_evaluator)
		# pr_model.append(pr_recall_model[0])
		# pr_nms.append(pr_recall_nms[0])
		# recall_model.append(pr_recall_model[1])
		# recall_nms.append(pr_recall_nms[1])
			# pdb.set_trace()
			# r1 = np.arange(len(pr_model))
			# r2 = [x + barWidth for x in r1]
			# r3 = [x + barWidth for x in r2]
			# plt.bar(r1, pr_model, color='green', width=barWidth, label='model')
			# plt.bar(r2, pr_nms, color='red', width=barWidth, label='nms')
	# pdb.set_trace()
		# if ids>10:
	pr = [np.array(pr_model),np.array(pr_nms)]
	plt.hist(pr,50,histtype='bar')
	# plt.hist(pr_model)
	# plt.show()
	plt.savefig('./output/pr.png')
	plt.clf()

	recall = [np.array(recall_model),np.array(recall_nms)]
	plt.hist(recall,50,histtype='bar')
	# plt.show()
	plt.savefig('./output/recall.png')
	plt.clf()
	pdb.set_trace()
	# plt.hist(recall_model)
	# # plt.show()
	# plt.savefig('./output/recall_model.png')
	# plt.clf()
	# plt.hist(recall_nms)
	# # plt.show()
	# plt.savefig('./output/recall_nms.png')
	# plt.clf()
	pdb.set_trace()
			# pdb.set_trace()

		# seq_process(_config,seq,image_transforms,boxmap_model,prediction_model,device,fname,collate_fn)
		# print("Time: {0}".format(time.time()-start))
		# overall_mae=overall_mae+mae
	# print('Overall MAE: {0} for all video'.format(sum(overall_mae)/len(overall_mae)))
		# coco_evaluator.synchronize_between_processes()
		# coco_evaluator.accumulate()
		# coco_evaluator.summarize()
		# pr_recall = coco_evaluator.coco_eval['bbox'].stats

	# calc_mAP([0.05,0.50,0.70,0.95],output_save_dir)
# 	pdb.set_trace()
# def seq_process(_config,seq,image_transforms,boxmap_model,prediction_model,device,fname,collate_fn):
# 	dataset_test=Sport(_config['dataset']['data_dir'],_config['dataset']['image_set'],videoname=[seq],transforms=image_transforms)
# 	data_loader_test = torch.utils.data.DataLoader(dataset_test, batch_size=4, num_workers=4,collate_fn=collate_fn)
# 	mae,fname=detect(boxmap_model, prediction_model, data_loader_test, _config,seq,device,fname)
# 	return mae,fname
# class BlockingThreadPoolExecutor(ThreadPoolExecutor):
#     """An executor which blocks on submit when no executors are available.
	
#     This means that you can call `submit` as often as you like, but if there are
#     more than `max_workers` jobs already active, any further calls to `submit`
#     will wait until an executor becomes available before returning.
#     """
#     def __init__(self, max_workers):
#         super().__init__(max_workers)
#         self.semaphore = BoundedSemaphore(max_workers)

#     def submit(self, fn, *args, **kwargs):
#         self.semaphore.acquire()
#         try:
#             future = super().submit(fn, *args, **kwargs)
#         except:
#             self.semaphore.release()
#             raise
#         else:
#             future.add_done_callback(lambda x: self.semaphore.release())
#             return future
