import pdb
import os
import torch
import json
def coco_to_json_format(output_dir,image_ids):
	res_write = open(os.path.join(output_dir,'LRP_evaluation','results.json'),'w')
	json_res_data = []
	ids = 0
	for a_id in image_ids:
		output = torch.load(os.path.join(output_dir,'evaluation',str(a_id)+'.pth'))
		image_id = output['targ']['image_id'].item()
		data = output['res'][a_id]
		data['boxes'][:,2:] -=data['boxes'][:,:2]	
		for box,score,label in zip(data['boxes'],data['scores'],data['labels']):
			box[0],box[1],box[2],box[3] = float(box[0]),float(box[1]),float(box[2]),float(box[3])
			json_data = {"image_id": image_id,"category_id":label.tolist(),"bbox":box.tolist(),'score':float(score.item()),'id':ids}
			json_res_data.append(json_data)
			ids = ids + 1
	json.dump(json_res_data, res_write)
	targ_write = open(os.path.join(output_dir,'LRP_evaluation','targets.json'),'w')
	json_targ_annotations = []
	ids = 0
	img_data = []
	for a_id in image_ids:
		output = torch.load(os.path.join(output_dir,'evaluation',str(a_id)+'.pth'))
		image_id = output['targ']['image_id'].item()
		data = output['targ']
		data['boxes'][0][:,2:] -=data['boxes'][0][:,:2]	
		for box,area,label in zip(data['boxes'][0],data['area'][0],data['labels'][0]):
			box[0],box[1],box[2],box[3] = float(box[0]),float(box[1]),float(box[2]),float(box[3])
			json_data = {"image_id": image_id,"category_id":label.tolist(),"bbox":box.tolist(),'area':area.tolist(),'iscrowd':0,'id':ids}
			json_targ_annotations.append(json_data)
			ids = ids + 1
		img_data.append({'width':1920,'height':1080,'id':image_id})
	targ_data = {}
	targ_data['info'] = {'description': 'COCO Data format', 'url': 'http://cocodataset.org', 'version': '1.0', 'year': 2017, 'contributor': 'COCO Consortium', 'date_created': '2017/09/01'}
	targ_data['images'] = img_data
	targ_data['annotations'] = json_targ_annotations
	targ_data['categories'] = [{'supercategory': 'person', 'id': 1, 'name': 'person'}]
	json.dump(targ_data,targ_write)
	return