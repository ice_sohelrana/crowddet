import sys
import torch
import torch.nn as nn
import numpy
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image, ImageDraw
from concurrent.futures import ThreadPoolExecutor, as_completed
from threading import BoundedSemaphore
from tqdm import tqdm
# from mAP_evaluation.main import calculate_mAP
sys.path.insert(0,'..')
from load_im import load_im,boxmap_padding

sys.path.insert(0, '../frcnn_fpn')
from detection.transform import resize_boxes
sys.path.insert(0,'../box_selector')
from selector import box_select
from util.boxmap_represent import boxmapCreate
from util.metric_logger import MetricLogger
import pdb
import copy
import csv
import os

@torch.no_grad()
def detect(boxmap_model,prediction_model,data_loader,_config,seq,device,fname):
	eval_output_stride = _config['boxmap']['eval_output_stride']

	cpu_device = torch.device("cpu")
	boxmap_model.eval()
	prediction_model.eval()
	metric_logger = MetricLogger(delimiter="  ")
	det_boxes = list()
	det_labels = list()
	det_scores = list()
	true_boxes = list()
	true_labels = list()
	true_difficulties = list() 
	header = 'Test:'
	result_dir=_config["output_dir"]
	if not os.path.exists(os.path.join(result_dir,_config['selector']['algorithm'],_config['selector']['boxmap_use'],seq)):
		os.makedirs(os.path.join(result_dir,_config['selector']['algorithm'],_config['selector']['boxmap_use'],seq))
	save_path = os.path.join(result_dir,_config['selector']['algorithm'],_config['selector']['boxmap_use'],seq)
	overall_mae = []
	jobs = {}
	fname=1
	with BlockingThreadPoolExecutor(max_workers=3) as executor:
		for images, targets,videoname,frm_number in metric_logger.log_every(data_loader, 100, header):
			future = executor.submit(process,_config,images,targets,result_dir,save_path,overall_mae,device,fname,frm_number,prediction_model,boxmap_model,videoname)
			jobs[future] = _config,images,targets,result_dir,save_path,overall_mae,device,fname,frm_number,prediction_model,boxmap_model,videoname
			fname = fname+1
	final_mae = []
	for future in as_completed(jobs):
		inp = jobs[future]
		res = future.result()
		# final_mae.append(res[0])
		# overall_mae.append
		final_mae=final_mae+res
		# pdb.set_trace()
		# print(f'Input: {inp}, Output: {res}')
		print(f'Output: {res}')
	return final_mae,fname
	# pdb.set_trace()
		# det_boxes.extend([torch.from_numpy(final_boxes[:,0:4])])
		# det_scores.extend([torch.from_numpy(final_boxes[:,4])])
		# # labels = targets[0]['labels'][0:len(final_boxes)]
		# labels = torch.ones(len(final_boxes))
		# det_labels.extend([labels])
		# # pdb.set_trace()
		# true_boxes.extend([targets[0]['boxes']])
		# true_labels.extend([targets[0]['labels']])
		# true_difficulties.extend([labels[0:len(targets[0]['boxes'])]])
	# APs, mAP=calculate_mAP(det_boxes, det_labels, det_scores, true_boxes, true_labels, true_difficulties)
	# print('\nMean Average Precision (mAP): %.3f' % mAP)

@torch.no_grad()
def process(_config,images,targets,result_dir,save_path,overall_mae,device,fname,frm_number,prediction_model,boxmap_model,videoname):
	images = list(img.to(device) for img in images)
	original_img = copy.deepcopy(images) 
	prediction_boxes = prediction_model(images)
	if _config['selector']['boxmap_use']=='gt':
		boxmap_out = boxmapCreate(targets[0]['boxes'].cpu().numpy(),(1080,1920))
	else:
		if _config['boxmap']['image_pred_fusion']==True:
			boxmap_inps = []
			for a_preds,a_img in zip(prediction_boxes,images):
				boxmap = boxmapCreate(a_preds['boxes'].cpu().numpy(),(1088,1920))
				boxmap = torch.from_numpy(boxmap)
				boxmap=boxmap.type(torch.FloatTensor)
				a_img = load_im("no_url",a_img)
				boxmap_inp = torch.cat((a_img,boxmap.unsqueeze(dim=0).to(device)),dim=0)
				boxmap_inps.append(boxmap_inp)
		else:
			raise NotImplementedError('This model is not implemented')
		boxmap_out=boxmap_model(torch.stack(boxmap_inps))
		upsample = nn.Upsample(size=(boxmap_out.shape[2]*eval_output_stride,boxmap_out.shape[3]*eval_output_stride), mode='bilinear',align_corners=True)
		boxmap_out = upsample(boxmap_out)
		boxmap_out=boxmap_out.squeeze(dim=0).squeeze(dim=0)
		boxmap_out = boxmap_out[0:1080,].cpu().numpy()
	# GT boxmap
	# boxes = resize_boxes(targets[0]['boxes'],(749,1333),(1080,1920))

	# boxmap = torch.from_numpy(boxmap).to(device)
	scores = prediction_boxes[0]['scores'].unsqueeze(dim=1)
	pred_boxes = torch.cat((prediction_boxes[0]['boxes'],scores),dim=1)
	pred_boxes = pred_boxes.cpu().numpy()
	final_boxes,mae = box_select([boxmap_out],[pred_boxes],_config['selector'])
	overall_mae = overall_mae+mae
	# img = original_img[0].cpu().permute(1,2,0).numpy()
	# im = Image.fromarray(np.uint8(img*255))		
	# draw = ImageDraw.Draw(im)
	# for a_box in final_boxes:
	# 	draw.rectangle([int(a_box[0]),int(a_box[1]),int(a_box[2]),int(a_box[3])],outline="red",fill=None)
	
	# for a_box in targets[0]["boxes"]:
		# draw.rectangle([int(a_box[0]),int(a_box[1]),int(a_box[2]),int(a_box[3])],outline="green",fill=None)
	# plt.imshow(im)
	# plt.show()
	# im.save(os.path.join(save_path,str(frm_number[0])+".jpg"))
	with open(os.path.join(result_dir,"detection-results-pixel-voting",videoname[0]+"_"+str(frm_number[0])+".txt"), mode='w') as det_file:
		det_writer = csv.writer(det_file, delimiter=' ', quotechar='"', quoting=csv.QUOTE_MINIMAL)
		for a_box in final_boxes:
			det_writer.writerow([1,a_box[4],a_box[0],a_box[1],a_box[2],a_box[3]])
	with open(os.path.join(result_dir,"ground-truth-gt-pixel-voting",videoname[0]+"_"+str(frm_number[0])+".txt"), mode='w') as gt_file:
		gt_writer = csv.writer(gt_file, delimiter=' ', quotechar='"', quoting=csv.QUOTE_MINIMAL)
		for a_box in targets[0]['boxes']:
			gt_writer.writerow([1,a_box[0].item(),a_box[1].item(),a_box[2].item(),a_box[3].item()])
	fname = fname+1
	print('Overall MAE: {0}'.format(sum(overall_mae)/len(overall_mae)))
	return overall_mae
class BlockingThreadPoolExecutor(ThreadPoolExecutor):
    """An executor which blocks on submit when no executors are available.
    
    This means that you can call `submit` as often as you like, but if there are
    more than `max_workers` jobs already active, any further calls to `submit`
    will wait until an executor becomes available before returning.
    """
    def __init__(self, max_workers):
        super().__init__(max_workers)
        self.semaphore = BoundedSemaphore(max_workers)

    def submit(self, fn, *args, **kwargs):
        self.semaphore.acquire()
        try:
            future = super().submit(fn, *args, **kwargs)
        except:
            self.semaphore.release()
            raise
        else:
            future.add_done_callback(lambda x: self.semaphore.release())
            return future