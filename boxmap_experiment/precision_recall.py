import matplotlib.pyplot as plt
from random import choices
import random
import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
from torch.utils.data.dataset import Dataset  # For custom datasets
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import time
import os
import copy
import glob
import torch.nn.functional as F
from PIL import Image, ImageDraw
import random
from random import sample,randint
from torch.autograd import Variable
#plt.ion()   # interactive mode
import pdb, sys, os
#import psutil
import csv
class PRgraph(object):
	"""docstring for PRgraph"""
	def __init__(self, iou_threshold):
		super(PRgraph, self).__init__()
		self.iou_threshold = iou_threshold
		#self.gt_url = gt_url
	def detectionGTOverlap(self,detection_objs,gt_objs,conf_threshold):
		# frame_number = []
		# with open(gtfile_path, 'r') as csvfile:
		# 	csv_read = csv.reader(csvfile, delimiter = ',')
		# 	for row in csv_read:
		# 		frame_number.append(row[0])
		# total_frame = len(list(set(frame_number)))
		tp = 0
		fp = 0
		fn = 0
		#pdb.set_trace()
		single_tp_count,single_fp_count,single_fn_count = self.getTpFpFn(detection_objs,gt_objs)
		# for i in range(1,total_frame+1):
		# 	frame_url = images_path[i-1]
		# 	detection_objs = detectionFromFrame(i,frame_url,self.nms_threshold,conf_threshold)
		# 	gt_objs = self.singleFrameBoxes(i,gtfile_path)
		# 	if len(detection_objs) == 0:
		# 		fn = fn + len(gt_objs)
		# 	else:
		# 		single_tp_count,single_fp_count,single_fn_count = self.getTpFpFn(detection_objs,gt_objs)
		# 		tp = tp + single_tp_count
		# 		fp = fp + single_fp_count
		# 		fn = fn + single_fn_count
			#print("TP = {0},FP={1}, FN={2}".format(tp,fp,fn))
			#print(i)
		tp,fp,fn=single_tp_count,single_fp_count,single_fn_count
		return tp,fp,fn
			#single_frame_true_positive = len(gt_objs)
					#pdb.set_trace()
			#pdb.set_trace()
	def getTpFpFn(self,detection_objs,gt_objs):
		tp_count = 0
		fp_count = 0
		fn_count = 0
		for i in range(len(detection_objs)):
			temp_tp_count = tp_count
			for j in range(len(gt_objs)):
				iou = self.IOU_calculation(detection_objs[i][0],detection_objs[i][1],(detection_objs[i][2]-detection_objs[i][0]),(detection_objs[i][3]-detection_objs[i][1]),gt_objs[j][0],gt_objs[j][1],(gt_objs[j][2]-gt_objs[j][0]),(gt_objs[j][3]-gt_objs[j][1]))
				if iou >= self.iou_threshold:
					tp_count = tp_count + 1
					break
			if temp_tp_count==tp_count:
				fp_count = fp_count + 1
		for i in range(len(gt_objs)):
			fn_flag = 1
			for j in range(len(detection_objs)):
				iou = self.IOU_calculation(detection_objs[j][0],detection_objs[j][1],(detection_objs[j][2]-detection_objs[j][0]),(detection_objs[j][3]-detection_objs[j][1]),gt_objs[i][0],gt_objs[i][1],(gt_objs[i][2]-gt_objs[i][0]),(gt_objs[i][3]-gt_objs[i][1]))
				if iou >= self.iou_threshold:
					fn_flag = 0
			if fn_flag == 1:
				fn_count = fn_count + 1
		return tp_count,fp_count,fn_count
	def IOU_calculation(self,x1,y1,width_1,height_1,x2,y2,width_2,height_2):
		x11 = x1 + width_1
		y11 = y1 + height_1
		x22 = x2 + width_2
		y22 = y2 + height_2
		xA = max(x1, x2)
		yA = max(y1, y2)
		xB = min(x11, x22)
		yB = min(y11, y22)
		interArea = max(0,xB - xA + 1) * max(0,yB - yA + 1)
		boxAArea = (x11 - x1 + 1) * (y11 - y1 + 1)
		boxBArea = (x22 - x2 + 1) * (y22 - y2 + 1)
		iou = interArea / float(boxAArea + boxBArea - interArea)
		return iou

	# def all_video_precision_recall(self,conf_threshold):
	# 	video_name_list = self.get_all_video_name()
	# 	all_v_tp = 0
	# 	all_v_fp = 0
	# 	all_v_fn = 0
	# 	for i in range(len(video_name_list)):
	# 		image_path = self.root_path+video_name_list[i]+"/img1/*.jpg"
	# 		imageset_images_path=sorted(glob.glob(image_path))
	# 		gtfile_path = self.root_path+video_name_list[i]+"/gt/gt.txt"
	# 		single_v_tp,single_v_fp,single_v_fn = self.detectionGTOverlap(gtfile_path,imageset_images_path,conf_threshold)
	# 		all_v_tp = all_v_tp + single_v_tp
	# 		all_v_fp = all_v_fp + single_v_fp
	# 		all_v_fn = all_v_fn + single_v_fn
	# 	recall = all_v_tp/(all_v_tp+all_v_fn)
	# 	precision = all_v_tp/(all_v_tp+all_v_fp)
	# 	return precision,recall
	def single_video_precision_recall(self,videoname,det_boxes,gt_boxes,conf_threshold):
		# image_path = self.root_path+videoname+"/img1/*.jpg"
		# gtfile_path = self.root_path+videoname+"/gt/gt.txt"
		# imageset_images_path=sorted(glob.glob(image_path))
		single_v_tp,single_v_fp,single_v_fn = self.detectionGTOverlap(det_boxes,gt_boxes,conf_threshold)
		recall = single_v_tp/(single_v_tp+single_v_fn)
		precision = single_v_tp/(single_v_tp+single_v_fp)
		return recall,precision
	def single_video_precision_recall_graph_generation(self,det_boxes,gt_boxes,videoname):
		conf_threshold = 0.1
		precision_list = []
		recall_list = []
		threshold_list = []
		max_distance = 999
		exp_recall = 0
		epx_precision = 0
		exp_threshold = 0
		for i in range(18):
			recall,precision = self.single_video_precision_recall(videoname,det_boxes,gt_boxes,conf_threshold)
			precision_list.append(precision)
			recall_list.append(recall)
			threshold_list.append(conf_threshold)
			if abs(recall-precision)<max_distance:
				max_distance = abs(recall-precision)
				exp_threshold = conf_threshold
				exp_recall = recall
				epx_precision = precision
			conf_threshold = conf_threshold + 0.05
			print(i)
		print("Exp threshold:{0}, Exp precision:{1}, Exp recall:{1}".format(exp_threshold,epx_precision,exp_recall))
		#pdb.set_trace()
		plt.figure(1)
		plt.title("Precision/Recall vs threshold--"+videoname+"_IoU threshold:"+str(self.iou_threshold))
		plt.xlabel('Threshold_value')
		plt.ylabel('Precision/Recall')
		plt.plot(threshold_list,precision_list,color = 'r', label="Precision")
		plt.plot(threshold_list,recall_list,color = 'g', label="Recall")
		plt.legend()
		plt.show()
		plt.pause(0)
	def all_video_precision_recall_graph_generation(self):
		conf_threshold = 0.1
		precision_list = []
		recall_list = []
		threshold_list = []
		max_distance = 999
		exp_recall = 0
		epx_precision = 0
		exp_threshold = 0
		for i in range(18):
			recall,precision = self.all_video_precision_recall(conf_threshold)
			print(i)
			precision_list.append(precision)
			recall_list.append(recall)
			threshold_list.append(conf_threshold)
			if abs(recall-precision)<max_distance:
				max_distance = abs(recall-precision)
				exp_threshold = conf_threshold
				exp_recall = recall
				epx_precision = precision
			conf_threshold = conf_threshold + 0.05
		print("Exp threshold:{0}, Exp precision:{1}, Exp recall:{1}".format(exp_threshold,epx_precision,exp_recall))
		#pdb.set_trace()
		plt.figure(1)
		plt.title("Precision/Recall vs threshold")
		plt.xlabel('Threshold_value')
		plt.ylabel('Precision/Recall')

		plt.plot(threshold_list,precision_list,color = 'r', label="Precision")
		plt.plot(threshold_list,recall_list,color = 'g', label="Recall")
		plt.legend()
		plt.show()
		plt.pause(0)
# class evaluate(object):
# 	def __init__(self, iou_thresholds):
# 		super(evaluate, self).__init__()
# 		self.thresholds = iou_thresholds
# 	def update(dets,gts):
# 		scorelist = list()
# 		for i,det in enumerate(dets):
# 			for i,gt in enumerate(gts):


