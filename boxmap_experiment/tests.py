import argparse
import os

import torch
import torch.backends.cudnn
from torch.nn import DataParallel
from torch.optim.rmsprop import RMSprop
from torch.utils.data import DataLoader
from tqdm import trange, tqdm
import os
import math
import json

import pdb
from torchvision import transforms
import torchvision.models as models
import sys
import sacred
import random
import pdb
import numpy as np
import time
import glob
import os
from boxmap_detect import detect
from datasets.sports_boxmap_experiment import Sports
from pytorch_eval.coco_utils import get_coco_api_from_dataset
from pytorch_eval.coco_eval import CocoEvaluator
import pytorch_eval.utils
from pytorch_LRP.LRP_utils import coco_to_json_format
# from boxmap_detect_multithread import detect

# sys.path.insert(0, '..')
from datasets.sports_boxmap_experiment import Sports
from get_model import prediction_model_load,boxmap_model_load
sys.path.insert(0, '../lib')
sys.path.insert(0, '../model/rcnn_fpn_baseline')
from config import config
# sys.path.insert(0, '../model')
# from misc import collate_fn
# from frcnn_transforms import image_transforms
# 

from sacred import Experiment
ex = Experiment()
ex.add_config('./cfgs/config.yaml')
# detect = ex.capture(detect)
@ex.automain
def my_main(_config,_log,_run):
	sacred.commands.print_config(_run)
	torch.manual_seed(_config['seed'])
	torch.cuda.manual_seed(_config['seed'])
	np.random.seed(_config['seed'])
	torch.backends.cudnn.deterministic = True
	if torch.cuda.is_available():
		device = torch.device('cuda', torch.cuda.current_device())
		torch.backends.cudnn.benchmark = True
	else:
		device = torch.device('cpu')
	_log.info("Initializing object detector.")
<<<<<<< HEAD
	print('Loading models')
	prediction_model = prediction_model_load(_config['predictor']['weight'],device)
	boxmap_model= boxmap_model_load(_config['boxmap'],device)
=======
	print('Loading models: {0}'.format(_config['predictor']['predictor_name']))
	if _config['predictor']['predictor_name']=='rcnn-emd-refine':
		sys.path.insert(0, '../model/rcnn_emd_refine')
	if _config['predictor']['predictor_name']=='rcnn-fpn-baseline':
		sys.path.insert(0, '../model/rcnn_fpn_baseline')
	from config import config

	prediction_model = prediction_model_load(_config['predictor'],device)
	if _config['selector']['boxmap_use']=='model':
		boxmap_model= boxmap_model_load(_config['boxmap'],device)
	else:
		boxmap_model=None
>>>>>>> Add cornermap
	#_config['data_dir']
	seqs = os.listdir(os.path.join(_config['dataset']['data_dir'],_config['dataset']['image_set']))
	output_save_dir = os.path.join(_config["output_dir"],_config['selector']['algorithm'])
	overall_mae=[]
	fname=0	
	gt_boxes = []


	# for seq in seqs:
	for seq in range(1):

		start = time.time()
		# dataset_test=Sport(_config['dataset']['data_dir'],_config['dataset']['image_set'],videoname=[seq],transforms=image_transforms)
		dataset_test=Sports(config,if_train=False,other_sports=True)
		data_loader_test = torch.utils.data.DataLoader(dataset_test, batch_size=1, shuffle=False,num_workers=0)
		coco = get_coco_api_from_dataset(data_loader_test.dataset)
		iou_types = ["bbox"]
		coco_evaluator = CocoEvaluator(coco, iou_types)

		mae,fname,image_ids=detect(boxmap_model, prediction_model, data_loader_test, _config,str(seq),device,fname,output_save_dir,coco_evaluator)
		# seq_process(_config,seq,image_transforms,boxmap_model,prediction_model,device,fname,collate_fn)
	# 	print("Time: {0}".format(time.time()-start))
	# 	overall_mae=overall_mae+mae
	# print('Overall MAE: {0} for all video'.format(sum(overall_mae)/len(overall_mae)))
	detection_list = sorted(os.listdir(os.path.join(_config['output_dir'],'evaluation')))
	# pdb.set_trace()
	coco_to_json_format(_config['output_dir'],image_ids)
	for a_id in image_ids:
		output = torch.load(os.path.join(_config['output_dir'],'evaluation',str(a_id)+'.pth'))
		coco_evaluator.update(output['res'])
	coco_evaluator.synchronize_between_processes()
	coco_evaluator.accumulate()
	coco_evaluator.summarize()




	# calc_mAP([0.05,0.50,0.70,0.95],output_save_dir)
# 	pdb.set_trace()
# def seq_process(_config,seq,image_transforms,boxmap_model,prediction_model,device,fname,collate_fn):
# 	dataset_test=Sport(_config['dataset']['data_dir'],_config['dataset']['image_set'],videoname=[seq],transforms=image_transforms)
# 	data_loader_test = torch.utils.data.DataLoader(dataset_test, batch_size=4, num_workers=4,collate_fn=collate_fn)
# 	mae,fname=detect(boxmap_model, prediction_model, data_loader_test, _config,seq,device,fname)
# 	return mae,fname
# class BlockingThreadPoolExecutor(ThreadPoolExecutor):
#     """An executor which blocks on submit when no executors are available.
	
#     This means that you can call `submit` as often as you like, but if there are
#     more than `max_workers` jobs already active, any further calls to `submit`
#     will wait until an executor becomes available before returning.
#     """
#     def __init__(self, max_workers):
#         super().__init__(max_workers)
#         self.semaphore = BoundedSemaphore(max_workers)

#     def submit(self, fn, *args, **kwargs):
#         self.semaphore.acquire()
#         try:
#             future = super().submit(fn, *args, **kwargs)
#         except:
#             self.semaphore.release()
#             raise
#         else:
#             future.add_done_callback(lambda x: self.semaphore.release())
#             return future
