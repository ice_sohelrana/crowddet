import sys
import torch
from torch.nn import DataParallel

import torchvision.models as models
import sacred
from sacred import Experiment
sys.path.insert(0, '../boxmap_model')
from resnet_fcn import ResNetFcn
from seg_models import DeepLabV3Plus_sub
sys.path.insert(0, '../lib')
sys.path.insert(0, '../model/rcnn_fpn_baseline')
# from config import Config
from network import Network

import pdb
# from detection.faster_rcnn import fasterrcnn_resnet50_fpn,FastRCNNPredictor
# from frcnn_transforms import image_transforms
# import sacred
# from sacred import Experiment
# ex = Experiment()
# ex.add_config('./cfgs/config.yaml')
@torch.no_grad()
def box_predictions(image,device):

	image,_ = image_transforms(image)
	image = image.unsqueeze(dim=0).to(device)
	predictions = model(image)
	return predictions
def prediction_model_load(weights,device):
	net = Network()
	net.cuda(device)
	net = net.eval()
	weights = torch.load(weights)
	net.load_state_dict(weights['state_dict'])
	return net
@torch.no_grad()
def boxmap_model_load(_config,device):
	# train_output_stride = 8
	# eval_output_stride = 8
	if _config["boxmap_name"]=='resnet-FCN':
		if _config["backbone"]=='resnet18':
			model = models.resnet18(pretrained=True)
		elif _config["backbone"]=='resnet34':
			model = models.resnet34(pretrained=True)
		if _config['image_pred_fusion']==True:
			boxmap_model = ResNetFcn(model,_config['train_output_stride'], _config['eval_output_stride'],4)
		else:
			boxmap_model = ResNetFcn(model,_config['train_output_stride'], _config['eval_output_stride'],3)
	elif _config["boxmap_name"] == 'deeplabv3plus':
		if _config['image_pred_fusion']==True:
			if _config['score_fusion']==True:
				boxmap_model = DeepLabV3Plus_sub(encoder_name=_config["backbone"],encoder_output_stride=_config['eval_output_stride'],in_channels=9)
			else:
				boxmap_model = DeepLabV3Plus_sub(encoder_name=_config["backbone"],encoder_output_stride=_config['eval_output_stride'],in_channels=4)
		else:
			boxmap_model = DeepLabV3Plus_sub(encoder_name=_config["backbone"],encoder_output_stride=_config['eval_output_stride'],in_channels=3)
	boxmap_model = DataParallel(boxmap_model).to(device)
	checkpoint = torch.load(_config['weight'])
	# args.start_epoch = checkpoint['epoch']
	# best_acc = checkpoint['best_acc']
	boxmap_model.load_state_dict(checkpoint['state_dict'])
	return boxmap_model