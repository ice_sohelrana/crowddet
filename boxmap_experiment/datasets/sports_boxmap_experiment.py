import torch.utils.data as data
from torchvision import datasets, transforms as T
import cv2
import torch
import os
import csv
import glob
import numpy as np

class Sports(data.Dataset):
    def __init__(self, config, if_train=False,other_sports=False):
        # pdb.set_trace()
        if if_train:
            self.training = True
            source = config.train_source
            other_source = config.train_other_source
            self.short_size = 1080 #config.train_image_short_size
            self.max_size = 1920 #config.train_image_max_size
            self.width = config.width
            self.height = config.height
            # image_set = 'train'
        else:
            self.training = False
            source = config.eval_source
            other_source = config.eval_other_source
            self.short_size = 1080 #config.eval_image_short_size
            self.max_size = 1920 #config.eval_image_max_size
            self.width = config.width
            self.height = config.height
            # image_set='val'
        # self.records = misc_utils.load_json_lines(source)
        videos = os.listdir(source)
        self.images = []
        self.targets = []
        self.boxmap = []
        self.source = source
        self.image_ids = []
        self.tls=[]
        self.brs=[]
        image_id = 1
        for a_video in videos:
            frm_id = 1
            image_dirs = sorted(glob.glob(os.path.join(source, a_video, "img1","*.png")))
            tls = sorted(glob.glob(os.path.join(source, a_video, "fixed_cornermaps","tl","*.npz")))
            brs = sorted(glob.glob(os.path.join(source, a_video, "fixed_cornermaps","br","*.npz")))
            self.tls+=tls
            self.brs+=brs
            ann_path = os.path.join(source,a_video,"gt.csv")
            annotations = self.load_annotation(ann_path)
            for ind,a_dir in enumerate(image_dirs):
                self.images.append(a_dir)
                self.image_ids.append(image_id)
                frame_boxes = self.get_single_same_frame_objects(frm_id)
                self.targets.append(frame_boxes)
                frm_id = frm_id+1
                image_id = image_id + 1
        self.config = config
        if other_sports==True:
            videos = os.listdir(other_source)
            for a_video in videos:
                frm_id = 1
                image_dirs = sorted(glob.glob(os.path.join(other_source, a_video, "img1","*.png")))
                tls = sorted(glob.glob(os.path.join(other_source, a_video, "fixed_cornermaps","tl","*.npz")))
                brs = sorted(glob.glob(os.path.join(other_source, a_video, "fixed_cornermaps","br","*.npz")))
                self.tls+=tls
                self.brs+=brs
                ann_path = os.path.join(other_source,a_video,"gt.csv")
                annotations = self.load_annotation(ann_path)
                for ind,a_dir in enumerate(image_dirs):
                    self.images.append(a_dir)
                    self.image_ids.append(image_id)
                    frame_boxes = self.get_single_same_frame_objects(frm_id)

                    self.targets.append(frame_boxes)
                    frm_id = frm_id+1
                    image_id = image_id + 1
        # print(len(self.images))

        # total_img = int(len(self.images)/2)*2
<<<<<<< HEAD
        self.images = self.images[0:20]
        self.targets = self.targets[0:20]
        self.image_ids = self.image_ids[0:20]
        self.records = self.targets
=======



        self.images = self.images#+self.images[1000:1100]+self.images[4000:4100]+self.images[6000:6100]+self.images[10000:10100]
        self.targets = self.targets#+self.targets[1000:1100]+self.targets[4000:4100]+self.targets[6000:6100]+self.targets[10000:10100]
        self.image_ids = self.image_ids#+self.image_ids[1000:1100]+self.image_ids[4000:4100]+self.image_ids[6000:6100]+self.image_ids[10000:10100]
        self.records = self.targets#+self.targets[1000:1100]+self.targets[4000:4100]+self.targets[6000:6100]+self.targets[10000:10100]
        self.tls=self.tls#+self.tls[1000:1100]+self.tls[4000:4100]+self.tls[6000:6100]+self.tls[10000:10100]
        self.brs=self.brs#+self.brs[1000:1100]+self.brs[4000:4100]+self.brs[6000:6100]+self.brs[10000:10100]
>>>>>>> Add cornermap
        self.size = len(self.images)
        self.normalize = T.Normalize(mean=[0.406, 0.456, 0.485],std=[0.225, 0.224, 0.229])

        # pdb.set_trace()
    def __getitem__(self, index):

        image_path = self.images[index] #os.path.join(self.source, record['ID']+'.jpg') # Chaged from PNG
        image = cv2.imread(image_path, cv2.IMREAD_COLOR)
        image_h = image.shape[0]
        image_w = image.shape[1]

        # boxmap_image = image.copy()
        gtboxes = self.targets[index] #misc_utils.load_gt(record, 'gtboxes', 'fbox', self.config.class_names)
        # boxmap_image = self.boxmap_image_preprocess(boxmap_image)
        image = image.transpose(2, 0, 1)
        image = torch.tensor(image).float()
        gtboxes = np.array(gtboxes)
        gtboxes = gtboxes.astype('float64')
        target = {}
        target["boxes"] = torch.from_numpy(gtboxes[:,0:4])
        target["labels"] = torch.ones((len(gtboxes),), dtype=torch.int64)
        target["area"] = (gtboxes[:, 3] - gtboxes[:, 1]) * (gtboxes[:, 2] - gtboxes[:, 0])
        target["image_id"] = torch.tensor([self.image_ids[index]])
        target["iscrowd"] = torch.zeros((len(gtboxes),), dtype=torch.int64)
            # gtboxes = torch.tensor(gtboxes)
            
            # keep = (gtboxes[:, 2]>=0) * (gtboxes[:, 3]>=0)
            # gtboxes=gtboxes[keep, :]
            # gtboxes[:, 2:4] += gtboxes[:, :2]
            # im_info
        nr_gtboxes = gtboxes.shape[0]
        im_info = np.array([0, 0, 1, image_h, image_w, nr_gtboxes])
        tls=np.load(self.tls[index])['heatmap']
        brs=np.load(self.brs[index])['heatmap']
        return image,target, im_info,tls,brs,self.image_ids[index]
    def image_preprocess(self,image):
        # image = torch.tensor(image).float()
        # image=image.permute(2,0,1)
        image = image/255
        norm_im = self.normalize(image)
        # norm_im = norm_im.permute(1,2,0)
        return norm_im
    def validation_annotation(self):
        for a_id in self.image_ids:
            for ind,a_box in enumerate(self.targets[a_id]):
                single_box ={"fbox":a_box,"tag":"person","h_box":a_box,"extra":None,"vbox":a_box,"head_attr":None}
    def __len__(self):
        return self.size
    def merge_batch(self, data):
        # image
        images = [it[0] for it in data]
        gt_boxes = [it[1] for it in data]
        im_info = np.array([it[2] for it in data])
        batch_height = np.max(im_info[:, 3])
        batch_width = np.max(im_info[:, 4])
        padded_images = [pad_image(
                im, batch_height, batch_width, self.config.image_mean) for im in images]
        t_height, t_width, scale = target_size(
                batch_height, batch_width, self.short_size, self.max_size)
        # INTER_CUBIC, INTER_LINEAR, INTER_NEAREST, INTER_AREA, INTER_LANCZOS4
        resized_images = np.array([cv2.resize(
                im, (t_width, t_height), interpolation=cv2.INTER_LINEAR) for im in padded_images])
        resized_images = resized_images.transpose(0, 3, 1, 2)
        images = torch.tensor(resized_images).float()
        # ground_truth
        ground_truth = []
        for it in gt_boxes:
            gt_padded = np.zeros((self.config.max_boxes_of_image, self.config.nr_box_dim))
            it[:, 0:4] *= scale
            max_box = min(self.config.max_boxes_of_image, len(it))
            gt_padded[:max_box] = it[:max_box]
            ground_truth.append(gt_padded)
        ground_truth = torch.tensor(ground_truth).float()
        # im_info
        im_info[:, 0] = t_height
        im_info[:, 1] = t_width
        im_info[:, 2] = scale
        im_info = torch.tensor(im_info)
        if max(im_info[:, -1] < 2):
            return None, None, None
        else:
            return images, ground_truth, im_info

    def load_annotation(self,ann_path):
        annotations = []
        with open(ann_path, 'r') as csvfile:
            csv_read = csv.reader(csvfile, delimiter = ',')
            next(csv_read)
            for row in csv_read:
                if row[2]!= 'Others' and row[2]!= 'other' and row[2] != 'others' and row[2]!= 'Other':
                    x = int(float(row[10]))
                    y = int(float(row[9]))
                    width = int(float(row[11]))
                    height = int(float(row[12]))
                    frame_number = int(row[4].replace(".png",''))
                    annotations.append([frame_number,x,y,x+width,y+height])
        annotations = self.same_frame_objects(annotations)
        return annotations
    def same_frame_objects(self,annotations):
        framewise_order = sorted(annotations,key=lambda x: int(x[0]))
        prev_frame_number = 1
        single_frame_objs = []
        self.frame_objs = []
        for i in range(len(framewise_order)):
            if int(framewise_order[i][0])==prev_frame_number:
                single_frame_objs.append([framewise_order[i][1],framewise_order[i][2],framewise_order[i][3],framewise_order[i][4],1])
            else:
                if len(single_frame_objs)==0:
                    self.frame_objs.append("empty")
                else:
                    self.frame_objs.append(single_frame_objs)
                single_frame_objs=[]
                single_frame_objs.append([framewise_order[i][1],framewise_order[i][2],framewise_order[i][3],framewise_order[i][4],1])
            prev_frame_number=int(framewise_order[i][0])
        if len(single_frame_objs)==0:
            self.frame_objs.append("empty")
        else:
            self.frame_objs.append(single_frame_objs)
        return
    def get_single_same_frame_objects(self,frame_number):
        return self.frame_objs[int(frame_number)-1]  