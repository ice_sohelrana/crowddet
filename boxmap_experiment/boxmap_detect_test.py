import sys
import torch
import torch.nn as nn
import numpy
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image, ImageDraw
from pathlib import Path
from pycocotools.cocoeval import COCOeval
from pycocotools.coco import COCO
from pytorch_eval.coco_utils import get_coco_api_from_dataset
from pytorch_eval.coco_eval import CocoEvaluator
from torchvision import transforms
import pytorch_eval.utils
import pickle
# from mAP_evaluation.main import calculate_mAP
sys.path.insert(0,'./box_selector')
from selector import box_select
from util.centermap import render_centermap
from torchvision import datasets, transforms as T

from util.boxmap_represent import boxmapCreate
from util.metric_logger import MetricLogger
from util.utils import iou,ious
import pdb
import copy
import csv
import os
import glob
import copy
sys.path.insert(0, '../boxmap_model')
from utils.resize import fit

@torch.no_grad()
def detect(boxmap_model,prediction_model,dataset_test,ids,fig,_config,device,fname,output_save_dir):#,coco_evaluator):
	#coco evaluator set
	data_loader = torch.utils.data.DataLoader([dataset_test], batch_size=1, shuffle=False,num_workers=0)#,collate_fn=collate_fn)
	eval_output_stride = _config['boxmap']['eval_output_stride']
	n_threads = torch.get_num_threads()
	# FIXME remove this and make paste_masks_in_image run on the GPU
	torch.set_num_threads(1)
	cpu_device = torch.device("cpu")
	boxmap_model.eval()
	prediction_model.eval()
	metric_logger = MetricLogger(delimiter="  ")
	det_boxes = list()
	det_labels = list()
	det_scores = list()
	true_boxes = list()
	true_labels = list()
	true_difficulties = list() 
	header = 'Test:'
	result_dir=_config["output_dir"]
	save_error_dir = '/media/sohel/HDD2/detection_algorithms/optimize-boxes-for-boxmap/dataset/dataset_4'
	# if not os.path.exists(os.path.join(result_dir,_config['selector']['algorithm'],_config['selector']['boxmap_use'],seq)):
	# 	os.makedirs(os.path.join(result_dir,_config['selector']['algorithm'],_config['selector']['boxmap_use'],seq))
	# save_path = os.path.join(result_dir,_config['selector']['algorithm'],_config['selector']['boxmap_use'],seq)
	overall_mae = []
	old_ids = ids
	# pdb.set_trace()
	# dirs = '/media/sohel/HDD2/detection_algorithms/boxmap/box_selector/dataset'
	# for images, targets,videoname,frm_number in metric_logger.log_every(data_loader, 100, header):
	for images, targets,im_info,image_ids in metric_logger.log_every(data_loader, 100, header):
		images = images.to(device)
		im_info = im_info.to(device)
		# im_info = list(inf.to(device) for inf in im_info)
		original_img = copy.deepcopy(images) 
		prediction_boxes = prediction_model(images,im_info)
		prediction_boxes = emd_refine_box_preprocess(prediction_boxes,top_k=2,model_type=_config['predictor']['predictor_name'])
		# prediction_boxes = prediction_boxes.numpy()
		if _config['selector']['boxmap_use']=='gt':
			boxmap_out = boxmapCreate(targets['boxes'][0].cpu().numpy(),(1080,1920))
			centermap=render_centermap(boxmap_out.shape,targets['boxes'][0].cpu().numpy())
		else:
			if _config['boxmap']['image_pred_fusion']==True:
				boxmap_inps = []
				# pdb.set_trace()
				# for a_preds,a_img in zip(prediction_boxes,images):
				boxmap = boxmapCreate(prediction_boxes[:,0:4],(1080,1920))
				# pdb.set_trace()
				image = image_preprocess(original_img[0])
				boxmap = torch.from_numpy(boxmap)
				boxmap=boxmap.type(torch.FloatTensor)
				if _config['boxmap']['score_fusion'] == True:
					ranges=[0.20,0.40,0.60,0.80,1.00]
					sm1 = boxmapCreate(prediction_boxes[prediction_boxes[:,4]<ranges[0]],(1080,1920))
					sm1 = torch.from_numpy(sm1)
					sm1 = sm1.type(torch.FloatTensor)
					sm2 = boxmapCreate(prediction_boxes[prediction_boxes[:,4]<ranges[1]],(1080,1920))
					sm2 = torch.from_numpy(sm2)
					sm2=sm2.type(torch.FloatTensor)
					sm3 = boxmapCreate(prediction_boxes[prediction_boxes[:,4]<ranges[2]],(1080,1920))
					sm3 = torch.from_numpy(sm3)
					sm3=sm3.type(torch.FloatTensor)
					sm4 = boxmapCreate(prediction_boxes[prediction_boxes[:,4]<ranges[3]],(1080,1920))
					sm4 = torch.from_numpy(sm4)
					sm4=sm4.type(torch.FloatTensor)
					sm5 = boxmapCreate(prediction_boxes[prediction_boxes[:,4]<ranges[4]],(1080,1920))
					sm5 = torch.from_numpy(sm5)
					sm5 = sm5.type(torch.FloatTensor)
					scoremap = torch.cat((sm1.unsqueeze(dim=0).to(device),sm2.unsqueeze(dim=0).to(device),sm3.unsqueeze(dim=0).to(device),sm4.unsqueeze(dim=0).to(device),sm5.unsqueeze(dim=0).to(device)),dim=0)
					boxmap_inp = torch.cat((image,boxmap.unsqueeze(dim=0).to(device),scoremap),dim=0)
				else:
					boxmap_inp = torch.cat((image,boxmap.unsqueeze(dim=0).to(device)),dim=0)
				boxmap_inp = fit(boxmap_inp.unsqueeze(dim=0), (1088,1920), fit_mode='contain').squeeze(dim=0)
				boxmap_inps.append(boxmap_inp)
			elif _config['boxmap']['image_pred_fusion']==False:
				boxmap_inps = []
				image = data_loader.dataset.image_preprocess(original_img[0])
				boxmap_inp = fit(image.unsqueeze(dim=0), (1088,1920), fit_mode='contain').squeeze(dim=0)
				boxmap_inps.append(boxmap_inp)
			else:
				raise NotImplementedError('This model is not implemented')
			# pdb.set_trace()
			boxmap_out=boxmap_model(torch.stack(boxmap_inps))
			# boxmap_out = torch.argmax(boxmap_out, dim=1)
			# upsample = nn.Upsample(size=(boxmap_out.shape[2]*eval_output_stride,boxmap_out.shape[3]*eval_output_stride), mode='bilinear',align_corners=True)
			# boxmap_out = upsample(boxmap_out)
			boxmap_out[boxmap_out<0.50]=0
			boxmap_out = torch.round(boxmap_out)
			boxmap_out=boxmap_out.squeeze(dim=0).squeeze(dim=0)
			# boxmap_out = boxmap_out.squeeze(dim=0)
			
			boxmap_out = boxmap_out[0:1080,].cpu().numpy()

		
		#This part is for check each overlap class evaluation
		target_overlaps = ious(targets['boxes'][0],targets['boxes'][0])
		keep = []
		for inds,a_overlap in enumerate(target_overlaps):
			n_overlap = len(torch.where(a_overlap!=0)[0])-1
			if n_overlap==0:
				keep.append(inds)
		if keep!=[]:
			# pdb.set_trace()
			targets['boxes'] = targets['boxes'][0][keep]
			targets['labels'] = targets['labels'][0][keep]
			targets['area'] = targets['area'][0][keep]
		else:
			targets['boxes'] = torch.zeros((0,4))
			targets['labels'] = torch.zeros((0))
			targets['area'] = torch.zeros(0)
		
		# GT boxmap
		# gt_boxmap_out = boxmapCreate(targets[0]['boxes'].cpu().numpy(),(1080,1920))
		# boxmap_out[gt_boxmap_out==0]=0
		# pdb.set_trace()
		# # boxes = resize_boxes(targets[0]['boxes'],(749,1333),(1080,1920))

		# path = os.path.join(dirs,'gt_boxes','8.npy')
		# np.save(path,targets[0]['boxes'].numpy())

		# path = os.path.join(dirs,'gt_boxmap','8.npy')
		# np.save(path,gt_boxmap_out)
		# path = os.path.join(dirs,'pred_boxmap','8.npy')
		# np.save(path,boxmap_out)

		# # boxmap = torch.from_numpy(boxmap).to(device)
		# scores = prediction_boxes[0]['scores'].unsqueeze(dim=1)
		# pred_boxes = torch.cat((prediction_boxes[0]['boxes'],scores),dim=1)
		# pred_boxes = pred_boxes.cpu().numpy()
		# prediction_boxes = prediction_boxes[:,0:5].numpy()

		# path = os.path.join(dirs,'pred_boxes','8.npy')
		# np.save(path,pred_boxes)
		# pdb.set_trace()
		setnms_preds = copy.deepcopy(prediction_boxes)
		pixelvoting_boxes,mae,sel_boxmap = box_select([boxmap_out],[prediction_boxes[:,0:5]],[targets['boxes'].cpu().numpy()],_config['selector'])
		# cocoEval = COCOeval(iouType='bbox')

		# pdb.set_trace()
		# This part is for check each overlap class evaluation
		pred_overlaps = ious(pixelvoting_boxes[:,0:4],pixelvoting_boxes[:,0:4])
		keep = []
		for inds,a_overlap in enumerate(pred_overlaps):
			n_overlap = len(np.where(a_overlap!=0)[0])-1
			if n_overlap==0:
				keep.append(inds)
		if keep!=[]:
			pixelvoting_boxes = pixelvoting_boxes[keep]
		else:
			pixelvoting_boxes = np.zeros((0,5))
		# pdb.set_trace()
		# pixelvoting_boxes[:,4]=1
		iou_types = ["bbox"]
		copy_dataset = copy.deepcopy(dataset_test)
		orig_dataset = copy.deepcopy(dataset_test)
		coco_model = get_coco_api_from_dataset([dataset_test])

		coco_evaluator_model = CocoEvaluator(coco_model, iou_types)
		predictions = coco_evaluator_model.convert_sports_to_coco_detection(pixelvoting_boxes)
		res = {targets['image_id'].item():predictions[0]}
		# res = {target["image_id"].item(): output for target, output in zip(targets, predictions)}
		coco_evaluator_model.update(res)
		coco_evaluator_model.synchronize_between_processes()
		coco_evaluator_model.accumulate()
		coco_evaluator_model.summarize()
		pr_recall_model = coco_evaluator_model.coco_eval['bbox'].stats
		# del coco_evaluator_model
		# del coco_model
		nms_boxes,opt_mae,opt_sel_boxmap = box_select([boxmap_out],[setnms_preds],[targets['boxes'].cpu().numpy()],_config['optional_selector'])
		# nms_boxes[:,4]=1
		pred_overlaps = ious(nms_boxes[:,0:4],nms_boxes[:,0:4])
		keep = []
		for inds,a_overlap in enumerate(pred_overlaps):
			n_overlap = len(np.where(a_overlap!=0)[0])-1
			if n_overlap==0:
				keep.append(inds)
		if keep!=[]:
			nms_boxes = nms_boxes[keep]
		else:
			nms_boxes = np.zeros((0,5))


		coco_nms = get_coco_api_from_dataset([copy_dataset])
		coco_evaluator_nms = CocoEvaluator(coco_nms, iou_types)
		nms_predictions = coco_evaluator_nms.convert_sports_to_coco_detection(nms_boxes[:,0:5])
		nms_res = {targets['image_id'].item():nms_predictions[0]} #{target["image_id"].item(): output for target, output in zip(targets, predictions)}
		coco_evaluator_nms.update(nms_res)
		coco_evaluator_nms.synchronize_between_processes()
		coco_evaluator_nms.accumulate()
		coco_evaluator_nms.summarize()
		pr_recall_nms = coco_evaluator_nms.coco_eval['bbox'].stats




		# pr_recall_model = coco_evaluator_nms.coco_eval['bbox'].stats
		# predictions = nms_predictions
		if pr_recall_nms[4]>pr_recall_model[4]:
		# pr_recall_nms=0
		# if pr_recall_model[4]==1.0:
			# pdb.set_trace()
			pred_gt_ious = ious(targets['boxes'].numpy(),prediction_boxes[:,:4])
			f=open(os.path.join(save_error_dir,'gt_boxes',str(old_ids)+'.npy'), 'wb')
			np.save(f,targets['boxes'].numpy())
			f.close()
			f=open(os.path.join(save_error_dir,'gt_boxmap',str(old_ids)+'.npy'), 'wb')
			np.save(f,boxmap_out)
			f.close()
			f=open(os.path.join(save_error_dir,'pred_boxes',str(old_ids)+'.npy'), 'wb')
			np.save(f,prediction_boxes)
			f.close()

			for ids,a_targ_ious in enumerate(pred_gt_ious):
				indice = np.where(a_targ_ious>=0.50)
				if ids==0:
					indices=indice[0]
				else:
					indices = np.concatenate((indices,indice[0]),axis=0)
			pred_indices = list(set(indices))
			prediction_boxes = prediction_boxes[pred_indices]
			plt.clf()
			im = original_img[0].permute(1,2,0).cpu().numpy()
			# im = im.astype('int')
			# pdb.set_trace()
			im = Image.fromarray(np.uint8(im))
			# im.save('./output/data/images/'+str(ids)+'.png')
			# f = open('./output/data/gtboxes/'+str(ids)+'.pth', 'wb')
			# torch.save(orig_dataset,f)
			# f = open('./output/data/pixelvotingboxes/'+str(ids)+'.npy', 'wb')
			# np.save(f,pixelvoting_boxes)
			# f = open('./output/data/setnmsboxes/'+str(ids)+'.npy', 'wb')
			# np.save(f,nms_boxes[:,0:5])
			# pdb.set_trace()
			# cp_im = im.copy()

			draw1 = ImageDraw.Draw(im)
			for a_box in targets['boxes'].cpu().numpy(): #targets['boxes'][0]
				draw1.rectangle([int(a_box[0]),int(a_box[1]),int(a_box[2]),int(a_box[3])],fill=None,outline="green",width=3)
			for a_box in nms_boxes[:,0:4]: #targets['boxes'][0]
				draw1.rectangle([int(a_box[0]),int(a_box[1]),int(a_box[2]),int(a_box[3])],fill=None,outline="blue",width=2)			
			for a_box in pixelvoting_boxes[:,0:4]:
				draw1.rectangle([int(a_box[0]),int(a_box[1]),int(a_box[2]),int(a_box[3])],fill=None,outline="red",width=1)
			im.save("./output/no_overlap_error/"+str(old_ids)+"_"+str(pr_recall_model[4])+'.png')
			# draw2 = ImageDraw.Draw(cp_im)
			# for a_box in targets['boxes'][0]:
			# 	draw2.rectangle([int(a_box[0]),int(a_box[1]),int(a_box[2]),int(a_box[3])],fill=None,outline="green",width=2)
			# for a_box in nms_boxes[:,0:5]:
			# 	draw2.rectangle([int(a_box[0]),int(a_box[1]),int(a_box[2]),int(a_box[3])],fill=None,outline="red",width=2)
			# fig, ax = plt.subplots(nrows=1, ncols=1)
			# fig = plt.gcf()
			# fig.set_size_inches(20, 15)
			# # fig.set_size_inches(40, 25)

			# ax[0].imshow(im)
			# # ax[1].imshow(cp_im)
			# ax[0].set_title("pixelvoting"+"_mAP_"+str(pr_recall_model[1])+"_recall_"+str(pr_recall_model[4]),fontsize= 20)
			# # ax[1].set_title("setnms"+"_mAP_"+str(pr_recall_nms[1])+"_recall_"+str(pr_recall_nms[4]),fontsize= 20)
			# plt.savefig('./output/opt_error/'+str(ids)+'.png')
			# # if pr_recall_nms[4]>pr_recall_model[4]:
			# # 	plt.savefig('./output/recall_error/'+str(ids)+'.png')
			# # if pr_recall_nms[4]>pr_recall_model[4]:
			# # 	plt.savefig('./output/results/'+str(ids)+'.png')
			# plt.close()
		# pdb.set_trace()
		overall_mae = overall_mae+mae
		# img = original_img[0].cpu().permute(1,2,0).numpy()
		# im = Image.fromarray(np.uint8(img*255))		
		# draw = ImageDraw.Draw(im)

		# for a_box in final_boxes:
		# 	draw.rectangle([int(a_box[0]),int(a_box[1]),int(a_box[2]),int(a_box[3])],outline="red",fill=None)
		# for a_box in targets[0]["boxes"]:
			# draw.rectangle([int(a_box[0]),int(a_box[1]),int(a_box[2]),int(a_box[3])],outline="green",fill=None)
		# plt.imshow(im)
		# plt.show()
		# im.save(os.path.join(save_path,str(frm_number[0])+".jpg"))
		# with open(os.path.join(output_save_dir,"preds",str(fname)+".txt"), mode='w') as det_file:
		# 	det_writer = csv.writer(det_file, delimiter=' ', quotechar='"', quoting=csv.QUOTE_MINIMAL)
		# 	for a_box in final_boxes:
		# 		det_writer.writerow([a_box[0],a_box[1],a_box[2],a_box[3],a_box[4]])

		# with open(os.path.join(output_save_dir,"gts",str(fname)+".txt"), mode='w') as gt_file:
		# 	gt_writer = csv.writer(gt_file, delimiter=' ', quotechar='"', quoting=csv.QUOTE_MINIMAL)
		# 	for a_box in targets[0]['boxes']:
		# 		gt_writer.writerow([a_box[0].item(),a_box[1].item(),a_box[2].item(),a_box[3].item()])

		fname=fname+1

	# pdb.set_trace()
	print('Overall MAE: {0}'.format(sum(overall_mae)/len(overall_mae)))
	return overall_mae,fname,pr_recall_model,pr_recall_nms#,final_boxes,boxmap_out,targets[0]['boxes'].cpu().numpy(),nms_boxes
	# pdb.set_trace()
		# det_boxes.extend([torch.from_numpy(final_boxes[:,0:4])])
		# det_scores.extend([torch.from_numpy(final_boxes[:,4])])
		# # labels = targets[0]['labels'][0:len(final_boxes)]
		# labels = torch.ones(len(final_boxes))
		# det_labels.extend([labels])
		# # pdb.set_trace()
		# true_boxes.extend([targets[0]['boxes']])
		# true_labels.extend([targets[0]['labels']])
		# true_difficulties.extend([labels[0:len(targets[0]['boxes'])]])
	# APs, mAP=calculate_mAP(det_boxes, det_labels, det_scores, true_boxes, true_labels, true_difficulties)
	# print('\nMean Average Precision (mAP): %.3f' % mAP)


def emd_refine_box_preprocess(pred_boxes,top_k,model_type):
	if model_type=='rcnn-emd-refine':
		n = pred_boxes.shape[0]
		pred_boxes = pred_boxes.reshape(-1, 6)
		idents = np.tile(np.arange(n)[:,None], (1, top_k)).reshape(-1, 1)
		pred_boxes = np.hstack((pred_boxes, idents))
		keep = pred_boxes[:, 4] > 0.01
		pred_boxes = pred_boxes[keep]
	if model_type=='rcnn-fpn-baseline':
		assert pred_boxes.shape[-1] % 6 == 0, "Prediction dim Error!"
		pred_boxes = pred_boxes.reshape(-1, 6)
		keep = pred_boxes[:, 4] > 0.01
		pred_boxes = pred_boxes[keep]
	return pred_boxes

def remove_small_boxes(boxes, min_size):
	# type: (Tensor, float) -> Tensor
	"""
	Remove boxes which contains at least one side smaller than min_size.
	Arguments:
		boxes (Tensor[N, 4]): boxes in (x1, y1, x2, y2) format
		min_size (float): minimum size
	Returns:
		keep (Tensor[K]): indices of the boxes that have both sides
			larger than min_size
	"""
	ws, hs = boxes[:, 2] - boxes[:, 0], boxes[:, 3] - boxes[:, 1]
	keep = (ws >= min_size) & (hs >= min_size)
	keep = keep.nonzero().squeeze(1)
	return keep
normalize=T.Normalize(mean=[0.406, 0.456, 0.485],std=[0.225, 0.224, 0.229])
def image_preprocess(image):
    # image = torch.tensor(image).float()
    # image=image.permute(2,0,1)
    image = image/255
    norm_im = normalize(image)
    # norm_im = norm_im.permute(1,2,0)
    return norm_im