import json
import os
# from vision import VisionDataset
from torch.utils.data import Dataset, DataLoader
from PIL import Image, ImageDraw
import matplotlib.pyplot as plt

import glob
import pdb
import csv
import numpy as np
import torch
import natsort
import os
from torchvision import transforms
import torchvision.models as models
import sys
import sacred
import random
import time
from boxmap_detect import detect
from mAP.calc_mAP import calc_mAP

from pytorch_eval.coco_utils import get_coco_api_from_dataset
from pytorch_eval.coco_eval import CocoEvaluator
import pytorch_eval.utils
# from boxmap_detect_multithread import detect
from precision_recall import PRgraph
sys.path.insert(0, '..')
from get_model import prediction_model_load,boxmap_model_load
sys.path.insert(0, '../frcnn_fpn')
from misc import collate_fn
from frcnn_transforms import image_transforms
class Sport(Dataset):
	def __init__(self, data_path, image_set='train',videoname=None,frame=None,
				 transforms=None):
		super(Sport, self).__init__()
		# self.mode = 'gtFine' if mode == 'fine' else 'gtCoarse'
		self.images = []
		self.targets = []
		self.boxmaps = []
		self.data_path = data_path

		# if image_set=="train":
		# 	videos = ["14_netball_1","16_netball_3"]#,"15_netball_2","16_netball_3","17_netball_4","18_netball_5","19_netball_6","23_netball_10","24_netball_11","26_netball_13",] #videos[0:1]
		# else:
		# 	videos = ["21_netball_8"] #videos[2:3]
		#val
		self.ids = []
		self.image_ids = []
		image_ids = 1
		self.frm_ids=[]
		self.videonames = []
		frm_id = int(frame.split('.')[0])
		image_dirs = os.path.join(self.data_path,image_set, videoname, "img1",frame)
		# image_dirs = image_dirs #300:350
		# box_dirs = sorted(glob.glob(os.path.join(self.data_path, a_video, "masks","*.png")))
		# box_files = os.listdir(os.path.join(self.data_path,image_set, a_video, "masks"))
		# box_files = natsort.natsorted(box_files)
		# box_files = box_files.sort(key=lambda f: int(filter(str.isdigit, f)))
		ann_path = os.path.join(self.data_path,image_set,videoname,"gt.csv")
		annotations = self.load_annotation(ann_path)
		# if image_set=="train":
		# 	minind = 370#val
		# 	maxind = 370+28#val+1
		# else:
		# 	minind = 370#val#500
		# 	maxind = 370+28#val+1 #750
		# for ind,(a_dir,a_box_file) in enumerate(zip(image_dirs[minind:maxind],box_files[minind:maxind])):
		# for ind,(a_dir,a_box_file) in enumerate(zip(image_dirs,box_files)):
		self.images.append(image_dirs)
		frame_boxes = self.get_single_same_frame_objects(int(frame.split('.')[0]))
		self.targets.append(frame_boxes)
		self.frm_ids.append(frm_id)
		self.videonames.append(videoname)
		self.image_ids.append(image_ids)
		frm_id = frm_id+1				
		image_ids=image_ids+1		
		self.transforms = transforms
	def load_annotation(self,ann_path):
		annotations = []
		with open(ann_path, 'r') as csvfile:
			csv_read = csv.reader(csvfile, delimiter = ',')
			next(csv_read)
			for row in csv_read:
				if row[2]!= 'Others' and row[2]!= 'other' and row[2] != 'others' and row[2]!= 'Other':
					x = int(float(row[10]))
					y = int(float(row[9]))
					width = int(float(row[11]))
					height = int(float(row[12]))
					frame_number = int(row[4].replace(".png",''))
					annotations.append([frame_number,x,y,x+width,y+height])
		annotations = self.same_frame_objects(annotations)
		return annotations
	def same_frame_objects(self,annotations):
		framewise_order = sorted(annotations,key=lambda x: int(x[0]))
		prev_frame_number = 1
		single_frame_objs = []
		self.frame_objs = []
		for i in range(len(framewise_order)):
			if int(framewise_order[i][0])==prev_frame_number:
				single_frame_objs.append([framewise_order[i][1],framewise_order[i][2],framewise_order[i][3],framewise_order[i][4]])
			else:
				if len(single_frame_objs)==0:
					self.frame_objs.append("empty")
				else:
					self.frame_objs.append(single_frame_objs)
				single_frame_objs=[]
				single_frame_objs.append([framewise_order[i][1],framewise_order[i][2],framewise_order[i][3],framewise_order[i][4]])
			prev_frame_number=int(framewise_order[i][0])
		if len(single_frame_objs)==0:
			self.frame_objs.append("empty")
		else:
			self.frame_objs.append(single_frame_objs)
		return
	def get_single_same_frame_objects(self,frame_number):
		return self.frame_objs[int(frame_number)-1]    
	def __getitem__(self, index):
		image = Image.open(self.images[index]).convert('RGB')
		# pdb.set_trace()
		boxes = self.targets[index]
		boxes = torch.as_tensor(boxes,dtype=torch.float32)
		boxes[:, 0::2].clamp_(min=0, max=image.size[0]) 
		boxes[:, 1::2].clamp_(min=0, max=image.size[1]) 

		keep = (boxes[:, 3] > boxes[:, 1]) & (boxes[:, 2] > boxes[:, 0])
		boxes = boxes[keep]

		area = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])
		labels = torch.ones((len(boxes),), dtype=torch.int64)
		image_id = torch.tensor([self.image_ids[index]])
		iscrowd = torch.zeros((len(boxes),), dtype=torch.int64)
		# boxes["boxes"] = boxes
		target = {}
		target["boxes"] = boxes
		target["labels"] = labels
		target["area"] = area
		target["image_id"] = image_id
		target["iscrowd"] = iscrowd
		if self.transforms is not None:
			image,_ = self.transforms(image)
			# boxmap = boxmap.type(torch.FloatTensor)	
			# image, target = self.transforms(image, target)
		return image, target#,self.videonames[index],self.frm_ids[index]

	def __len__(self):
		return len(self.images)
def display_image(orig_img,pred_boxes,gt_boxes,boxmap,target_boxmap,nms_boxes,ind):
	img1 = orig_img.copy()
	draw = ImageDraw.Draw(img1)
	for a_box in gt_boxes:   
		draw.rectangle([int(a_box[0]),int(a_box[1]),int(a_box[2]),int(a_box[3])],outline ="green",width=2) 
	for indx,a_box in enumerate(pred_boxes):   
		draw.rectangle([int(a_box[0]),int(a_box[1]),int(a_box[2]),int(a_box[3])],outline ="red",width=2) 
	img2 = orig_img.copy()
	draw = ImageDraw.Draw(img2)
	for a_box in gt_boxes:   
		draw.rectangle([int(a_box[0]),int(a_box[1]),int(a_box[2]),int(a_box[3])],outline ="green",width=2) 
	for a_box in nms_boxes:   
		draw.rectangle([int(a_box[0]),int(a_box[1]),int(a_box[2]),int(a_box[3])],outline ="red",width=2) 
		
	f, axarr = plt.subplots(2,2)
	f = plt.gcf()
	f.set_size_inches(50, 40)
	axarr[0][0].set_title('Boxmap Detection',fontsize = 50)
	axarr[0][0].imshow(img1)

	axarr[0][1].set_title('NMS Detection',fontsize = 50)
	axarr[0][1].imshow(img2)

	axarr[1][0].set_title('Target Boxmap',fontsize = 50)
	axarr[1][0].imshow(target_boxmap)

	axarr[1][1].set_title('Output Boxmap',fontsize = 50)
	axarr[1][1].imshow(boxmap)


	plt.savefig('./output/50/bottom_sample/'+str(ind)+'_gt.png')
	# plt.show()
	# pdb.set_trace()
	# axarr[1][0].colorbar()
	# plt.colorbar(im, ax=axarr[1][0])
	
	
# m

from sacred import Experiment
ex = Experiment()
ex.add_config('./cfgs/config.yaml')
# detect = ex.capture(detect)
@ex.automain
def my_main(_config,_log,_run):
	sacred.commands.print_config(_run)
	torch.manual_seed(_config['seed'])
	torch.cuda.manual_seed(_config['seed'])
	np.random.seed(_config['seed'])
	torch.backends.cudnn.deterministic = True
	if torch.cuda.is_available():
		device = torch.device('cuda', torch.cuda.current_device())
		torch.backends.cudnn.benchmark = True
	else:
		device = torch.device('cpu')
	_log.info("Initializing object detector.")
	print('Loading models')
	prediction_model = prediction_model_load(_config['predictor']['weight'],device)
	boxmap_model= boxmap_model_load(_config['boxmap'],device)
	#_config['data_dir']
	seqs = os.listdir(os.path.join(_config['dataset']['data_dir'],_config['dataset']['image_set']))
	output_save_dir = os.path.join(_config["output_dir"],_config['selector']['algorithm'])
	overall_mae=[]
	fname=0	
	gt_boxes = []
	for seq in seqs:
		start = time.time()
		print(seq)
		# imgs = os.listdir(os.path.join(_config['dataset']['data_dir'],_config['dataset']['image_set'],seq,'img1'))
		imgs = ['00000027.png','00000036.png','00000038.png','00000035.png','00000017.png','00000024.png','00000037.png','00000025.png','00000108.png','00000019.png']
		all_maps = []
		all_mae = []
		pr_graph = PRgraph(0.50)
		all_gt_boxes = []
		all_pred_boxes = []
		for ind,img in enumerate(imgs):
			dataset_test=Sport(_config['dataset']['data_dir'],_config['dataset']['image_set'],videoname=seq,frame=img,transforms=image_transforms)
			data_loader_test = torch.utils.data.DataLoader(dataset_test, batch_size=1, num_workers=0,collate_fn=collate_fn)
			
			coco = get_coco_api_from_dataset(data_loader_test.dataset)
			iou_types = ["bbox"]
			coco_evaluator = CocoEvaluator(coco, iou_types)
			
			mae,fname,pred_boxes,boxmap_out,gt_boxes,nms_boxes=detect(boxmap_model, prediction_model, data_loader_test, _config,seq,device,fname,output_save_dir,coco_evaluator)
			# maps = calc_mAP([0.05,0.50,0.70,0.95],output_save_dir)
			orig_img = Image.open(os.path.join(_config['dataset']['data_dir'],_config['dataset']['image_set'],seq,'img1',img))
			target_boxmap = Image.open(os.path.join(_config['dataset']['data_dir'],_config['dataset']['image_set'],seq,'masks',str(int(img.split('.')[0]))+'.png'))
			display_image(orig_img,pred_boxes,gt_boxes,boxmap_out,target_boxmap,nms_boxes,ind)
			coco_evaluator.synchronize_between_processes()
			coco_evaluator.accumulate()
			coco_evaluator.summarize()
			pdb.set_trace()
			# all_gt_boxes = all_gt_boxes + [*gt_boxes]
			# all_pred_boxes = all_pred_boxes + [*pred_boxes]
			print(len(pred_boxes),len(nms_boxes))
			# all_maps.append(maps)
			all_mae.append(mae)
		# all_pred_boxes = np.stack(all_pred_boxes)
		# all_pred_boxes[:,4]=1
		# pr_graph.single_video_precision_recall_graph_generation(all_pred_boxes,np.stack(all_gt_boxes),seq)
		pdb.set_trace()
		# seq_process(_config,seq,image_transforms,boxmap_model,prediction_model,device,fname,collate_fn)
		print("Time: {0}".format(time.time()-start))
		overall_mae=overall_mae+mae
	print('Overall MAE: {0} for all video'.format(sum(overall_mae)/len(overall_mae)))
	# calc_mAP([0.05,0.50,0.70,0.95],output_save_dir)