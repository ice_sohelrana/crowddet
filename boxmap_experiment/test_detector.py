import argparse
import os

import torch
import torch.backends.cudnn
from torch.nn import DataParallel
from torch.optim.rmsprop import RMSprop
from torch.utils.data import DataLoader
from tqdm import trange, tqdm
# from stacked_hourglass.model import hg1, hg2, hg8
# from stacked_hourglass.datasets.mpii import Mpii
# from stacked_hourglass.datasets.sports import sports

# from stacked_hourglass.train import do_training_epoch, do_validation_epoch
# from stacked_hourglass.train_fcn import do_training_epoch, do_validation_epoch

# from stacked_hourglass.utils.logger import Logger
# from stacked_hourglass.utils.misc import save_checkpoint, adjust_learning_rate
import pdb
from torchvision import transforms
import torchvision.models as models
import sys
import sacred
import pdb
sys.path.insert(0, '..')
from dataset.sport import Sport
from boxmap_detect import detect
from get_model import prediction_model_load,boxmap_model_load
sys.path.insert(0, '../frcnn_fpn')
from misc import collate_fn
from frcnn_transforms import image_transforms
# 
from sacred import Experiment
ex = Experiment()
ex.add_config('./cfgs/config.yaml')

@ex.automain()
def main(args):
	if torch.cuda.is_available():
		device = torch.device('cuda', torch.cuda.current_device())
		torch.backends.cudnn.benchmark = True
	else:
		device = torch.device('cpu')
	torch.set_grad_enabled(False)
	train_output_stride = 8
	eval_output_stride = 8
	# os.makedirs(args.checkpoint, exist_ok=True)

	print("Creating data loaders")
	dataset_test= Sport(args.data_path, image_set="val", transforms=image_transforms)
	data_loader_test = torch.utils.data.DataLoader(dataset_test, batch_size=1, num_workers=args.workers,collate_fn=collate_fn)
	
	prediction_model = prediction_model_load(args.pred_weights,device)
	boxmap_model= boxmap_model_load(args.boxmap_weights,device)
	detect(boxmap_model, prediction_model, data_loader_test, device)
if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Train a stacked hourglass model.')
	# Dataset setting
	parser.add_argument('--data-path', default='/media/sohel/HDD2/googledrive/detection_sports/netball_bbl/netball', type=str,
						help='path to images')
	parser.add_argument('--pred-weights', default="/media/sohel/HDD2/detection_algorithms/boxmap/frcnn_fpn/weights/model_26.pth", type=str,
						help='path to images')
	parser.add_argument('--boxmap-weights', default="/media/sohel/HDD2/detection_algorithms/boxmap/results/weights_8_det+boxmap.pth", type=str,
						help='path to images')
	# Model structure
	parser.add_argument('--arch', '-a', metavar='ARCH', default='resnet_fcn',
						choices=['hg1', 'hg2', 'hg8','resnet_fcn'],
						help='model architecture')
	# Training strategy
	parser.add_argument('-j', '--workers', default=4, type=int, metavar='N',
						help='number of data loading workers (default: 4)')
	parser.add_argument('--epochs', default=1, type=int, metavar='N',
						help='number of total epochs to run')
	parser.add_argument('--start-epoch', default=0, type=int, metavar='N',
						help='manual epoch number (useful on restarts)')
	parser.add_argument('--train-batch', default=1, type=int, metavar='N',
						help='train batchsize')
	parser.add_argument('--test-batch', default=1, type=int, metavar='N',
						help='test batchsize')
	parser.add_argument('--lr', '--learning-rate', default=2.5e-4, type=float,
						metavar='LR', help='initial learning rate')
	parser.add_argument('--momentum', default=0, type=float, metavar='M',
						help='momentum')
	parser.add_argument('--weight-decay', '--wd', default=0, type=float,
						metavar='W', help='weight decay (default: 0)')
	parser.add_argument('--schedule', type=int, nargs='+', default=[60, 90],
						help='Decrease learning rate at these epochs.')
	parser.add_argument('--gamma', type=float, default=0.1,
						help='LR is multiplied by gamma on schedule.')
	# Miscs
	parser.add_argument('-c', '--checkpoint', default='checkpoint', type=str, metavar='PATH',
						help='path to save checkpoint (default: checkpoint)')
	parser.add_argument('--snapshot', default=0, type=int,
						help='save models for every #snapshot epochs (default: 0)')
	parser.add_argument('--resume', default='', type=str, metavar='PATH',
						help='path to latest checkpoint (default: none)')

	main(parser.parse_args())
