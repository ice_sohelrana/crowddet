import sys
import torch
import torch.nn as nn
import numpy
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image, ImageDraw
from pathlib import Path
import sys
import pickle
# from mAP_evaluation.main import calculate_mAP
# sys.path.insert(0,'..')
# from load_im import load_im,boxmap_padding

# sys.path.insert(0, '../frcnn_fpn')
# from detection.transform import resize_boxes
sys.path.insert(0,'./box_selector')
from selector import box_select
from util.boxmap_represent import boxmapCreate
from util.centermap import render_centermap

from util.metric_logger import MetricLogger
from util.utils import iou,ious
import pdb
import copy
import csv
import os
import glob
sys.path.insert(0, '../boxmap_model')
from utils.resize import fit
@torch.no_grad()
def detect(boxmap_model,prediction_model,data_loader,_config,seq,device,fname,output_save_dir,coco_evaluator):
	#coco evaluator set
	


	eval_output_stride = _config['boxmap']['eval_output_stride']
	n_threads = torch.get_num_threads()
	# FIXME remove this and make paste_masks_in_image run on the GPU
	torch.set_num_threads(1)
	cpu_device = torch.device("cpu")
	if _config['selector']['boxmap_use']=='model':
		boxmap_model.eval()
	prediction_model.eval()
	metric_logger = MetricLogger(delimiter="  ")
	det_boxes = list()
	det_labels = list()
	det_scores = list()
	true_boxes = list()
	true_labels = list()
	true_difficulties = list() 
	header = 'Test:'
	result_dir=_config["output_dir"]
	# if not os.path.exists(os.path.join(result_dir,_config['selector']['algorithm'],_config['selector']['boxmap_use'],seq)):
	# 	os.makedirs(os.path.join(result_dir,_config['selector']['algorithm'],_config['selector']['boxmap_use'],seq))
	save_path = os.path.join(result_dir,_config['selector']['algorithm'],_config['selector']['boxmap_use'],seq)
	overall_mae = []
	evaluate_dir = os.path.join(_config['output_dir'],'evaluation')
	boxmap_dir = os.path.join(_config['output_dir'],'boxmap')
	# dirs = '/media/sohel/HDD2/detection_algorithms/boxmap/box_selector/dataset'
	# for images, targets,videoname,frm_number in metric_logger.log_every(data_loader, 100, header):
	all_img_ids = []
	for images,targets,im_info,tl_maps,br_maps,image_ids in metric_logger.log_every(data_loader, 100, header):
		images = images.to(device)
		im_info = im_info.to(device)
		# im_info = list(inf.to(device) for inf in im_info)
		original_img = copy.deepcopy(images) 
		prediction_boxes = prediction_model(images,im_info)
		# pdb.set_trace()
		prediction_boxes = emd_refine_box_preprocess(prediction_boxes,top_k=2,model_type='fpn')
		prediction_boxes = prediction_boxes.numpy()
		# prediction_boxes = prediction_boxes.reshape(-1, 6)
		# keep = prediction_boxes[:, 4] > 0.01
		# prediction_boxes = prediction_boxes[keep]
		# keep = prediction_boxes[:, 4] > 0.01
		# prediction_boxes = prediction_boxes[keep]
		if _config['selector']['boxmap_use']=='gt':
			boxmap_out = boxmapCreate(targets['boxes'][0].cpu().numpy(),(1080,1920))
			centermap=render_centermap(boxmap_out.shape,targets['boxes'][0].cpu().numpy())
		else:
			if _config['boxmap']['image_pred_fusion']==True:
				boxmap_inps = []
				# pdb.set_trace()
				# for a_preds,a_img in zip(prediction_boxes,images):
				boxmap = boxmapCreate(prediction_boxes[:,0:4],(1080,1920))
				image = data_loader.dataset.image_preprocess(original_img[0])
				boxmap = torch.from_numpy(boxmap)
				boxmap=boxmap.type(torch.FloatTensor)
				if _config['boxmap']['score_fusion'] == True:
					ranges=[0.20,0.40,0.60,0.80,1.00]
					sm1 = boxmapCreate(prediction_boxes[prediction_boxes[:,4]<ranges[0]],(1080,1920))
					sm1 = torch.from_numpy(sm1)
					sm1 = sm1.type(torch.FloatTensor)
					sm2 = boxmapCreate(prediction_boxes[prediction_boxes[:,4]<ranges[1]],(1080,1920))
					sm2 = torch.from_numpy(sm2)
					sm2=sm2.type(torch.FloatTensor)
					sm3 = boxmapCreate(prediction_boxes[prediction_boxes[:,4]<ranges[2]],(1080,1920))
					sm3 = torch.from_numpy(sm3)
					sm3=sm3.type(torch.FloatTensor)
					sm4 = boxmapCreate(prediction_boxes[prediction_boxes[:,4]<ranges[3]],(1080,1920))
					sm4 = torch.from_numpy(sm4)
					sm4=sm4.type(torch.FloatTensor)
					sm5 = boxmapCreate(prediction_boxes[prediction_boxes[:,4]<ranges[4]],(1080,1920))
					sm5 = torch.from_numpy(sm5)
					sm5 = sm5.type(torch.FloatTensor)
					scoremap = torch.cat((sm1.unsqueeze(dim=0).to(device),sm2.unsqueeze(dim=0).to(device),sm3.unsqueeze(dim=0).to(device),sm4.unsqueeze(dim=0).to(device),sm5.unsqueeze(dim=0).to(device)),dim=0)
					boxmap_inp = torch.cat((image,boxmap.unsqueeze(dim=0).to(device),scoremap),dim=0)
				else:
					boxmap_inp = torch.cat((image,boxmap.unsqueeze(dim=0).to(device)),dim=0)
				boxmap_inp = fit(boxmap_inp.unsqueeze(dim=0), (1088,1920), fit_mode='contain').squeeze(dim=0)
				boxmap_inps.append(boxmap_inp)
			elif _config['boxmap']['image_pred_fusion']==False:
				boxmap_inps = []
				image = data_loader.dataset.image_preprocess(original_img[0])
				boxmap_inp = fit(image.unsqueeze(dim=0), (1088,1920), fit_mode='contain').squeeze(dim=0)
				boxmap_inps.append(boxmap_inp)
			else:
				raise NotImplementedError('This model is not implemented')
			boxmap_out=boxmap_model(torch.stack(boxmap_inps))
			# boxmap_out = torch.argmax(boxmap_out, dim=1)
			# upsample = nn.Upsample(size=(boxmap_out.shape[2]*eval_output_stride,boxmap_out.shape[3]*eval_output_stride), mode='bilinear',align_corners=True)
			# boxmap_out = upsample(boxmap_out)
			if _config['boxmap']['cornermap_use']==True:
				tl_maps=boxmap_out[0][1].cpu().numpy()
				tl_maps=tl_maps[0:1080,]
				br_maps=boxmap_out[0][2].cpu().numpy()
				br_maps=br_maps[0:1080,]
				boxmap_out=boxmap_out[0][0]
			boxmap_out[boxmap_out<=0.50]=0
			boxmap_out = torch.round(boxmap_out)
			if _config['boxmap']['cornermap_use']==False:
				boxmap_out=boxmap_out.squeeze(dim=0).squeeze(dim=0)
			boxmap_out = boxmap_out[0:1080,].cpu().numpy()
		
		# This part is for check each overlap class evaluation
		# target_overlaps = ious(targets[0]['boxes'],targets[0]['boxes'])
		# keep = []
		# # pdb.set_trace()
		# for inds,a_overlap in enumerate(target_overlaps):
		# 	n_overlap = len(torch.where(a_overlap!=0)[0])-1
		# 	if n_overlap==0:
		# 		keep.append(inds)
		# if keep!=[]:
		# 	targets[0]['boxes'] = targets[0]['boxes'][keep]
		# 	targets[0]['labels'] = targets[0]['labels'][keep]
		# 	targets[0]['area'] = targets[0]['area'][keep]
		# else:
		# 	targets[0]['boxes'] = torch.zeros((0,4))
		# 	targets[0]['labels'] = torch.zeros((0))
		# 	targets[0]['area'] = torch.zeros(0)
		# GT boxmap
		# gt_boxmap_out = boxmapCreate(targets[0]['boxes'].cpu().numpy(),(1080,1920))
		# boxmap_out[gt_boxmap_out==0]=0
		# pdb.set_trace()
		# # boxes = resize_boxes(targets[0]['boxes'],(749,1333),(1080,1920))

		# path = os.path.join(dirs,'gt_boxes','8.npy')
		# np.save(path,targets[0]['boxes'].numpy())

		# path = os.path.join(dirs,'gt_boxmap','8.npy')
		# np.save(path,gt_boxmap_out)
		# path = os.path.join(dirs,'pred_boxmap','8.npy')
		# np.save(path,boxmap_out)

		# # boxmap = torch.from_numpy(boxmap).to(device)
		# pdb.set_trace()
		# scores = prediction_boxes[0]['scores'].unsqueeze(dim=1)
		# pred_boxes = torch.cat((prediction_boxes[0]['boxes'],scores),dim=1)
<<<<<<< HEAD
		if _config['selector']['algorithm']!='set-nms' or _config['selector']['algorithm']!='nms':
			keep = remove_small_boxes(prediction_boxes[:,0:4],min_size=1)
			prediction_boxes =  prediction_boxes[keep]
			prediction_boxes = prediction_boxes[:,0:5]#.cpu().numpy()
		
=======
		
		if _config['selector']['algorithm']!='set-nms' or _config['selector']['algorithm']!='cpu-nms':
			prediction_boxes = clip_negative_boxes(prediction_boxes)
			keep = remove_small_boxes(prediction_boxes[:,0:4],min_size=1)
			prediction_boxes =  prediction_boxes[keep]
			prediction_boxes = prediction_boxes[:,0:5]#.cpu().numpy()
		if _config['selector']['algorithm']=='cpu-nms':

			keep = remove_small_boxes(prediction_boxes[:,0:4],min_size=1)
			prediction_boxes = prediction_boxes[:,0:5]
>>>>>>> Add cornermap

		# path = os.path.join(dirs,'pred_boxes','8.npy')
		# np.save(path,pred_boxes)
		# 
		final_boxes,mae,_ = box_select([boxmap_out],[prediction_boxes],[targets['boxes'][0].cpu().numpy()],tl_maps,br_maps,_config['selector'])
		# This part is for check each overlap class evaluation
		# pred_overlaps = ious(final_boxes[:,0:4],final_boxes[:,0:4])
		# keep = []
		# for inds,a_overlap in enumerate(pred_overlaps):
		# 	n_overlap = len(np.where(a_overlap!=0)[0])-1
		# 	if n_overlap==0:
		# 		keep.append(inds)
		# if keep!=[]:
		# 	final_boxes = final_boxes[keep]
		# else:
		# 	final_boxes = np.zeros((0,5))


		predictions = coco_evaluator.convert_sports_to_coco_detection(final_boxes[:,0:5])
		res = {targets['image_id'].item():predictions[0]}
		all_img_ids.append(image_ids.item())
		output = {}
		output['res']=res
		output['targ']=targets
		torch.save(output,os.path.join(evaluate_dir,str(image_ids.item())+'.pth'))
		gt_path = os.path.join(boxmap_dir,'gt',str(image_ids.item())+'.png')
		gt_boxmap = boxmapCreate(targets['boxes'][0].cpu().numpy(),(1080,1920))
		gt_img = Image.fromarray(np.uint8(gt_boxmap))
		gt_img.save(gt_path)
		# pdb.set_trace()
		# f = open(gt_path, 'wb')
		# np.save(f,boxmap_out)
		# f.close()
		pred_path = os.path.join(boxmap_dir,'pred',str(image_ids.item())+'.png')
		# f = open(pred_path, 'wb')
		pred_boxmap = boxmapCreate(final_boxes[:,0:4],(1080,1920))
		pred_img = Image.fromarray(np.uint8(pred_boxmap))
		pred_img.save(pred_path)
		# np.save(f,pred_boxmap)
		# f.close()
		# with open(os.path.join(evaluate_dir,str(image_ids.item())+'.pickle'), 'wb') as handle:
		# 	pickle.dump(res, handle, protocol=pickle.HIGHEST_PROTOCOL)
		# prediction_boxes
		# coco_evaluator.update(res)


		# nms_boxes,_ = box_select([boxmap_out],[pred_boxes],_config['optional_selector'])
		overall_mae = overall_mae+mae
		# img = original_img[0].cpu().permute(1,2,0).numpy()
		# im = Image.fromarray(np.uint8(img*255))		
		# draw = ImageDraw.Draw(im)

		# for a_box in final_boxes:
		# 	draw.rectangle([int(a_box[0]),int(a_box[1]),int(a_box[2]),int(a_box[3])],outline="red",fill=None)
		# for a_box in targets[0]["boxes"]:
			# draw.rectangle([int(a_box[0]),int(a_box[1]),int(a_box[2]),int(a_box[3])],outline="green",fill=None)
		# plt.imshow(im)
		# plt.show()
		# im.save(os.path.join(save_path,str(frm_number[0])+".jpg"))
		# with open(os.path.join(output_save_dir,"preds",str(fname)+".txt"), mode='w') as det_file:
		# 	det_writer = csv.writer(det_file, delimiter=' ', quotechar='"', quoting=csv.QUOTE_MINIMAL)
		# 	for a_box in final_boxes:
		# 		det_writer.writerow([a_box[0],a_box[1],a_box[2],a_box[3],a_box[4]])

		# with open(os.path.join(output_save_dir,"gts",str(fname)+".txt"), mode='w') as gt_file:
		# 	gt_writer = csv.writer(gt_file, delimiter=' ', quotechar='"', quoting=csv.QUOTE_MINIMAL)
		# 	for a_box in targets[0]['boxes']:
		# 		gt_writer.writerow([a_box[0].item(),a_box[1].item(),a_box[2].item(),a_box[3].item()])

		fname=fname+1

	# pdb.set_trace()
	print('Overall MAE: {0} for videoset: {1}'.format(sum(overall_mae)/len(overall_mae),seq))
	return overall_mae,fname,all_img_ids#,final_boxes,boxmap_out,targets[0]['boxes'].cpu().numpy(),nms_boxes
	# pdb.set_trace()
		# det_boxes.extend([torch.from_numpy(final_boxes[:,0:4])])
		# det_scores.extend([torch.from_numpy(final_boxes[:,4])])
		# # labels = targets[0]['labels'][0:len(final_boxes)]
		# labels = torch.ones(len(final_boxes))
		# det_labels.extend([labels])
		# # pdb.set_trace()
		# true_boxes.extend([targets[0]['boxes']])
		# true_labels.extend([targets[0]['labels']])
		# true_difficulties.extend([labels[0:len(targets[0]['boxes'])]])
	# APs, mAP=calculate_mAP(det_boxes, det_labels, det_scores, true_boxes, true_labels, true_difficulties)
	# print('\nMean Average Precision (mAP): %.3f' % mAP)



def remove_small_boxes(boxes, min_size):
	# type: (Tensor, float) -> Tensor
	"""
	Remove boxes which contains at least one side smaller than min_size.
	Arguments:
		boxes (Tensor[N, 4]): boxes in (x1, y1, x2, y2) format
		min_size (float): minimum size
	Returns:
		keep (Tensor[K]): indices of the boxes that have both sides
			larger than min_size
	"""
	ws, hs = boxes[:, 2] - boxes[:, 0], boxes[:, 3] - boxes[:, 1]
	keep = (ws >= min_size) & (hs >= min_size)
	keep = keep.nonzero()
	return keep[0]
def clip_negative_boxes(boxes):
	boxes[boxes<0]=0
	# boxes[:,0]
	# boxes[:,0]=min(boxes[:,0].all(),0)
	# boxes[:,1]=min(boxes[:,1].all(),0)
	return boxes
def emd_refine_box_preprocess(pred_boxes,top_k,model_type):
	if model_type=='emd':
		n = pred_boxes.shape[0]
		pred_boxes = pred_boxes.reshape(-1, 6)
		idents = np.tile(np.arange(n)[:,None], (1, top_k)).reshape(-1, 1)
		pred_boxes = np.hstack((pred_boxes, idents))
		keep = pred_boxes[:, 4] > 0.01
		pred_boxes = pred_boxes[keep]
	if model_type=='fpn':
		assert pred_boxes.shape[-1] % 6 == 0, "Prediction dim Error!"
		pred_boxes = pred_boxes.reshape(-1, 6)
		keep = pred_boxes[:, 4] > 0.01
		pred_boxes = pred_boxes[keep]
	return pred_boxes