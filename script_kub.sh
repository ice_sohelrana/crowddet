#/usr/bin/env bash
IMAGE="registry.dl.cs.latrobe.edu.au/sohel/deeplabv3plus-smoothl1loss-cornermap-emd-both"
# 1. Build image
docker build . -t "$IMAGE"
# 3. Push image to cluster
docker push "$IMAGE"
# 4. Run container on cluster
kubectl apply -f pod.yml

# 2. Run on the local just after build
# docker run \
# --runtime=nvidia -it \
# --volume=/nfs/users/haritha/datasets/volleyball:/data \
# --volume=/nfs/users/haritha/logs:/results \
# --rm "$IMAGE" python3 combined_network.py

